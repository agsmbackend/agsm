﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MotoreListiniV2.DataAccessLayer.Database;
using MotoreListiniV2.DataAccessLayer.Models.Listino;

namespace MotoreListiniV2.DataAccessLayer.Extensions
{
    public static class ControlliListino
    {
        /// <summary>
        /// Controlla se le date sono ok per duplicare il listino
        /// </summary>
        /// <param name="db"></param>
        /// <param name="idOggetto"></param>
        /// <param name="s"></param>
        /// <param name="dataValDa"></param>
        /// <param name="dataValA"></param>
        /// <returns></returns>
        public static async Task<bool> IsOkNewDateDuplicaAsync(this ListinoContext db, int idOggetto, Source s, DateTime dataValDa, DateTime dataValA)
        {
            _ = db ?? throw new ArgumentNullException(nameof(db));

            ProdOffRel checkdate = null;

            switch (s)
            {
                case Source.Prodotto:

                    checkdate = await db.ProdOffRel.AsNoTracking()
                        .Where(x => x.ProdProdottoId == idOggetto)
                        .Where(x => x.Tte >= dataValDa)
                        .Where(x => x.Tts <= dataValA)
                        .FirstOrDefaultAsync().ConfigureAwait(false);
                    break;

                case Source.Listino:

                    var idProdotto = await db.ProdOffRel.AsNoTracking()
                        .Where(x => x.OffertaId == idOggetto)
                        .Select(x => x.ProdProdottoId)
                        .FirstOrDefaultAsync()
                        .ConfigureAwait(false) ?? -1;

                    checkdate = await db.ProdOffRel.AsNoTracking()
                        .Where(x => x.ProdProdottoId == idProdotto)
                        .Where(x => x.Tte >= dataValDa)
                        .Where(x => x.Tts <= dataValA)
                        .Where(x => x.OffertaId != idOggetto)
                        .FirstOrDefaultAsync()
                        .ConfigureAwait(false);
                    break;

                case Source.AttivazioneProdotto:

                    checkdate = await db.ProdOffRel.AsNoTracking()
                        .Where(x => x.ProdProdottoId == idOggetto)
                        .Where(x => x.DataAAtt >= dataValDa)
                        .Where(x => x.DataDaAtt <= dataValA)
                        .FirstOrDefaultAsync()
                        .ConfigureAwait(false);

                    break;

                case Source.AttivazioneListino:

                    idProdotto = await db.ProdOffRel.AsNoTracking()
                        .Where(x => x.OffertaId == idOggetto)
                        .Select(x => x.ProdProdottoId)
                        .FirstOrDefaultAsync()
                        .ConfigureAwait(false) ?? -1;

                    checkdate = await db.ProdOffRel.AsNoTracking()
                       .Where(x => x.ProdProdottoId == idProdotto)
                       .Where(x => x.DataAAtt >= dataValDa)
                       .Where(x => x.DataDaAtt <= dataValA)
                       .Where(x => x.OffertaId != idOggetto)
                       .FirstOrDefaultAsync()
                       .ConfigureAwait(false);
                    break;
            }

            return checkdate != null;
        }

        /// <summary>
        /// Controlla se esiste già un prodotto con il nome passato
        /// </summary>
        /// <param name="db"></param>
        /// <param name="nomeProdotto"></param>
        /// <returns></returns>
        public static async Task<bool> ExistsProdottoAsync(this ListinoContext db, string nomeProdotto)
        {
            _ = db ?? throw new ArgumentNullException(nameof(db));

            return await db.Prodotto.AsNoTracking()
                        .AnyAsync(x => x.ProdDescrizione == nomeProdotto);
        }

        /// <summary>
        /// Controlla se esiste già un listino con il nome passato
        /// </summary>
        /// <param name="db"></param>
        /// <param name="codListino"></param>
        /// <returns></returns>
        public static async Task<bool> ExistsCodListinoAsync(this ListinoContext db, string codListino)
        {
            _ = db ?? throw new ArgumentNullException(nameof(db));

            return await db.ProdottoOfferta.AsNoTracking()
                    .AnyAsync(x => x.OffertaCod == codListino);
        }

        public enum Source
        {
            Prodotto,
            Listino,
            AttivazioneProdotto,
            AttivazioneListino
        }
    }
}