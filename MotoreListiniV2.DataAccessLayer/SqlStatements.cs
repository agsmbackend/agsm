﻿namespace MotoreListiniV2.DataAccessLayer
{
    public static class SqlStatements
    {
        public const string SelectElencoListini =
            @"select Prod_prodotto_id                       as idProdotto,
               prod_descrizione                       as prodotto,
               po.ts_tipo_servizio_id                 as tipoServizio,
               tariffa,
               po.offerta_id                          as idOfferta,
               po.offerta_cod                         as codOfferta,
               scheda_prodotto,
               mese,
               anno,
               rev                                    as revisione,
               trim                                   as trimestre,
               username,
               ttp.tipo_termini_pagamenti_descrizione as terminiPagamento,
               po.flagindtofix                        as passaFisso,
               convert(varchar, po.tts, 103)          as iniVal,
               convert(varchar, po.tte, 103)          as finVal,
               convert(varchar, po.dataDa_Att, 103)   as iniAtt,
               convert(varchar, po.dataA_Att, 103)    as finAtt,
               price.prezzi_id                        as idPrezzo,
               price.TipoPrezzo,
               price.Offerta,
               price.componente,
               price.prezzo,
               price.unitamisura                      as unitaMisuraPrezzo,
               price.MeseDa                           as valMeseDa,
               price.MeseA                            as valMeseA,
               price.percapplicata                    as Percentuale,
               cf.componenti_fatturazione_descrizione as AltroComponente,
               cfat.value                             as prezzoAltraComp,
               cf.componenti_fatturazione_unitamisura as unitaMisuraAltraComp,
               cfat.range_da                          as valMeseDaAltraComp,
               cfat.range_a                           as valMeseAAltraComp,
               cfat.opzionale                         as isOpzionaleAltraComp
        from LISTINO.vw_Prodotto_Offerte po
                 left join tipo_termini_pagamenti ttp with (nolock)
                           on ttp.tipo_termini_pagamenti_id = po.tipo_termini_pagamenti_id
                 left join LISTINO.vw_Prezzi_Offerte price with (nolock) on po.offerta_id = price.offerta_id
                 left join LISTINO.offerta_componentifatt cfat with (nolock) on cfat.offerta_id = po.offerta_id
                 left join LISTINO.componenti_fatturazione cf with (nolock)
                           on cf.componenti_fatturazione_id = cfat.componenti_fatturazione_id
        where cfat.tte is null
          and po.offerta_id in (@idListino)
        order by po.Prod_prodotto_id, po.offerta_id desc, price.tipo_prezzi_id, price.componenti_prezzi_id";


    }
}