﻿namespace MotoreListiniV2.DataAccessLayer.Database
{
    internal static class SchemaName
    {
        internal const string Dbo = "Dbo";
        internal const string Listino = "LISTINO";
        internal const string Prospect = "PROSPECT";
    }
}