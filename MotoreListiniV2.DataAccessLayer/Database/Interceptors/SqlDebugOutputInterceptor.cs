﻿using System.Data.Common;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Serilog;

namespace MotoreListiniV2.DataAccessLayer.Database.Interceptors
{
    public class SqlDebugOutputInterceptor : DbCommandInterceptor
    {
        public override Task CommandFailedAsync(DbCommand command, CommandErrorEventData eventData,
            CancellationToken cancellationToken = new())
        {
            Log.Error(eventData.Exception.Demystify(),
                "---- CommandFailed ---- Duration {Duration}ms - CommandText {CommandText} - CommandParameters {CommandParameters} - MessageError {MessageError}",
                eventData.Duration.TotalMilliseconds.ToString(CultureInfo.InvariantCulture),
                command.CommandText,
                string.Join(", ", command.Parameters.Cast<SqlParameter>().ToList().Select(p => $"{p.ParameterName}={p.Value}")),
                eventData.Exception.InnerException?.Message);

            return base.CommandFailedAsync(command, eventData, cancellationToken);
        }
    }
}