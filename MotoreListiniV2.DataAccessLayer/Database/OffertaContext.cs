﻿using Microsoft.EntityFrameworkCore;
using MotoreListiniV2.DataAccessLayer.Database.EntityConfigurations.Offerta;
using MotoreListiniV2.DataAccessLayer.Models.Offerta;

namespace MotoreListiniV2.DataAccessLayer.Database
{
    public class OffertaContext : DbContext
    {
        public OffertaContext(DbContextOptions<OffertaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ProdArea> ProdArea { get; set; }
        public virtual DbSet<ProspectLam> ProspectLam { get; set; }

        public virtual DbSet<Area> Area { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.ApplyConfiguration(new AreaConfiguration());
            modelBuilder.ApplyConfiguration(new ProdAreaConfiguration());
            modelBuilder.ApplyConfiguration(new ProspectLamConfiguration());

        }
    }
}