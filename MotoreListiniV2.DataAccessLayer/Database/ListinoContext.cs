﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using MotoreListiniV2.DataAccessLayer.Database.EntityConfigurations.Listino;
using MotoreListiniV2.DataAccessLayer.Models.Listino;
using UtilitiesGSA.Extensions;

namespace MotoreListiniV2.DataAccessLayer.Database
{
    public class ListinoContext : DbContext
    {
        /*
        public ListinoContext()
        {
        }
        */

        public ListinoContext(DbContextOptions<ListinoContext> options)
           : base(options)
        {
        }

        public virtual DbSet<ComponentiFatturazione> ComponentiFatturazione { get; set; }
        public virtual DbSet<ComponentiPrezzi> ComponentiPrezzi { get; set; }
        public virtual DbSet<OffertaComponentifatt> OffertaComponentifatt { get; set; }
        public virtual DbSet<OffertaPrezzi> OffertaPrezzi { get; set; }
        public virtual DbSet<OffertaToNetaOp> OffertaToNetaOp { get; set; }
        public virtual DbSet<ProdOffRel> ProdOffRel { get; set; }
        public virtual DbSet<Prodotto> Prodotto { get; set; }
        public virtual DbSet<ProdottoOfferta> ProdottoOfferta { get; set; }
        public virtual DbSet<TipoCompRel> TipoCompRel { get; set; }
        public virtual DbSet<TipoPrezzi> TipoPrezzi { get; set; }
        public virtual DbSet<TipoServizio> TipoServizio { get; set; }
        public virtual DbSet<TipoTariffa> TipoTariffa { get; set; }
        public virtual DbSet<Uso> Uso { get; set; }
        public virtual DbSet<MateriaPrima> MateriaPrima { get; set; }
        public virtual DbSet<TipoTerminiPagamenti> TipoTerminiPagamenti { get; set; }
        public virtual DbSet<AuditModifiche> AuditModifiche { get; set; }
        public virtual DbSet<Categoria> Categoria { get; set; }
        public virtual DbSet<Configurazione> Configurazione { get; set; }
        public virtual DbSet<OffertaCaratteristiche> OffertaCaratteristiche { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        [DbFunction(name: "ufn_CheckListinoAlreadyApplicato", schema: "LISTINO")]
        public static int UfnCheckListinoAlreadyApplicato(string codListino)
        {
            return 0;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TipoTerminiPagamenti>().HasQueryFilter(b => b.TipoTerminiPagamentiCod != "dd");
            modelBuilder.Entity<TipoPrezzi>().HasQueryFilter(b => b.Tte == null);
            modelBuilder.Entity<TipoCompRel>().HasQueryFilter(b => b.Tte == null);
            modelBuilder.Entity<OffertaPrezzi>().HasQueryFilter(b => b.Tte == null);
            modelBuilder.Entity<OffertaComponentifatt>().HasQueryFilter(b => b.Tte == null);

            modelBuilder.ApplyConfiguration(new AuditModificheConfiguration());
            modelBuilder.ApplyConfiguration(new ComponentiFatturazioneConfiguration());
            modelBuilder.ApplyConfiguration(new ComponentiPrezziConfiguration());
            modelBuilder.ApplyConfiguration(new OffertaComponentifattConfiguration());
            modelBuilder.ApplyConfiguration(new OffertaPrezziConfiguration());
            modelBuilder.ApplyConfiguration(new OffertaToNetaOpConfiguration());
            modelBuilder.ApplyConfiguration(new ProdOffRelConfiguration());
            modelBuilder.ApplyConfiguration(new ProdottoConfiguration());
            modelBuilder.ApplyConfiguration(new ProdottoOffertaConfiguration());
            modelBuilder.ApplyConfiguration(new TipoCompRelConfiguration());
            modelBuilder.ApplyConfiguration(new TipoPrezziConfiguration());
            modelBuilder.ApplyConfiguration(new TipoServizioConfiguration());
            modelBuilder.ApplyConfiguration(new TipoTariffaConfiguration());
            modelBuilder.ApplyConfiguration(new UsoConfiguration());
            modelBuilder.ApplyConfiguration(new MateriaPrimaConfiguration());
            modelBuilder.ApplyConfiguration(new TipoTerminiPagamentiConfiguration());
            modelBuilder.ApplyConfiguration(new CategoriaConfiguration());
            modelBuilder.ApplyConfiguration(new ConfigurazioneConfiguration());
            modelBuilder.ApplyConfiguration(new OffertaCaratteristicheConfiguration());

            var trimStringConverter = new ValueConverter<string, string>(
                toDb => toDb.TrimIfNotEmpty(),
                fromDb => fromDb.TrimIfNotEmpty());


            foreach (var entityType in modelBuilder.Model.GetEntityTypes())
            {
                foreach (var entityProperty in entityType.GetProperties())
                {
                    if (entityProperty.ClrType == typeof(string))
                    {
                        entityProperty.SetValueConverter(trimStringConverter);
                    }
                }
            }

        }

    }
}