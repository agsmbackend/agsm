﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MotoreListiniV2.DataAccessLayer.Models.Offerta;

namespace MotoreListiniV2.DataAccessLayer.Database.EntityConfigurations.Offerta
{
    public class ProspectLamConfiguration : IEntityTypeConfiguration<ProspectLam>
    {
        public void Configure(EntityTypeBuilder<ProspectLam> builder)
        {
            builder.ToTable("prospect_lam", SchemaName.Prospect);

            builder.HasIndex(e => new {e.WsdDefWfStepId, e.TsTipoServizioId})
                .HasDatabaseName("IX_offerte_ts");

            builder.HasIndex(e => new {e.SogSoggettoCreate, e.WsdDefWfStepId, e.TsTipoServizioId})
                .HasDatabaseName("IX_offerte_ts_2");

            builder.Property(e => e.ProspectLamId).HasColumnName("prospect_lam_id");

            builder.Property(e => e.Alfa)
                .HasColumnName("alfa")
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.Allegato)
                .HasColumnName("allegato")
                .HasMaxLength(255);

            builder.Property(e => e.AllegatoEmail)
                .HasColumnName("allegato_email")
                .HasMaxLength(255);

            builder.Property(e => e.BonusPagamenti)
                .HasColumnName("bonus_pagamenti")
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.ClienteRef)
                .HasColumnName("cliente_ref")
                .HasMaxLength(255);

            builder.Property(e => e.CondPart).HasColumnName("cond_part");

            builder.Property(e => e.DataFinForn)
                .HasColumnName("data_fin_forn")
                .HasColumnType("datetime");

            builder.Property(e => e.DataIniForn)
                .HasColumnName("data_ini_forn")
                .HasColumnType("datetime");

            builder.Property(e => e.DataInsert)
                .HasColumnName("data_insert")
                .HasColumnType("datetime");

            builder.Property(e => e.DataScadOff)
                .HasColumnName("data_scad_off")
                .HasColumnType("datetime");

            builder.Property(e => e.FornitoreRef)
                .HasColumnName("fornitore_ref")
                .HasMaxLength(255);

            builder.Property(e => e.Garanzia)
                .HasColumnName("garanzia")
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.GgVal).HasColumnName("gg_val");

            builder.Property(e => e.IsOffertLam)
                .IsRequired()
                .HasColumnName("isOffertLam")
                .HasDefaultValueSql("((1))");

            builder.Property(e => e.LivelloAutorizzazioneId).HasColumnName("livello_autorizzazione_id");

            builder.Property(e => e.MesiGaranzia).HasColumnName("mesi_garanzia");

            builder.Property(e => e.MinutiScadOff)
                .HasColumnName("minuti_scad_off")
                .HasMaxLength(2)
                .IsUnicode(false);

            builder.Property(e => e.NomeOfferta)
                .HasColumnName("nomeOfferta")
                .HasMaxLength(255);

            builder.Property(e => e.OraScadOff)
                .HasColumnName("ora_scad_off")
                .HasMaxLength(2)
                .IsUnicode(false);

            builder.Property(e => e.PrezzoGas)
                .HasColumnName("prezzo_gas")
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.ProdProdottoId).HasColumnName("prod_prodotto_id");

            builder.Property(e => e.ProspectLamIdStorico).HasColumnName("prospect_lam_id_storico");

            builder.Property(e => e.PspsogSoggettoId).HasColumnName("pspsog_soggetto_id");

            builder.Property(e => e.SogSoggettoCreate).HasColumnName("sog_soggetto_create");

            builder.Property(e => e.TfGas)
                .HasColumnName("tf_gas")
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.TipoAcciseId).HasColumnName("tipo_accise_id");

            builder.Property(e => e.TipoBonus1).HasColumnName("tipo_bonus1");

            builder.Property(e => e.TipoBonus2).HasColumnName("tipo_bonus2");

            builder.Property(e => e.TipoFasceId).HasColumnName("tipo_fasce_id");

            builder.Property(e => e.TipoGaranziaId).HasColumnName("tipo_garanzia_id");

            builder.Property(e => e.TipoModalitaPagamentiId).HasColumnName("tipo_modalita_pagamenti_id");

            builder.Property(e => e.TipoPrezzoId).HasColumnName("tipo_prezzo_id");

            builder.Property(e => e.TipoRecessoId).HasColumnName("tipo_recesso_id");

            builder.Property(e => e.TipoSogId).HasColumnName("tipo_sog_id");

            builder.Property(e => e.TipoTerminiPagamentiId).HasColumnName("tipo_termini_pagamenti_id");

            builder.Property(e => e.TsTipoServizioId)
                .HasColumnName("ts_tipo_servizio_id")
                .HasMaxLength(3);

            builder.Property(e => e.ValoreOfferta).HasColumnName("valore_offerta");

            builder.Property(e => e.Versione).HasColumnName("versione");

            builder.Property(e => e.VolumeAnnuo)
                .HasColumnName("volume_annuo")
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.VolumeContrattuale)
                .HasColumnName("volume_contrattuale")
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.WsdDefWfStepId).HasColumnName("wsd_def_wf_step_id");
        }
    }
}