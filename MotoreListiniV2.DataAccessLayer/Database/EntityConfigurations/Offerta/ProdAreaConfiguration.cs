﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MotoreListiniV2.DataAccessLayer.Models.Offerta;

namespace MotoreListiniV2.DataAccessLayer.Database.EntityConfigurations.Offerta
{
    public class ProdAreaConfiguration : IEntityTypeConfiguration<ProdArea>
    {
        public void Configure(EntityTypeBuilder<ProdArea> builder)
        {
            builder.HasKey(e => new { e.ProdProdottoId, e.ArAreaId, e.Tts });

            builder.ToTable("prod_area", SchemaName.Dbo);

            builder.Property(e => e.ProdProdottoId).HasColumnName("prod_prodotto_id");

            builder.Property(e => e.ArAreaId).HasColumnName("ar_area_id");

            builder.Property(e => e.Tts)
                .HasColumnName("TTS")
                .HasColumnType("datetime");

            builder.Property(e => e.Tte)
                .HasColumnName("TTE")
                .HasColumnType("datetime");
        }
    }
}