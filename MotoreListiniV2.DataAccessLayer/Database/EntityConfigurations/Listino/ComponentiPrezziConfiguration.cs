﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MotoreListiniV2.DataAccessLayer.Models.Listino;

namespace MotoreListiniV2.DataAccessLayer.Database.EntityConfigurations.Listino
{
    public class ComponentiPrezziConfiguration : IEntityTypeConfiguration<ComponentiPrezzi>
    {
        public void Configure(EntityTypeBuilder<ComponentiPrezzi> builder)
        {
            builder.ToTable("componenti_prezzi", SchemaName.Listino);

            builder.Property(e => e.ComponentiPrezziId).HasColumnName("componenti_prezzi_id");

            builder.Property(e => e.ComponentiPrezziCod)
                .HasColumnName("componenti_prezzi_cod")
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.ComponentiPrezziDescrizione)
                .HasColumnName("componenti_prezzi_descrizione")
                .HasMaxLength(255)
                .IsUnicode(false);

            builder.Property(e => e.Unitamisura)
                .HasColumnName("unitamisura")
                .HasMaxLength(50)
                .IsUnicode(false);
        }
    }
}