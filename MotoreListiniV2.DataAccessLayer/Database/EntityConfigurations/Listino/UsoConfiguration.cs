﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MotoreListiniV2.DataAccessLayer.Models.Listino;

namespace MotoreListiniV2.DataAccessLayer.Database.EntityConfigurations.Listino
{
    public class UsoConfiguration : IEntityTypeConfiguration<Uso>
    {
        public void Configure(EntityTypeBuilder<Uso> builder)
        {
            builder.HasKey(e => e.UsoId);

            builder.ToTable("uso", SchemaName.Dbo);

            builder.Property(e => e.UsoId).HasColumnName("uso_id");

            builder.Property(e => e.UsoDesc)
                .IsRequired()
                .HasColumnName("uso_desc")
                .HasMaxLength(255);

            builder.Property(e => e.TsTipoServizioId)
                .HasColumnName("ts_tipo_servizio_id")
                .HasMaxLength(3);

            builder.HasOne(d => d.TsTipoServizio)
                .WithMany(p => p.Usi)
                .HasForeignKey(d => d.TsTipoServizioId)
                .HasConstraintName("FK_uso__tipo_servizio");
        }
    }
}