﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MotoreListiniV2.DataAccessLayer.Models.Listino;

namespace MotoreListiniV2.DataAccessLayer.Database.EntityConfigurations.Listino
{
    public class ProdottoOffertaConfiguration : IEntityTypeConfiguration<ProdottoOfferta>
    {
        public void Configure(EntityTypeBuilder<ProdottoOfferta> builder)
        {
            builder.HasKey(e => e.OffertaId);

            builder.ToTable("Prodotto_Offerta", SchemaName.Listino);

            builder.HasIndex(e => e.OffertaCod)
                .HasDatabaseName("ak_codOfferta")
                .IsUnique();

            builder.Property(e => e.OffertaId).HasColumnName("offerta_id");

            builder.Property(e => e.Anno).HasColumnName("anno");

            builder.Property(e => e.Flagindtofix).HasColumnName("flagindtofix");

            builder.Property(e => e.Mese).HasColumnName("mese");

            builder.Property(e => e.OffertaCod)
                .HasColumnName("offerta_cod")
                .HasMaxLength(50);

            builder.Property(e => e.OffertaNome)
                .HasColumnName("offerta_nome")
                .HasMaxLength(200);

            builder.Property(e => e.OffertaDescrizione)
                .HasColumnName("offerta_descrizione")
                .HasMaxLength(255);

            builder.Property(e => e.OffertaNote)
                .HasColumnName("offerta_note")
                .IsUnicode(false);

            builder.Property(e => e.Rev).HasColumnName("rev");

            builder.Property(e => e.SchedaConfrontabilita)
                .HasColumnName("scheda_confrontabilita")
                .HasMaxLength(255);

            builder.Property(e => e.SchedaProdotto)
                .HasColumnName("scheda_prodotto")
                .HasMaxLength(255);

            builder.Property(e => e.ScopaDataatt).HasColumnName("scopa_dataatt");

            builder.Property(e => e.TipoTerminiPagamentiId).HasColumnName("tipo_termini_pagamenti_id");

            builder.Property(e => e.Trim).HasColumnName("trim");

            builder.Property(e => e.Username)
                .HasColumnName("username")
                .HasMaxLength(255)
                .IsUnicode(false);

            builder.HasOne(d => d.TipoTerminiPagamenti)
                .WithMany(p => p.ProdottoOfferta)
                .HasForeignKey(d => d.TipoTerminiPagamentiId)
                .HasConstraintName("fk_terminipagamaneti_po");

            
            builder.Property(e => e.Uso)
                .HasColumnName("uso")
                .HasMaxLength(3);

            builder.Property(e => e.MateriaPrima)
                .HasColumnName("materia_prima")
                .HasMaxLength(3);

            builder.Property(e => e.PrezzoEnergia).HasColumnName("prezzo_energia");

            builder.Property(e => e.PercMateriaEnergia).HasColumnName("perc_materia_energia");

            builder.Property(e => e.PercComponenteEnergia).HasColumnName("perc_componente_energia");

            builder.Property(e => e.PercSpesaTrasportoGestione).HasColumnName("perc_spesa_trasporto_gestione");

            builder.Property(e => e.PercOneriSistema).HasColumnName("perc_oneri_sistema");

            builder.Property(e => e.PercAsos).HasColumnName("perc_asos");

            builder.Property(e => e.PercCompExtra1).HasColumnName("perc_comp_extra_1");

            builder.Property(e => e.PercCompExtra2).HasColumnName("perc_comp_extra_2");

            builder.Property(e => e.PercCompExtra3).HasColumnName("perc_comp_extra_3");

    }
  }
}