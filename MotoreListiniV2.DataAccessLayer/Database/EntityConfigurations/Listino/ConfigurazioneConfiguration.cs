﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MotoreListiniV2.DataAccessLayer.Models.Listino;

namespace MotoreListiniV2.DataAccessLayer.Database.EntityConfigurations.Listino
{
    public class ConfigurazioneConfiguration : IEntityTypeConfiguration<Configurazione>
    {
        public void Configure(EntityTypeBuilder<Configurazione> builder)
        {
            builder.HasKey(e => e.ConfId);

            builder.ToTable("configurazione", SchemaName.Listino);

            builder.Property(e => e.ConfId).HasColumnName("conf_id");

            builder.Property(e => e.CatId).HasColumnName("cat_id")
                .HasMaxLength(255);

            builder.Property(e => e.ConfNome).HasColumnName("conf_nome");

            builder.Property(e => e.ConfTesto).HasColumnName("conf_testo");

            builder.Property(e => e.ConfTextMaxLength).HasColumnName("conf_text_max_length");

            builder.HasOne(d => d.Categoria)
                .WithMany(p => p.Configurazioni)
                .HasForeignKey(d => d.CatId)
                .HasConstraintName("fk_categorie_configurazioni");
        }
    }
}