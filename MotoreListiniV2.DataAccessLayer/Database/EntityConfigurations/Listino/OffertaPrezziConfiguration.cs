﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MotoreListiniV2.DataAccessLayer.Models.Listino;

namespace MotoreListiniV2.DataAccessLayer.Database.EntityConfigurations.Listino
{
    public class OffertaPrezziConfiguration : IEntityTypeConfiguration<OffertaPrezzi>
    {
        public void Configure(EntityTypeBuilder<OffertaPrezzi> builder)
        {
            builder.HasKey(e => e.OffPrezziId);

            builder.ToTable("offerta_prezzi", SchemaName.Listino);

            builder.Property(e => e.OffPrezziId).HasColumnName("off_prezzi_id");

            builder.Property(e => e.ComponentiPrezziId).HasColumnName("componenti_prezzi_id");

            builder.Property(e => e.DataInsert)
                .HasColumnName("data_insert")
                .HasColumnType("datetime");

            builder.Property(e => e.OffertaId).HasColumnName("offerta_id");

            builder.Property(e => e.Percapplicata).HasColumnName("percapplicata");

            builder.Property(e => e.Prezzo).HasColumnName("prezzo");

            builder.Property(e => e.ProdProdottoId).HasColumnName("prod_prodotto_id");

            builder.Property(e => e.RangeA).HasColumnName("range_a");

            builder.Property(e => e.RangeDa).HasColumnName("range_da");

            builder.Property(e => e.TipoPrezziId).HasColumnName("tipo_prezzi_id");

            builder.Property(e => e.Tte)
                .HasColumnName("tte")
                .HasColumnType("datetime");

            builder.Property(e => e.Tts)
                .HasColumnName("tts")
                .HasColumnType("datetime");

            builder.HasOne(d => d.ComponentiPrezzi)
                .WithMany(p => p.OffertaPrezzi)
                .HasForeignKey(d => d.ComponentiPrezziId)
                .HasConstraintName("fk_componenti_prezziofferta");

            builder.HasOne(d => d.Offerta)
                .WithMany(p => p.OffertaPrezzi)
                .HasForeignKey(d => d.OffertaId)
                .HasConstraintName("fk_offerta_prezzi");

            builder.HasOne(d => d.ProdProdotto)
                .WithMany(p => p.OffertaPrezzi)
                .HasForeignKey(d => d.ProdProdottoId)
                .HasConstraintName("fk_offerta_prezzi_prodotto");

            builder.HasOne(d => d.TipoPrezzi)
                .WithMany(p => p.OffertaPrezzi)
                .HasForeignKey(d => d.TipoPrezziId)
                .HasConstraintName("fk_tipo_prezziofferta");
        }
    }
}