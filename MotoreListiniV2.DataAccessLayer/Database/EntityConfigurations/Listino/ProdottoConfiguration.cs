﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MotoreListiniV2.DataAccessLayer.Models.Listino;

namespace MotoreListiniV2.DataAccessLayer.Database.EntityConfigurations.Listino
{
    public class ProdottoConfiguration : IEntityTypeConfiguration<Prodotto>
    {
        public void Configure(EntityTypeBuilder<Prodotto> builder)
        {
            builder.HasKey(e => e.ProdProdottoId);

            builder.ToTable("prodotto", SchemaName.Dbo);

            builder.HasIndex(e => new {e.ProdDescrizione, e.TsTipoServizioId})
                .HasDatabaseName("ak_Prod_Desc")
                .IsUnique();

            builder.Property(e => e.ProdProdottoId).HasColumnName("Prod_prodotto_id");

            builder.Property(e => e.HaveIban)
                .HasColumnName("haveIban")
                .HasDefaultValueSql("((0))");

            builder.Property(e => e.HavePaperless)
                .HasColumnName("havePaperless")
                .HasDefaultValueSql("((0))");

            builder.Property(e => e.OfferingLam).HasColumnName("offeringLam");

            builder.Property(e => e.ProdDescrizione)
                .HasColumnName("prod_descrizione")
                .HasMaxLength(255);

            builder.Property(e => e.ProdNote)
                .HasColumnName("prod_note")
                .HasMaxLength(255);

            builder.Property(e => e.Tariffa)
                .IsRequired()
                .HasColumnName("tariffa")
                .HasMaxLength(255)
                .HasDefaultValueSql("('DOM')");

            builder.Property(e => e.TsTipoServizioId)
                .IsRequired()
                .HasColumnName("ts_tipo_servizio_id")
                .HasMaxLength(3);
        }
    }
}