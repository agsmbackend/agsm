﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MotoreListiniV2.DataAccessLayer.Models.Listino;

namespace MotoreListiniV2.DataAccessLayer.Database.EntityConfigurations.Listino
{
    public class OffertaComponentifattConfiguration : IEntityTypeConfiguration<OffertaComponentifatt>
    {
        public void Configure(EntityTypeBuilder<OffertaComponentifatt> builder)
        {
            builder.HasKey(e => e.OffFattId);

            builder.ToTable("offerta_componentifatt", SchemaName.Listino);

            builder.Property(e => e.OffFattId).HasColumnName("off_fatt_id");

            builder.Property(e => e.ComponentiFatturazioneId).HasColumnName("componenti_fatturazione_id");

            builder.Property(e => e.DataInsert)
                .HasColumnName("data_insert")
                .HasColumnType("datetime");

            builder.Property(e => e.OffertaId).HasColumnName("offerta_id");

            builder.Property(e => e.Opzionale).HasColumnName("opzionale");

            builder.Property(e => e.RangeA).HasColumnName("range_a");

            builder.Property(e => e.RangeDa).HasColumnName("range_da");

            builder.Property(e => e.Tte)
                .HasColumnName("tte")
                .HasColumnType("datetime");

            builder.Property(e => e.Tts)
                .HasColumnName("tts")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            builder.Property(e => e.Value).HasColumnName("value");

            builder.HasOne(d => d.ComponentiFatturazione)
                .WithMany(p => p.OffertaComponentifatt)
                .HasForeignKey(d => d.ComponentiFatturazioneId)
                .HasConstraintName("fk_componenti_fatturazione");

            builder.HasOne(d => d.Offerta)
                .WithMany(p => p.OffertaComponentifatt)
                .HasForeignKey(d => d.OffertaId)
                .HasConstraintName("fk_offerta_componentifatt");
        }
    }
}