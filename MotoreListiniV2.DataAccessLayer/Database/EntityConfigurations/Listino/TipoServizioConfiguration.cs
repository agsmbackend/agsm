﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MotoreListiniV2.DataAccessLayer.Models.Listino;

namespace MotoreListiniV2.DataAccessLayer.Database.EntityConfigurations.Listino
{
    public class TipoServizioConfiguration : IEntityTypeConfiguration<TipoServizio>
    {
        public void Configure(EntityTypeBuilder<TipoServizio> builder)
        {
            builder.HasKey(e => e.TsTipoServizioId);

            builder.ToTable("tipo_servizio", SchemaName.Dbo);

            builder.Property(e => e.TsTipoServizioId)
                .HasColumnName("ts_tipo_servizio_id")
                .HasMaxLength(3)
                .ValueGeneratedNever();

            builder.Property(e => e.TsDescrizione)
                .HasColumnName("ts_descrizione")
                .HasMaxLength(255);
        }
    }
}