﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MotoreListiniV2.DataAccessLayer.Models.Listino;

namespace MotoreListiniV2.DataAccessLayer.Database.EntityConfigurations.Listino
{
    public class CategoriaConfiguration : IEntityTypeConfiguration<Categoria>
    {
        public void Configure(EntityTypeBuilder<Categoria> builder)
        {
            builder.HasKey(e => e.CatId);

            builder.ToTable("categoria", SchemaName.Listino);

            builder.HasIndex(e => new {e.CatDescrizione, e.TsTipoServizioId})
                .HasDatabaseName("ak_Cat_Desc")
                .IsUnique();

            builder.Property(e => e.CatId).HasColumnName("cat_id");

            builder.Property(e => e.TsTipoServizioId)
                .IsRequired()
                .HasColumnName("ts_tipo_servizio_id")
                .HasMaxLength(3);

            builder.Property(e => e.CatDescrizione)
                .HasColumnName("cat_descrizione")
                .HasMaxLength(255);

            builder.Property(e => e.Attiva)
                .IsRequired()
                .HasColumnName("attiva");

        }
    }
}