﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MotoreListiniV2.DataAccessLayer.Models.Listino;

namespace MotoreListiniV2.DataAccessLayer.Database.EntityConfigurations.Listino
{
    public class TipoPrezziConfiguration : IEntityTypeConfiguration<TipoPrezzi>
    {
        public void Configure(EntityTypeBuilder<TipoPrezzi> builder)
        {
            builder.ToTable("tipo_prezzi", SchemaName.Listino);

            builder.Property(e => e.TipoPrezziId).HasColumnName("tipo_prezzi_id");

            builder.Property(e => e.TipoPrezziCod)
                .HasColumnName("tipo_prezzi_cod")
                .HasMaxLength(255)
                .IsUnicode(false);

            builder.Property(e => e.TipoPrezziDescrizione)
                .HasColumnName("tipo_prezzi_descrizione")
                .HasMaxLength(500)
                .IsUnicode(false);

            builder.Property(e => e.TsTipoServizioId)
                .HasColumnName("ts_tipo_servizio_id")
                .HasMaxLength(5)
                .IsUnicode(false);

            builder.Property(e => e.Tte)
                .HasColumnName("tte")
                .HasColumnType("datetime");

            builder.Property(e => e.Tts)
                .HasColumnName("tts")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");
        }
    }
}