﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MotoreListiniV2.DataAccessLayer.Models.Listino;

namespace MotoreListiniV2.DataAccessLayer.Database.EntityConfigurations.Listino
{
    public class TipoCompRelConfiguration : IEntityTypeConfiguration<TipoCompRel>
    {
        public void Configure(EntityTypeBuilder<TipoCompRel> builder)
        {
            builder.ToTable("tipo_comp_rel", SchemaName.Listino);

            builder.Property(e => e.TipoCompRelId).HasColumnName("tipo_comp_rel_id");

            builder.Property(e => e.ComponentiPrezziId).HasColumnName("componenti_prezzi_id");

            builder.Property(e => e.LabelGrafica)
                .HasColumnName("label_grafica")
                .HasMaxLength(255)
                .IsUnicode(false);

            builder.Property(e => e.TipoPrezziId).HasColumnName("tipo_prezzi_id");

            builder.Property(e => e.Tte)
                .HasColumnName("tte")
                .HasColumnType("datetime");

            builder.Property(e => e.Tts)
                .HasColumnName("tts")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            builder.HasOne(d => d.ComponentiPrezzi)
                .WithMany(p => p.TipoCompRel)
                .HasForeignKey(d => d.ComponentiPrezziId)
                .HasConstraintName("fk_componenti_prezzirel");

            builder.HasOne(d => d.TipoPrezzi)
                .WithMany(p => p.TipoCompRel)
                .HasForeignKey(d => d.TipoPrezziId)
                .HasConstraintName("fk_tipo_prezzirel");
        }
    }
}