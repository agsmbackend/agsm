﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MotoreListiniV2.DataAccessLayer.Models.Listino;

namespace MotoreListiniV2.DataAccessLayer.Database.EntityConfigurations.Listino
{
    public class TipoTariffaConfiguration : IEntityTypeConfiguration<TipoTariffa>
    {
        public void Configure(EntityTypeBuilder<TipoTariffa> builder)
        {
            builder.HasKey(e => e.TariffaId);

            builder.ToTable("tipo_tariffa", SchemaName.Dbo);

            builder.Property(e => e.TariffaId).HasColumnName("tariffa_id");

            builder.Property(e => e.TariffaDesc)
                .IsRequired()
                .HasColumnName("tariffa_desc")
                .HasMaxLength(255);

            builder.Property(e => e.TsTipoServizioId)
                .HasColumnName("ts_tipo_servizio_id")
                .HasMaxLength(3);

            builder.HasOne(d => d.TsTipoServizio)
                .WithMany(p => p.TipoTariffa)
                .HasForeignKey(d => d.TsTipoServizioId)
                .HasConstraintName("fk_tipo_servizio1");
        }
    }
}