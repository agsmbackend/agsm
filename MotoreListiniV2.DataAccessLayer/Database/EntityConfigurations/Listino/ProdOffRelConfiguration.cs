﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MotoreListiniV2.DataAccessLayer.Models.Listino;

namespace MotoreListiniV2.DataAccessLayer.Database.EntityConfigurations.Listino
{
    public class ProdOffRelConfiguration : IEntityTypeConfiguration<ProdOffRel>
    {
        public void Configure(EntityTypeBuilder<ProdOffRel> builder)
        {
            builder.ToTable("Prod_Off_Rel", SchemaName.Listino);

            builder.Property(e => e.Prodoffrelid).HasColumnName("prodoffrelid");

            builder.Property(e => e.DataAAtt)
                .HasColumnName("dataA_Att")
                .HasColumnType("datetime");

            builder.Property(e => e.DataDaAtt)
                .HasColumnName("dataDa_Att")
                .HasColumnType("datetime");

            builder.Property(e => e.OffertaId).HasColumnName("offerta_id");

            builder.Property(e => e.ProdProdottoId).HasColumnName("prod_prodotto_id");

            builder.Property(e => e.Tte)
                .HasColumnName("tte")
                .HasColumnType("datetime");

            builder.Property(e => e.Tts)
                .HasColumnName("tts")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            builder.HasOne(d => d.Offerta)
                .WithMany(p => p.ProdOffRel)
                .HasForeignKey(d => d.OffertaId)
                .HasConstraintName("fk_offerta_off_rel");

            builder.HasOne(d => d.ProdProdotto)
                .WithMany(p => p.ProdOffRel)
                .HasForeignKey(d => d.ProdProdottoId)
                .HasConstraintName("fk_prodotto_off_rel");
        }
    }
}