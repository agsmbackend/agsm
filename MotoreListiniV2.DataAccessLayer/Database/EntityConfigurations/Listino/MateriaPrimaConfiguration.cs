﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MotoreListiniV2.DataAccessLayer.Models.Listino;

namespace MotoreListiniV2.DataAccessLayer.Database.EntityConfigurations.Listino
{
    public class MateriaPrimaConfiguration : IEntityTypeConfiguration<MateriaPrima>
    {
        public void Configure(EntityTypeBuilder<MateriaPrima> builder)
        {
            builder.HasKey(e => e.MpId);

            builder.ToTable("materia_prima", SchemaName.Dbo);

            builder.Property(e => e.MpId).HasColumnName("mp_id");

            builder.Property(e => e.MpDesc)
                .IsRequired()
                .HasColumnName("mp_desc")
                .HasMaxLength(255);

            builder.Property(e => e.TsTipoServizioId)
                .HasColumnName("ts_tipo_servizio_id")
                .HasMaxLength(3);

            builder.HasOne(d => d.TsTipoServizio)
                .WithMany(p => p.MateriePrime)
                .HasForeignKey(d => d.TsTipoServizioId)
                .HasConstraintName("FK_materia_prima_tipo_servizio");
        }
    }
}