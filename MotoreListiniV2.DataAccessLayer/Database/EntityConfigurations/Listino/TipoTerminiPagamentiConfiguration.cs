﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MotoreListiniV2.DataAccessLayer.Models.Listino;

namespace MotoreListiniV2.DataAccessLayer.Database.EntityConfigurations.Listino
{
    public class TipoTerminiPagamentiConfiguration : IEntityTypeConfiguration<TipoTerminiPagamenti>
    {
        public void Configure(EntityTypeBuilder<TipoTerminiPagamenti> builder)
        {
            builder.ToTable("tipo_termini_pagamenti", SchemaName.Dbo);

            builder.Property(e => e.TipoTerminiPagamentiId).HasColumnName("tipo_termini_pagamenti_id");

            builder.Property(e => e.TipoTerminiPagamentiCod)
                .HasColumnName("tipo_termini_pagamenti_cod")
                .HasMaxLength(50);

            builder.Property(e => e.TipoTerminiPagamentiDescrizione)
                .HasColumnName("tipo_termini_pagamenti_descrizione")
                .HasMaxLength(255);
        }
    }
}