﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MotoreListiniV2.DataAccessLayer.Models.Listino;

namespace MotoreListiniV2.DataAccessLayer.Database.EntityConfigurations.Listino
{
    public class AuditModificheConfiguration : IEntityTypeConfiguration<AuditModifiche>
    {
        public void Configure(EntityTypeBuilder<AuditModifiche> builder)
        {
            builder.ToTable("auditModifiche", SchemaName.Listino);

            builder.Property(e => e.Id).HasColumnName("id");

            builder.Property(e => e.DataModifica)
                .HasColumnName("dataModifica")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            builder.Property(e => e.KeyValues)
                .HasColumnName("keyValues")
                .IsUnicode(false);

            builder.Property(e => e.NewValues)
                .HasColumnName("newValues")
                .IsUnicode(false);

            builder.Property(e => e.OldValues)
                .HasColumnName("oldValues")
                .IsUnicode(false);

            builder.Property(e => e.OperationAudit)
                .HasColumnName("operationAudit")
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.TableName)
                .HasColumnName("tableName")
                .HasMaxLength(255)
                .IsUnicode(false);

            builder.Property(e => e.UserName)
                .HasColumnName("username")
                .HasMaxLength(255)
                .IsUnicode(false);
        }
    }
}