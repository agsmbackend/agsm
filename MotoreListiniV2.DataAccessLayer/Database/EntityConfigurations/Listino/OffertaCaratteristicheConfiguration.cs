﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MotoreListiniV2.DataAccessLayer.Models.Listino;

namespace MotoreListiniV2.DataAccessLayer.Database.EntityConfigurations.Listino
{
    public class OffertaCaratteristicheConfiguration : IEntityTypeConfiguration<OffertaCaratteristiche>
    {
        public void Configure(EntityTypeBuilder<OffertaCaratteristiche> builder)
        {
            builder.HasKey(e => e.OffCaratteristicheId);

            builder.ToTable("offerta_caratteristiche", SchemaName.Listino);

            builder.Property(e => e.OffCaratteristicheId).HasColumnName("off_caratteristiche_id");

            builder.Property(e => e.OffertaId).HasColumnName("offerta_id");

            builder.Property(e => e.CategoriaId).HasColumnName("categoria_id");

            builder.Property(e => e.ConfigurazioneId).HasColumnName("configurazione_id");

            builder.Property(e => e.FreeText).HasColumnName("free_text");

            builder.HasOne(d => d.Offerta)
                .WithMany(p => p.OffertaCaratteristiche)
                .HasForeignKey(d => d.OffertaId)
                .HasConstraintName("fk_offerta_prezzi");

            builder.HasOne(d => d.Categoria)
                .WithMany(p => p.OffertaCaratteristiche)
                .HasForeignKey(d => d.CategoriaId)
                .HasConstraintName("fk_offerta_categorie");

            builder.HasOne(d => d.Configurazione)
                .WithMany(p => p.OffertaCaratteristiche)
                .HasForeignKey(d => d.ConfigurazioneId)
                .HasConstraintName("fk_offerta_configurazioni");
        }
    }
}