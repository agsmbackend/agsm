﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MotoreListiniV2.DataAccessLayer.Models.Listino;

namespace MotoreListiniV2.DataAccessLayer.Database.EntityConfigurations.Listino
{
    public class ComponentiFatturazioneConfiguration : IEntityTypeConfiguration<ComponentiFatturazione>
    {
        public void Configure(EntityTypeBuilder<ComponentiFatturazione> builder)
        {
            builder.ToTable("componenti_fatturazione", SchemaName.Listino);

            builder.Property(e => e.ComponentiFatturazioneId).HasColumnName("componenti_fatturazione_id");

            builder.Property(e => e.ComponentiFatturazioneCod)
                .HasColumnName("componenti_fatturazione_cod")
                .HasMaxLength(255)
                .IsUnicode(false);

            builder.Property(e => e.ComponentiFatturazioneDescrizione)
                .HasColumnName("componenti_fatturazione_descrizione")
                .HasMaxLength(400)
                .IsUnicode(false);

            builder.Property(e => e.ComponentiFatturazioneNeta)
                .HasColumnName("componenti_fatturazione_neta")
                .HasMaxLength(100)
                .IsUnicode(false);

            builder.Property(e => e.ComponentiFatturazioneUnitamisura)
                .HasColumnName("componenti_fatturazione_unitamisura")
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.TsTipoServizioId)
                .HasColumnName("ts_tipo_servizio_id")
                .HasMaxLength(5)
                .IsUnicode(false);

            builder.Property(e => e.Tte)
                .HasColumnName("tte")
                .HasColumnType("datetime");

            builder.Property(e => e.Tts)
                .HasColumnName("tts")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");
        }
    }
}