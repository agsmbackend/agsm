﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MotoreListiniV2.DataAccessLayer.Models.Listino;

namespace MotoreListiniV2.DataAccessLayer.Database.EntityConfigurations.Listino
{
    public class OffertaToNetaOpConfiguration : IEntityTypeConfiguration<OffertaToNetaOp>
    {
        public void Configure(EntityTypeBuilder<OffertaToNetaOp> builder)
        {
            builder.HasKey(e => e.OffertanetaId);

            builder.ToTable("OffertaToNetaOp", SchemaName.Listino);

            builder.Property(e => e.OffertanetaId).HasColumnName("offertaneta_id");

            builder.Property(e => e.OffertaCod)
                .HasColumnName("offerta_cod")
                .HasMaxLength(50);

            builder.Property(e => e.OffertaNetasiu)
                .HasColumnName("offerta_netasiu")
                .HasMaxLength(100);

            builder.Property(e => e.ProdottoNetasiu)
                .HasColumnName("prodotto_netasiu")
                .HasMaxLength(100);

            builder.Property(e => e.ProdottoOffcodnetasiu)
                .HasColumnName("prodotto_offcodnetasiu")
                .HasMaxLength(100);

            builder.Property(e => e.Trattamento)
                .HasColumnName("trattamento")
                .HasMaxLength(10)
                .IsUnicode(false);

            builder.Property(e => e.Tte)
                .HasColumnName("tte")
                .HasColumnType("datetime");

            builder.Property(e => e.Tts)
                .HasColumnName("tts")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");
        }
    }
}