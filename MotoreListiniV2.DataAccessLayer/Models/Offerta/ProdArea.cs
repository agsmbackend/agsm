﻿using System;

namespace MotoreListiniV2.DataAccessLayer.Models.Offerta
{
    public class ProdArea
    {
        public int ProdProdottoId { get; set; }
        public int ArAreaId { get; set; }
        public DateTime Tts { get; set; }
        public DateTime? Tte { get; set; }
    }
}