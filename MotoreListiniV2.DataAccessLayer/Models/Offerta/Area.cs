﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MotoreListiniV2.DataAccessLayer.Models.Offerta
{
    [Table("area", Schema = "dbo")]
    public class Area
    {
        [Key]
        public int Ar_Area_Id { get; set; }

        public string Ar_Area_Cod { get; set; }
        public string Ar_Descrizione { get; set; }
        public DateTime? Tts { get; set; }
        public DateTime? Tte { get; set; }
    }
}