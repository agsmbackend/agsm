﻿using System;

namespace MotoreListiniV2.DataAccessLayer.Models.Offerta
{
    public class ProspectLam
    {
        public int ProspectLamId { get; set; }
        public int? PspsogSoggettoId { get; set; }
        public string ClienteRef { get; set; }
        public int? GgVal { get; set; }
        public DateTime? DataScadOff { get; set; }
        public string OraScadOff { get; set; }
        public string MinutiScadOff { get; set; }
        public DateTime? DataIniForn { get; set; }
        public DateTime? DataFinForn { get; set; }
        public int? ProdProdottoId { get; set; }
        public int? TipoPrezzoId { get; set; }
        public int? TipoAcciseId { get; set; }
        public string PrezzoGas { get; set; }
        public string TfGas { get; set; }
        public int? TipoBonus1 { get; set; }
        public int? TipoBonus2 { get; set; }
        public string FornitoreRef { get; set; }
        public string VolumeAnnuo { get; set; }
        public string VolumeContrattuale { get; set; }
        public string Alfa { get; set; }
        public int? TipoTerminiPagamentiId { get; set; }
        public int? TipoModalitaPagamentiId { get; set; }
        public int? TipoGaranziaId { get; set; }
        public int? MesiGaranzia { get; set; }
        public string Garanzia { get; set; }
        public int? TipoRecessoId { get; set; }
        public string CondPart { get; set; }
        public int? SogSoggettoCreate { get; set; }
        public string TsTipoServizioId { get; set; }
        public int? WsdDefWfStepId { get; set; }
        public DateTime? DataInsert { get; set; }
        public int? ProspectLamIdStorico { get; set; }
        public int? Versione { get; set; }
        public string Allegato { get; set; }
        public string AllegatoEmail { get; set; }
        public string BonusPagamenti { get; set; }
        public int? TipoFasceId { get; set; }
        public int? TipoSogId { get; set; }
        public string NomeOfferta { get; set; }
        public bool? IsOffertLam { get; set; }
        public int? ValoreOfferta { get; set; }
        public int? LivelloAutorizzazioneId { get; set; }
    }
}