﻿using System;
using GenFu;
using GenFu.ValueGenerators.Internet;

namespace MotoreListiniV2.DataAccessLayer.Models.FakeData
{
    public class WebAddressFiller : PropertyFiller<string>
    {
        public WebAddressFiller() : base(
                                        new[] { "object" },
                                        new[] { "website", "web", "webaddress", "sitehost" })
        {
        }

        public override object GetValue(object instance)
        {
            var domain = Domains.DomainName();

            return $"https://www.{domain}";
        }
    }

    public class ProductFiller : PropertyFiller<string>
    {
        protected static readonly Random Random = new(Environment.TickCount);

        private readonly string[] _nomeProdotto = { "PRODOTTO_111", "PRODOTTO_112", "PRODOTTO_113", "PRODOTTO_114", "PRODOTTO_115", "PRODOTTO_116", "PRODOTTO_117", "PRODOTTO_118",
                                                                "PRODOTTO_119", "PRODOTTO_120", "PRODOTTO_121", "PRODOTTO_122", "PRODOTTO_123", "PRODOTTO_124" };

        //second value is the property name to fill
        public ProductFiller() : base(new[] { "object" }, new[] { "nomeprodotto" })
        {
        }

        public override object GetValue(object instance)
        {
            //random value from array
            return _nomeProdotto[Random.Next(0, _nomeProdotto.Length)];
        }
    }

    public class ListinoFiller : PropertyFiller<string>
    {
        protected static readonly Random random = new Random(Environment.TickCount);

        private readonly string[] NomeListino = { "LISTINO_111", "LISTINO_112", "LISTINO_113", "LISTINO_114", "LISTINO_115", "LISTINO_116", "LISTINO_117", "LISTINO_118",
                                                                "LISTINO_119", "LISTINO_120", "LISTINO_121", "LISTINO_122", "LISTINO_123", "LISTINO_124" };

        //second value is the property name to fill
        public ListinoFiller() : base(new[] { "object" }, new[] { "codicelistino" })
        {
        }

        public override object GetValue(object instance)
        {
            //random value from array
            return NomeListino[random.Next(0, NomeListino.Length)];
        }
    }
}