﻿using System.Collections.Generic;
using GenFu;

namespace MotoreListiniV2.DataAccessLayer.Models.FakeData
{
    public class DataGeneratorService<T> : IDataGenerator<T>
       where T : class, new()
    {
        public List<T> Collection() => A.ListOf<T>();

        public List<T> Collection(int length) => A.ListOf<T>(length);

        public T Instance() => A.New<T>();
    }
}