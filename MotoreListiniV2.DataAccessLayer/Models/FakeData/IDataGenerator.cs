﻿using System.Collections.Generic;

namespace MotoreListiniV2.DataAccessLayer.Models.FakeData
{
    public interface IDataGenerator<T> where T : class
    {
        List<T> Collection();

        List<T> Collection(int length);

        T Instance();
    }
}