﻿using System.Collections.Generic;

namespace MotoreListiniV2.DataAccessLayer.Models.Listino
{
    public class Prodotto
    {
        public Prodotto()
        {
            OffertaPrezzi = new HashSet<OffertaPrezzi>();
            ProdOffRel = new HashSet<ProdOffRel>();
        }

        public int ProdProdottoId { get; set; }
        public string ProdDescrizione { get; set; }
        public string ProdNote { get; set; }
        public string TsTipoServizioId { get; set; }
        public string Tariffa { get; set; }
        public bool? HavePaperless { get; set; }
        public bool? HaveIban { get; set; }
        public bool OfferingLam { get; set; }

        public virtual ICollection<OffertaPrezzi> OffertaPrezzi { get; set; }
        public virtual ICollection<ProdOffRel> ProdOffRel { get; set; }
    }
}