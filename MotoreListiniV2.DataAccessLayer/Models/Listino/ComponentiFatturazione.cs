﻿using System;
using System.Collections.Generic;

namespace MotoreListiniV2.DataAccessLayer.Models.Listino
{
    public class ComponentiFatturazione
    {
        public ComponentiFatturazione()
        {
            OffertaComponentifatt = new HashSet<OffertaComponentifatt>();
        }

        public int ComponentiFatturazioneId { get; set; }
        public string ComponentiFatturazioneCod { get; set; }
        public string ComponentiFatturazioneDescrizione { get; set; }
        public string ComponentiFatturazioneUnitamisura { get; set; }
        public string ComponentiFatturazioneNeta { get; set; }
        public string TsTipoServizioId { get; set; }
        public DateTime Tts { get; set; }
        public DateTime? Tte { get; set; }

        public virtual ICollection<OffertaComponentifatt> OffertaComponentifatt { get; set; }
    }
}