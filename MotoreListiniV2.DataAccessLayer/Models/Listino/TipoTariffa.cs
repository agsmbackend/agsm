﻿namespace MotoreListiniV2.DataAccessLayer.Models.Listino
{
    public class TipoTariffa
    {
        public int TariffaId { get; set; }
        public string TariffaDesc { get; set; }
        public string TsTipoServizioId { get; set; }

        public virtual TipoServizio TsTipoServizio { get; set; }
    }
}