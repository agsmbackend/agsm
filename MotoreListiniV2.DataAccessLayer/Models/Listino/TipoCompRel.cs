﻿using System;

namespace MotoreListiniV2.DataAccessLayer.Models.Listino
{
    public class TipoCompRel
    {
        public int TipoCompRelId { get; set; }
        public int? TipoPrezziId { get; set; }
        public int? ComponentiPrezziId { get; set; }
        public string LabelGrafica { get; set; }
        public DateTime Tts { get; set; }
        public DateTime? Tte { get; set; }

        public virtual ComponentiPrezzi ComponentiPrezzi { get; set; }
        public virtual TipoPrezzi TipoPrezzi { get; set; }
    }
}