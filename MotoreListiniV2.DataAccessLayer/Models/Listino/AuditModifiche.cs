﻿using System;

namespace MotoreListiniV2.DataAccessLayer.Models.Listino
{
    public class AuditModifiche
    {
        public int Id { get; set; }
        public string TableName { get; set; }
        public DateTime DataModifica { get; set; }
        public string KeyValues { get; set; }
        public string OldValues { get; set; }
        public string NewValues { get; set; }
        public string OperationAudit { get; set; }
        public string UserName { get; set; }
    }
}