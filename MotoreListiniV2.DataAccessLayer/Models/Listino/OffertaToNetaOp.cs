﻿using System;

namespace MotoreListiniV2.DataAccessLayer.Models.Listino
{
    public class OffertaToNetaOp
    {
        public int OffertanetaId { get; set; }
        public string OffertaCod { get; set; }
        public string OffertaNetasiu { get; set; }
        public string ProdottoNetasiu { get; set; }
        public string ProdottoOffcodnetasiu { get; set; }
        public DateTime Tts { get; set; }
        public DateTime? Tte { get; set; }
        public string Trattamento { get; set; }
    }
}