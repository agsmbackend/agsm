﻿using System;

namespace MotoreListiniV2.DataAccessLayer.Models.Listino
{
    public class OffertaPrezzi
    {
        public int OffPrezziId { get; set; }
        public int? OffertaId { get; set; }
        public int? ProdProdottoId { get; set; }
        public int? TipoPrezziId { get; set; }
        public int? ComponentiPrezziId { get; set; }
        public double? Prezzo { get; set; }
        public int? RangeDa { get; set; }
        public int? RangeA { get; set; }
        public DateTime DataInsert { get; set; }
        public DateTime Tts { get; set; }
        public DateTime? Tte { get; set; }
        public int Percapplicata { get; set; }

        public virtual ComponentiPrezzi ComponentiPrezzi { get; set; }
        public virtual ProdottoOfferta Offerta { get; set; }
        public virtual Prodotto ProdProdotto { get; set; }
        public virtual TipoPrezzi TipoPrezzi { get; set; }
    }
}