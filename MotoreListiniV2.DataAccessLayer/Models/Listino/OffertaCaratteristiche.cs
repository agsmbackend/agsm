﻿using System;

namespace MotoreListiniV2.DataAccessLayer.Models.Listino
{
    public class OffertaCaratteristiche
    {
        public int OffCaratteristicheId { get; set; }
        public int? OffertaId { get; set; }
        public int? CategoriaId { get; set; }
        public int? ConfigurazioneId { get; set; }
        public string FreeText { get; set; }

        public virtual ProdottoOfferta Offerta { get; set; }
        public virtual Categoria Categoria { get; set; }
        public virtual Configurazione Configurazione { get; set; }

    }
}