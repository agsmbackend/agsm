﻿using System;
using System.Collections.Generic;

namespace MotoreListiniV2.DataAccessLayer.Models.Listino
{
    public class TipoPrezzi
    {
        public TipoPrezzi()
        {
            OffertaPrezzi = new HashSet<OffertaPrezzi>();
            TipoCompRel = new HashSet<TipoCompRel>();
        }

        public int TipoPrezziId { get; set; }
        public string TipoPrezziCod { get; set; }
        public string TipoPrezziDescrizione { get; set; }
        public string TsTipoServizioId { get; set; }
        public DateTime Tts { get; set; }
        public DateTime? Tte { get; set; }

        public virtual ICollection<OffertaPrezzi> OffertaPrezzi { get; set; }
        public virtual ICollection<TipoCompRel> TipoCompRel { get; set; }
    }
}