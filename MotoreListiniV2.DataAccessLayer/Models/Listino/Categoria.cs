﻿using System.Collections.Generic;

namespace MotoreListiniV2.DataAccessLayer.Models.Listino
{
    public class Categoria
    {
        public Categoria()
        {
            Configurazioni = new HashSet<Configurazione>();
            OffertaCaratteristiche = new HashSet<OffertaCaratteristiche>();
        }

        public int CatId { get; set; }
        public string TsTipoServizioId { get; set; }
        public string CatDescrizione { get; set; }
        public bool Attiva { get; set; }

        public virtual ICollection<Configurazione> Configurazioni { get; set; }
        public virtual ICollection<OffertaCaratteristiche> OffertaCaratteristiche { get; set; }

    }
}