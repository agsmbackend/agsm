﻿using System.Collections.Generic;

namespace MotoreListiniV2.DataAccessLayer.Models.Listino
{
    public class ProdottoOfferta
    {
        public ProdottoOfferta()
        {
            OffertaComponentifatt = new HashSet<OffertaComponentifatt>();
            OffertaPrezzi = new HashSet<OffertaPrezzi>();
            ProdOffRel = new HashSet<ProdOffRel>();
            OffertaCaratteristiche = new HashSet<OffertaCaratteristiche>();
        }

        public int OffertaId { get; set; }
        public string OffertaCod { get; set; }
        public string OffertaNome { get; set; }
        public string OffertaDescrizione { get; set; }
        public string SchedaConfrontabilita { get; set; }
        public string SchedaProdotto { get; set; }
        public int? Mese { get; set; }
        public int? Anno { get; set; }
        public int? Rev { get; set; }
        public int? Trim { get; set; }
        public string OffertaNote { get; set; }
        public bool ScopaDataatt { get; set; }
        public string Username { get; set; }
        public int? TipoTerminiPagamentiId { get; set; }
        public bool Flagindtofix { get; set; }

        //Nuovi campi
        public string Uso { get; set; }
        public string MateriaPrima { get; set; }
        public float? PrezzoEnergia { get; set; }
        public int? PercMateriaEnergia { get; set; }
        public int? PercComponenteEnergia { get; set; }
        public int? PercSpesaTrasportoGestione { get; set; }
        public int? PercOneriSistema{ get; set; }
        public int? PercAsos { get; set; }
        public int? PercCompExtra1 { get; set; }
        public int? PercCompExtra2 { get; set; }
        public int? PercCompExtra3 { get; set; }

        public virtual TipoTerminiPagamenti TipoTerminiPagamenti { get; set; }
        public virtual ICollection<OffertaComponentifatt> OffertaComponentifatt { get; set; }
        public virtual ICollection<OffertaPrezzi> OffertaPrezzi { get; set; }
        public virtual ICollection<ProdOffRel> ProdOffRel { get; set; }
        public virtual ICollection<OffertaCaratteristiche> OffertaCaratteristiche { get; set; }
    }
}