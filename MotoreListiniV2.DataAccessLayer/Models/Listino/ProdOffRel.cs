﻿using System;

namespace MotoreListiniV2.DataAccessLayer.Models.Listino
{
    public class ProdOffRel
    {
        public int Prodoffrelid { get; set; }
        public int? ProdProdottoId { get; set; }
        public int? OffertaId { get; set; }
        public DateTime Tts { get; set; }
        public DateTime? Tte { get; set; }
        public DateTime? DataDaAtt { get; set; }
        public DateTime? DataAAtt { get; set; }

        public virtual ProdottoOfferta Offerta { get; set; }
        public virtual Prodotto ProdProdotto { get; set; }
    }
}