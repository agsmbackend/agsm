﻿using System.Collections.Generic;

namespace MotoreListiniV2.DataAccessLayer.Models.Listino
{
    public class TipoTerminiPagamenti
    {
        public TipoTerminiPagamenti()
        {
            ProdottoOfferta = new HashSet<ProdottoOfferta>();
        }

        public int TipoTerminiPagamentiId { get; set; }
        public string TipoTerminiPagamentiCod { get; set; }
        public string TipoTerminiPagamentiDescrizione { get; set; }

        public virtual ICollection<ProdottoOfferta> ProdottoOfferta { get; set; }
    }
}