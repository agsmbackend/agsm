﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Newtonsoft.Json;

namespace MotoreListiniV2.DataAccessLayer.Models.Listino
{
    public class AuditEntry
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AuditEntry(EntityEntry entry)
        {
            Entry = entry;
        }

        public AuditEntry(EntityEntry entry, IHttpContextAccessor httpContextAccessor)
        {
            Entry = entry;
            _httpContextAccessor = httpContextAccessor;
        }

        public EntityEntry Entry { get; }
        public string TableName { get; set; }
        public Dictionary<string, object> KeyValues { get; } = new Dictionary<string, object>();
        public Dictionary<string, object> OldValues { get; } = new Dictionary<string, object>();
        public Dictionary<string, object> NewValues { get; } = new Dictionary<string, object>();
        public List<PropertyEntry> TemporaryProperties { get; } = new List<PropertyEntry>();
        public EntityState Operation { get; set; }

        public bool HasTemporaryProperties => TemporaryProperties.Any();

        public AuditModifiche ToAudit()
        {
            var audit = new AuditModifiche
            {
                TableName = TableName,
                DataModifica = DateTime.Now,
                KeyValues = JsonConvert.SerializeObject(KeyValues),
                OldValues = OldValues.Count == 0 ? null : JsonConvert.SerializeObject(OldValues),
                NewValues = NewValues.Count == 0 ? null : JsonConvert.SerializeObject(NewValues),
                OperationAudit = Operation.ToString(),
                UserName = _httpContextAccessor?.HttpContext?.User?.Identity?.Name
            };

            return audit;
        }
    }
}