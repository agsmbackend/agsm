﻿namespace MotoreListiniV2.DataAccessLayer.Models.Listino
{
    public class Uso
    {
        public int UsoId { get; set; }
        public string UsoDesc { get; set; }
        public string TsTipoServizioId { get; set; }

        public virtual TipoServizio TsTipoServizio { get; set; }
    }
}