﻿namespace MotoreListiniV2.DataAccessLayer.Models.Listino
{
    public class MateriaPrima
    {
        public int MpId { get; set; }
        public string MpDesc { get; set; }
        public string TsTipoServizioId { get; set; }

        public virtual TipoServizio TsTipoServizio { get; set; }
    }
}