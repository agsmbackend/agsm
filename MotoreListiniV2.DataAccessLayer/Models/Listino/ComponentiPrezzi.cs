﻿using System.Collections.Generic;

namespace MotoreListiniV2.DataAccessLayer.Models.Listino
{
    public class ComponentiPrezzi
    {
        public ComponentiPrezzi()
        {
            OffertaPrezzi = new HashSet<OffertaPrezzi>();
            TipoCompRel = new HashSet<TipoCompRel>();
        }

        public int ComponentiPrezziId { get; set; }
        public string ComponentiPrezziCod { get; set; }
        public string ComponentiPrezziDescrizione { get; set; }
        public string Unitamisura { get; set; }

        public virtual ICollection<OffertaPrezzi> OffertaPrezzi { get; set; }
        public virtual ICollection<TipoCompRel> TipoCompRel { get; set; }
    }
}