﻿using System.Collections.Generic;

namespace MotoreListiniV2.DataAccessLayer.Models.Listino
{
    public class TipoServizio
    {
        public TipoServizio()
        {
            TipoTariffa = new HashSet<TipoTariffa>();
            Usi = new HashSet<Uso>();
            MateriePrime = new HashSet<MateriaPrima>();
        }

        public string TsTipoServizioId { get; set; }
        public string TsDescrizione { get; set; }

        public virtual ICollection<TipoTariffa> TipoTariffa { get; set; }
        public virtual ICollection<Uso> Usi { get; set; }
        public virtual ICollection<MateriaPrima> MateriePrime { get; set; }
    }
}