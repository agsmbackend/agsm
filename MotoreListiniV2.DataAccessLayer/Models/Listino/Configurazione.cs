﻿using System.Collections.Generic;

namespace MotoreListiniV2.DataAccessLayer.Models.Listino
{
    public class Configurazione
    {
        public Configurazione()
        {
            OffertaCaratteristiche = new HashSet<OffertaCaratteristiche>();
        }

        public int ConfId { get; set; }
        public int? CatId { get; set; }
        public string ConfNome { get; set; }
        public string ConfTesto { get; set; }
        public int ConfTextMaxLength { get; set; }
        public virtual Categoria Categoria { get; set; }
        public virtual ICollection<OffertaCaratteristiche> OffertaCaratteristiche { get; set; }

    }
}