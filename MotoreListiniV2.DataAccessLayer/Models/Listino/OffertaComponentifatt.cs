﻿using System;

namespace MotoreListiniV2.DataAccessLayer.Models.Listino
{
    public class OffertaComponentifatt
    {
        public int OffFattId { get; set; }
        public int? OffertaId { get; set; }
        public int? ComponentiFatturazioneId { get; set; }
        public double? Value { get; set; }
        public int? RangeDa { get; set; }
        public int? RangeA { get; set; }
        public bool Opzionale { get; set; }
        public DateTime DataInsert { get; set; }
        public DateTime Tts { get; set; }
        public DateTime? Tte { get; set; }

        public virtual ComponentiFatturazione ComponentiFatturazione { get; set; }
        public virtual ProdottoOfferta Offerta { get; set; }
    }
}