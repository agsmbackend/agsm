using System;
using Microsoft.Extensions.Configuration;
using UtilitiesGSA.Shareds;

namespace MotoreListiniV2.BusinessLayer.Helper
{
    public static class IoHelper
    {
        public static string EnsureCorrectFilename(string filename)
        {
            if (filename.Contains(CommonChars.Backslash))
            {
                var filenameCorrect = filename.AsSpan()[..(filename.LastIndexOf(CommonChars.Backslash) + 1)];
                return filenameCorrect.ToString();
            }

            return filename;
        }

        public static string GetPathListini(IConfiguration config,string filename)
        {
            return $"{config.GetSection("UrlListini").Value}{filename}";
        }

        public static string GetPathSchedeGenerate(IConfiguration config, string filename)
        {
            return $"{config.GetSection("UrlSchedeGenerate").Value}{filename}";
        }

        public static string GetPathTemplates(IConfiguration config, string filename)
        {
            return $"{config.GetSection("UrlTemplates").Value}{filename}";
        }
    }
}