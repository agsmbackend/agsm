using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using MotoreListiniV2.Shared.Dto;
using MotoreListiniV2.Shared.ResponseBase;
using Newtonsoft.Json.Linq;

namespace MotoreListiniV2.BusinessLayer.Contracts
{
    public interface IMotoreListiniOfferteWriter
    {
        Task<ResponseTyped<string>> PostPdfUploader(IFormFile allegato);

        Task<ResponseTyped<string>> PostSaveFullListinoEntity(SaveFullListinoEntity listino);

        Task<ResponseTyped<string>> PostAnticipaChiusuraListino(JObject infoAnticipaData);

        Task<ResponseTyped<string>> PostSaveCategoriaEntity(SaveCategoriaEntity categoria);

        Task<ResponseTyped<string>> PostSaveConfigurazioneEntity(SaveConfigurazioneEntity configurazione);

        Task<ResponseTyped<int>> PostSaveSetupOfferta(SaveSetupEntity setup);

        Task<ResponseTyped<string>> DeleteConfigurazioneEntity(int idConfigurazione);

        Task<ResponseTyped<int>> PostSaveListinoEntity(FinalizeListinoEntity listino);

        //Task<ResponseTyped<string>> PostSaveListinoEntityTest();
    }
}