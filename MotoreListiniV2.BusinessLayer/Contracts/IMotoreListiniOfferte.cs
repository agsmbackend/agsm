﻿namespace MotoreListiniV2.BusinessLayer.Contracts
{
    public interface IMotoreListiniOfferte : IMotoreListiniOfferteReader, IMotoreListiniOfferteWriter
    {
    }
}