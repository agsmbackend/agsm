﻿using System.Threading.Tasks;
using MotoreListiniV2.Shared.ResponseBase;

namespace MotoreListiniV2.BusinessLayer.Contracts
{
    public interface ITransformerOfferta2Listino
    {
        Task<ResponseTyped<string>> CreateListinoByOfferta(int idOfferta);
    }
}