using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using MotoreListiniV2.Shared.Dto;
using MotoreListiniV2.Shared.ResponseBase;

namespace MotoreListiniV2.BusinessLayer.Contracts
{
    public interface IMotoreListiniOfferteReader
    {
        Task<ResponseTyped<IEnumerable<ProdottoView>>> GetTipoPrezzo(string tipoServizio);

        Task<ResponseTyped<IEnumerable<TipoOffertaView>>> GetTipoOfferta(string tipoServizio);

        Task<ResponseTyped<IEnumerable<TipoTerminiPagamentiView>>> GetListTerminiPagamento();

        Task<ResponseTyped<IEnumerable<ComponentiPrezziView>>> GetComponentiPrezzi(int idTipoOfferta);

        Task<ResponseTyped<IEnumerable<ComponentiPrezziGenericiView>>> GetComponentiPrezziGenerici(string tipoServizio);

        Task<ResponseTyped<IEnumerable<ComponentiFatturazioneView>>> GetComponentiFatturazione(string tipoServizio);

        Task<ResponseTyped<IEnumerable<ElencoListiniFullView>>> GetElencoListiniPanel(DateTime dataValiditaDa,
            DateTime dataValidataA, string prodotto, string listino);

        Task<ResponseTyped<IEnumerable<OffertaPrezziView>>> GetPrezziByListino(int idListino);

        Task<ResponseTyped<IEnumerable<OffertaComponentiFatturazioneView>>> GetComponentiFattByListino(int idListino);

        Task<ResponseTyped<DataTable>> GetElencoListiniExcel(List<int> idsListini);

        Task<ResponseTyped<IEnumerable<ElencoCategorieFullView>>> GetElencoCategorie(string descrizione,
            string grado, string tipo_servizio);

        Task<ResponseTyped<IEnumerable<ElencoCategorieOffertaFullView>>> GetElencoCategorieOfferta(int? idOfferta,
            string grado, string tipo_servizio);

        Task<ResponseTyped<IEnumerable<TipoTariffaView>>> GetTipiTariffa(int idServizio);

        Task<ResponseTyped<IEnumerable<UsoView>>> GetUsi(int idServizio);

        Task<ResponseTyped<IEnumerable<MateriaPrimaView>>> GetMateriePrime (int idServizio);

        Task<ResponseTyped<ElencoCaratteristicheFullView>> GetCaratteristicheOfferta(int idOfferta);

        Task<ResponseTyped<IEnumerable<ElencoListiniView>>> GetElencoListini(DateTime dataValiditaDa,
            DateTime dataValidataA, string prodotto, string listino);
    }
}