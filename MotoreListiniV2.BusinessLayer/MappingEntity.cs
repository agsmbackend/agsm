﻿using System;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using MotoreListiniV2.DataAccessLayer.Database;
using MotoreListiniV2.DataAccessLayer.Models.Listino;
using MotoreListiniV2.DataAccessLayer.Models.Offerta;
using MotoreListiniV2.Shared.Dto;
using UtilitiesGSA.Extensions;

namespace MotoreListiniV2.BusinessLayer
{
    public class MappingEntity : Profile
    {
        public MappingEntity(IWebHostEnvironment env)

        {
            var environment = env ?? throw new ArgumentNullException(nameof(env));

            CreateMap<TipoCompRel, ComponentiPrezziView>()
                .ForMember(x => x.ComponentiPrezziId, y => y.MapFrom(j => j.ComponentiPrezziId))
                .ForMember(x => x.Unitamisura, y => y.MapFrom(j => j.ComponentiPrezzi.Unitamisura))
                .ForMember(x => x.LabelGrafica, y => y.MapFrom(j => j.LabelGrafica));

            CreateMap<TipoCompRel, ComponentiPrezziGenericiView>()
                .ForMember(x => x.Id, y => y.MapFrom(j => j.ComponentiPrezziId))
                .ForMember(x => x.Nome, y => y.MapFrom(j => j.ComponentiPrezzi.ComponentiPrezziCod))
                .ForMember(x => x.Unitamisura, y => y.MapFrom(j => j.ComponentiPrezzi.Unitamisura));

            CreateMap<ProdOffRel, ElencoListiniFullView>()
                .ForMember(x => x.IdProdotto, y => y.MapFrom(j => j.ProdProdottoId))
                .ForMember(x => x.Prodotto, y => y.MapFrom(j => j.ProdProdotto.ProdDescrizione))
                .ForMember(x => x.TipoServizio, y => y.MapFrom(j => j.ProdProdotto.TsTipoServizioId))
                .ForMember(x => x.Tariffa, y => y.MapFrom(j => j.ProdProdotto.Tariffa))
                .ForMember(x => x.IdListino, y => y.MapFrom(j => j.OffertaId))
                .ForMember(x => x.CodOfferta, y => y.MapFrom(j => j.Offerta.OffertaCod))
                .ForMember(x => x.Descrizione, y => y.MapFrom(j => j.Offerta.OffertaNote))
                .ForMember(x => x.Scheda_prodotto,
                    y => y.MapFrom(j =>
                        !environment.IsDevelopment()
                            ? $"https://servizionline.agsm.it/retivendita/Files/PDF_Files/scheda_prodotto/{j.Offerta.SchedaProdotto}"
                            : $"https://servizionline.agsm.it/test_retivendita/Files/PDF_Files/scheda_prodotto/{j.Offerta.SchedaProdotto}"))
                .ForMember(x => x.Mese, y => y.MapFrom(j => j.Offerta.Mese))
                .ForMember(x => x.Anno, y => y.MapFrom(j => j.Offerta.Anno))
                .ForMember(x => x.Revisione, y => y.MapFrom(j => j.Offerta.Rev))
                .ForMember(x => x.Trimestre, y => y.MapFrom(j => j.Offerta.Trim))
                .ForMember(x => x.Username, y => y.MapFrom(j => j.Offerta.Username))
                .ForMember(x => x.TerminiPagamento, y => y.MapFrom(j => j.Offerta.TipoTerminiPagamentiId))
                .ForMember(x => x.ValiditaPrezzo, y => y.MapFrom(j => j.Offerta.ScopaDataatt))
                .ForMember(x => x.PassaFisso, y => y.MapFrom(j => j.Offerta.Flagindtofix))
                .ForMember(x => x.InizioValidita, y => y.MapFrom(j => j.Tts))
                .ForMember(x => x.FineValidita, y => y.MapFrom(j => j.Tte ?? new DateTime(9999, 12, 31)))
                .ForMember(x => x.InizioAttivazione, y => y.MapFrom(j => j.DataDaAtt ?? new DateTime(1900, 1, 1)))
                .ForMember(x => x.FineAttivazione, y => y.MapFrom(j => j.DataAAtt ?? new DateTime(9999, 12, 31)))
                .ForMember(x => x.IsEditable,
                    y => y.MapFrom(j => ListinoContext.UfnCheckListinoAlreadyApplicato(j.Offerta.OffertaCod) == 0));

            CreateMap<OffertaPrezzi, OffertaPrezziView>()
                .ForMember(x => x.TipoPrezziId, y => y.MapFrom(j => j.ProdProdottoId))
                .ForMember(x => x.TipoPrezzo, y => y.MapFrom(j => j.ProdProdotto.ProdDescrizione))
                .ForMember(x => x.IdTipoOfferta, y => y.MapFrom(j => j.TipoPrezziId))
                .ForMember(x => x.ValMeseDa, y => y.MapFrom(j => j.RangeDa))
                .ForMember(x => x.ValMeseA, y => y.MapFrom(j => j.RangeA))
                .ForMember(x => x.Percentuale, y => y.MapFrom(j => j.Percapplicata));

            /*
                .ForMember(x => x.OffComponentePrezzi,
                opt => opt.MapFrom(src => new List<OffertaComponentePrezziView>() { new OffertaComponentePrezziView() {
                                                    IdPrezzo = src.OffPrezziId,
                                                    ComponentiPrezziId = src.ComponentiPrezziId ?? -1,
                                                    LabelGrafica = src.TipoPrezzi.TipoCompRel.Where(x => x.ComponentiPrezziId == src.ComponentiPrezziId).Select(ris => ris.LabelGrafica).FirstOrDefault(),
                                                    Prezzo = src.Prezzo ?? 0,
                                                    UnitaMisuraPrezzo = src.ComponentiPrezzi.Unitamisura
                                                    } }));
                                                    */

            CreateMap<OffertaPrezzi, OffertaComponentePrezziView>()
                .ForMember(x => x.IdPrezzo, y => y.MapFrom(j => j.OffPrezziId))
                .ForMember(x => x.ComponentiPrezziId, y => y.MapFrom(j => j.ComponentiPrezziId))
                .ForMember(x => x.LabelGrafica,
                    y => y.MapFrom(j =>
                        j.TipoPrezzi.TipoCompRel.Where(x => x.ComponentiPrezziId == j.ComponentiPrezziId)
                            .Select(ris => ris.LabelGrafica).FirstOrDefault()))
                .ForMember(x => x.Prezzo, y => y.MapFrom(j => j.Prezzo))
                .ForMember(x => x.UnitaMisuraPrezzo, y => y.MapFrom(j => j.ComponentiPrezzi.Unitamisura));

            CreateMap<OffertaComponentifatt, OffertaComponentiFatturazioneView>()
                .ForMember(x => x.IdOffertaComponente, y => y.MapFrom(j => j.OffFattId))
                .ForMember(x => x.IdComponente, y => y.MapFrom(j => j.ComponentiFatturazioneId))
                .ForMember(x => x.Componente,
                    y => y.MapFrom(j => j.ComponentiFatturazione.ComponentiFatturazioneDescrizione))
                .ForMember(x => x.PrezzoAltraComp, y => y.MapFrom(j => j.Value))
                .ForMember(x => x.UnitaMisuraAltraComp,
                    y => y.MapFrom(j => j.ComponentiFatturazione.ComponentiFatturazioneUnitamisura))
                .ForMember(x => x.ValMeseDaAltraComp, y => y.MapFrom(j => j.RangeDa))
                .ForMember(x => x.ValMeseAAltraComp, y => y.MapFrom(j => j.RangeA))
                .ForMember(x => x.IsOpzionaleAltraComp, y => y.MapFrom(j => j.Opzionale));

            //OffertaToListino
            CreateMap<ProspectLam, SaveFullListinoEntity>()
                .ForMember(x => x.IdProdotto, y => y.MapFrom(j => -1))
                .ForMember(x => x.IdListino, y => y.MapFrom(j => -1))
                .ForMember(x => x.NomeProdotto, y => y.MapFrom(j => "LISTINO_OFF_" + j.ProspectLamId))
                .ForMember(x => x.CodiceListino, y => y.MapFrom(j => "LISTINO_OFF_" + j.ProspectLamId))
                .ForMember(x => x.TipoServizio, y => y.MapFrom(j => j.TsTipoServizioId))
                .ForMember(x => x.TipoTariffa, y => y.MapFrom(j => "AU"))
                .ForMember(x => x.Descrizione,
                    y => y.MapFrom(j => "Listino generato dall'offerta personalizzata identificativo " + j.ProspectLamId))
                .ForMember(x => x.Mese, y => y.MapFrom(j => DateTime.Now.Month))
                .ForMember(x => x.Anno, y => y.MapFrom(j => DateTime.Now.Year))
                .ForMember(x => x.ValiditaPrezzo, y => y.MapFrom(j => false))
                .ForMember(x => x.Revisione, y => y.MapFrom(j => 1))
                .ForMember(x => x.Trimestre,
                    y => y.MapFrom(j =>
                        ((int) Math.Round((double) DateTime.Now.Month / 3)) == 0
                            ? 1
                            : (int) Math.Round((double) DateTime.Now.Month / 3)))
                .ForMember(x => x.InizioValidita, y => y.MapFrom(j => DateTime.Now.Date))
                .ForMember(x => x.FineValidita,
                    y => y.MapFrom(j => DateTime.Now.Date.AddDays(10).AddHours(23).AddMinutes(59).AddSeconds(59)))
                .ForMember(x => x.InizioAttivazione, y => y.MapFrom(j => j.DataIniForn))
                .ForMember(x => x.FineAttivazione, y => y.MapFrom(j => j.DataFinForn))
                .ForMember(x => x.TerminiPagamento,
                    y => y.MapFrom(j => j.TipoTerminiPagamentiId == 1 ? 2 : j.TipoTerminiPagamentiId))
                .ForMember(x => x.Fixed, y => y.MapFrom(j => false))
                .ForMember(x => x.SpPDF, y => y.MapFrom(j => string.Empty))
                ;

            //estraggo record prezzo dall'offerta
            CreateMap<ProspectLam, SavePrezziEntity>()
                .ForMember(x => x.MeseDa, y => y.MapFrom(j => 0))
                .ForMember(x => x.MeseA,
                    y => y.MapFrom(j => (j.DataIniForn ?? new DateTime(1900, 1, 1)).MonthDiff(j.DataFinForn ?? new DateTime(2899, 1, 1), 1)))
                .ForMember(x => x.Percentuale, y => y.MapFrom(j => 100))
                .ForMember(x => x.TipoPrezziId, y => y.MapFrom(j => j.ProdProdottoId));


            CreateMap<ComponentiFatturazione, ComponentiFatturazioneView>()
                .ForMember(x => x.Id, y => y.MapFrom(src => src.ComponentiFatturazioneId))
                .ForMember(x => x.Nome, y => y.MapFrom(src => src.ComponentiFatturazioneDescrizione))
                .ForMember(x => x.Unitamisura, y => y.MapFrom(src => src.ComponentiFatturazioneUnitamisura));

            CreateMap<Prodotto, ProdottoView>()
                .ForMember(x => x.ProdProdottoId, y => y.MapFrom(src => src.ProdProdottoId))
                .ForMember(x => x.IsIndicizzato, y => y.MapFrom(src => src.ProdDescrizione.Contains("PUN", StringComparison.OrdinalIgnoreCase)));

            CreateMap<TipoPrezzi, TipoOffertaView>()
                .ForMember(x => x.TipoOffertaId, y => y.MapFrom(src => src.TipoPrezziId))
                .ForMember(x => x.TipoOffertaDesc, y => y.MapFrom(src => src.TipoPrezziCod));

            CreateMap<TipoTerminiPagamenti, TipoTerminiPagamentiView>()
                .ForMember(x => x.IsDefault, y => y.MapFrom(src => src.TipoTerminiPagamentiCod == "20gg"));

            #region categorie

            CreateMap<Categoria, ElencoCategorieFullView>()
                .ForMember(x => x.IdCategoria, y => y.MapFrom(j => j.CatId))
                .ForMember(x => x.TsTipoServizioId, y => y.MapFrom(j => j.TsTipoServizioId))
                .ForMember(x => x.CatDescrizione, y => y.MapFrom(j => j.CatDescrizione))
                .ForMember(x => x.Configurazioni, y => y.MapFrom(j => j.Configurazioni.Select(c => new ConfigurazioneView() { 
                    ConfId = c.ConfId,
                    CatId = (int)c.CatId,
                    ConfNome = c.ConfNome,
                    ConfTesto = c.ConfTesto,
                    ConfTextMaxLength = c.ConfTextMaxLength
                })));

            #endregion

            #region caratteristicheOfferta

            CreateMap<Categoria, ElencoCategorieOffertaFullView>()
                .ForMember(x => x.IdCategoria, y => y.MapFrom(j => j.CatId))
                .ForMember(x => x.TsTipoServizioId, y => y.MapFrom(j => j.TsTipoServizioId))
                .ForMember(x => x.CatDescrizione, y => y.MapFrom(j => j.CatDescrizione))
                .ForMember(x => x.Configurazioni, y => y.MapFrom(j => j.Configurazioni.Select(c => new ConfigurazioneOffertaView()
                {
                    ConfId = c.ConfId,
                    CatId = (int)c.CatId,
                    ConfNome = c.ConfNome,
                    ConfTesto = c.ConfTesto,
                    Selezionata = false
                })));

            CreateMap<OffertaCaratteristiche, ElencoCategorieOffertaFullView>()
                .ForMember(x => x.IdCategoria, y => y.MapFrom(j => j.CategoriaId))
                .ForMember(x => x.Testo, y => y.MapFrom(j => j.FreeText))
                .ForMember(x => x.TsTipoServizioId, y => y.MapFrom(j => j.Offerta.ProdOffRel.Select(m => m.ProdProdotto.TsTipoServizioId)))
                .ForMember(x => x.Tariffa, y => y.MapFrom(j => j.Offerta.ProdOffRel.Select(m => m.ProdProdotto.Tariffa)))
                .ForMember(x => x.Configurazioni, y => y.MapFrom(j => j.Categoria.Configurazioni.Select(c => new ConfigurazioneOffertaView()
                {
                    ConfId = c.ConfId,
                    CatId = (int)c.CatId,
                    ConfNome = c.ConfNome,
                    ConfTesto = c.ConfTesto,
                    Selezionata = c.ConfId == j.CategoriaId ? true : false
                })));

            CreateMap<TipoTariffa, TipoTariffaView>()
                .ForMember(x => x.TipoTariffaId, y => y.MapFrom(src => src.TariffaId))
                .ForMember(x => x.TipoTariffaDesc, y => y.MapFrom(src => src.TariffaDesc))
                .ForMember(x => x.TipoServizioId, y => y.MapFrom(src => src.TsTipoServizioId));

            CreateMap<Uso, UsoView>()
                .ForMember(x => x.UsoId, y => y.MapFrom(src => src.UsoId))
                .ForMember(x => x.UsoDesc, y => y.MapFrom(src => src.UsoDesc))
                .ForMember(x => x.TipoServizioId, y => y.MapFrom(src => src.TsTipoServizioId));

            CreateMap<MateriaPrima, MateriaPrimaView>()
                .ForMember(x => x.MpId, y => y.MapFrom(src => src.MpId))
                .ForMember(x => x.MpDesc, y => y.MapFrom(src => src.MpDesc))
                .ForMember(x => x.TipoServizioId, y => y.MapFrom(src => src.TsTipoServizioId));

            CreateMap<ProdottoOfferta, ElencoCaratteristicheFullView>()
                .ForMember(x => x.IdProdotto, y => y.MapFrom(j => j.ProdOffRel.SingleOrDefault().ProdProdottoId))
                .ForMember(x => x.NomeProdotto, y => y.MapFrom(j => j.ProdOffRel.SingleOrDefault().ProdProdotto.ProdDescrizione))
                .ForMember(x => x.TipoServizio, y => y.MapFrom(j => j.ProdOffRel.SingleOrDefault().ProdProdotto.TsTipoServizioId))
                .ForMember(x => x.TipoTariffa, y => y.MapFrom(j => j.ProdOffRel.SingleOrDefault().ProdProdotto.Tariffa))
                //.ForMember(x => x.CatDescrizione, y => y.MapFrom(j => j.CatDescrizione))
                .ForMember(x => x.Caratteristiche, y => y.MapFrom(j => j.OffertaCaratteristiche.Select(c => new CaratteristicaView()
                {
                    OffCaratteristicheId = 1, //c.OffCaratteristicheId,

                    OffertaId = c.OffertaId,
                    CategoriaId = c.CategoriaId,
                    CategoriaDesc = c.Categoria.CatDescrizione,
                    ConfigurazioneId = c.ConfigurazioneId,
                    ConfigurazioneText =  c.ConfigurazioneId != null ? c.Configurazione.ConfTesto : "",
                    FreeText = c.FreeText
                })));

            CreateMap<ProdOffRel, ElencoListiniView>()
                .ForMember(x => x.IdProdotto, y => y.MapFrom(j => j.ProdProdottoId))
                .ForMember(x => x.Prodotto, y => y.MapFrom(j => j.ProdProdotto.ProdDescrizione))
                .ForMember(x => x.TipoServizio, y => y.MapFrom(j => j.ProdProdotto.TsTipoServizioId))
                .ForMember(x => x.Tariffa, y => y.MapFrom(j => j.ProdProdotto.Tariffa))
                .ForMember(x => x.IdOfferta, y => y.MapFrom(j => j.OffertaId))
                .ForMember(x => x.CodOfferta, y => y.MapFrom(j => j.Offerta.OffertaCod))
                .ForMember(x => x.Descrizione, y => y.MapFrom(j => j.Offerta.OffertaNote))
                 .ForMember(x => x.Scheda_confrontabilita,
                    y => y.MapFrom(j =>
                        !environment.IsDevelopment()
                            ? $"https://servizionline.agsm.it/retivendita/Files/PDF_Files/scheda_confrontabilita/{j.Offerta.SchedaConfrontabilita}"
                            : $"https://servizionline.agsm.it/test_retivendita/Files/PDF_Files/scheda_confrontabilita/{j.Offerta.SchedaConfrontabilita}"))
                .ForMember(x => x.Scheda_prodotto,
                    y => y.MapFrom(j =>
                        !environment.IsDevelopment()
                            ? $"https://servizionline.agsm.it/retivendita/Files/PDF_Files/scheda_prodotto/{j.Offerta.SchedaProdotto}"
                            : $"https://servizionline.agsm.it/test_retivendita/Files/PDF_Files/scheda_prodotto/{j.Offerta.SchedaProdotto}"))
                .ForMember(x => x.Mese, y => y.MapFrom(j => j.Offerta.Mese))
                .ForMember(x => x.Anno, y => y.MapFrom(j => j.Offerta.Anno))
                .ForMember(x => x.Trim, y => y.MapFrom(j => j.Offerta.Trim))
                .ForMember(x => x.Revisione, y => y.MapFrom(j => j.Offerta.Rev))
                .ForMember(x => x.PrezzoEnergia, y => y.MapFrom(j => j.Offerta.PrezzoEnergia))
                .ForMember(x => x.InizioValidita, y => y.MapFrom(j => j.Tts))
                .ForMember(x => x.FineValidita, y => y.MapFrom(j => j.Tte ?? new DateTime(9999, 12, 31)));

            #endregion

        }
    }
}