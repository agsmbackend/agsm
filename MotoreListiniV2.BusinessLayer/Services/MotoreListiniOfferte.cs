﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using MotoreListiniV2.BusinessLayer.Contracts;
using MotoreListiniV2.BusinessLayer.Helper;
using MotoreListiniV2.DataAccessLayer;
using MotoreListiniV2.DataAccessLayer.Database;
using MotoreListiniV2.DataAccessLayer.Extensions;
using MotoreListiniV2.DataAccessLayer.Models.Listino;
using MotoreListiniV2.Shared.Dto;
using MotoreListiniV2.Shared.ResponseBase;
using Newtonsoft.Json.Linq;
using UtilitiesGSA.EfCore;
using UtilitiesGSA.Extensions;
using UtilitiesGSA.SqlData.Async;
using Z.EntityFramework.Plus;
using System.Text.RegularExpressions;
using Aspose.Pdf;
using System.Net;
using SelectPdf;

namespace MotoreListiniV2.BusinessLayer.Services
{
    public class MotoreListiniOfferte : IMotoreListiniOfferte
    {
        private readonly ListinoContext _db;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;

        private static readonly MemoryCacheEntryOptions CacheEntryOptions = new()
            {SlidingExpiration = TimeSpan.FromHours(2), AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(90)};

        public MotoreListiniOfferte(ListinoContext db, IMapper mapper, IConfiguration config)
        {
            _db = db ?? throw new ArgumentNullException(nameof(db));

            //var conn = db.Database.GetDbConnection;
            var connectionString = db.Database.GetDbConnection().ConnectionString;
            bool canconnect = db.Database.CanConnect();


            _mapper = mapper;
            _config = config;
        }


        /// <summary>
        /// Elenco prodotti per tipo servizion passato
        /// </summary>
        /// <param name="tipoServizio"></param>
        /// <returns></returns>
        public async Task<ResponseTyped<IEnumerable<ProdottoView>>> GetTipoPrezzo(string tipoServizio)
        {
            var risList = await _db.Prodotto.AsNoTracking()
                .Where(x => x.OfferingLam && x.TsTipoServizioId == tipoServizio)
                .ProjectTo<ProdottoView>(_mapper.ConfigurationProvider)
                .TagWith("MotoreListini.Repository.GetElencoProdotti")
                .TagWith("estrae tipi prodotto disponibili in base al parametro tipoServizio passato")
                .TagWith(@"Parameters: {tipoServizio}")
                .FromCacheAsync(CacheEntryOptions, nameof(Prodotto));

            if (risList?.Any() == true)
                return new ResponseTyped<IEnumerable<ProdottoView>>
                {
                    Content = risList,
                    Errors = null,
                    ResultTyped = ResultType.Ok
                };

            return new ResponseTyped<IEnumerable<ProdottoView>>
            {
                Message = new StringBuilder("Nessuna Tipologia di Prezzo trovata").ToString(),
                ResultTyped = ResultType.NotFound,
                Errors = null
            };
        }

        /// <summary>
        /// Elenco Tipo Termini Pagamento
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseTyped<IEnumerable<TipoTerminiPagamentiView>>> GetListTerminiPagamento()
        {
            var risList = await _db.TipoTerminiPagamenti.AsNoTracking()
                .TagWith("MotoreListini.Repository.GetListTerminiPagamento")
                .TagWith("estrae lista tipo termini pagamenti disponibili")
                .ProjectTo<TipoTerminiPagamentiView>(_mapper.ConfigurationProvider)
                .FromCacheAsync(CacheEntryOptions, nameof(TipoTerminiPagamenti));

            if (risList?.Any() == true)
                return new ResponseTyped<IEnumerable<TipoTerminiPagamentiView>>
                {
                    Content = risList,
                    Errors = null,
                    ResultTyped = ResultType.Ok
                };

            return new ResponseTyped<IEnumerable<TipoTerminiPagamentiView>>
            {
                Message = new StringBuilder("Nessun termine di pagamento disponibile").ToString(),
                ResultTyped = ResultType.NotFound,
                Errors = null
            };
        }

        /// <summary>
        /// Elenco Tipo Offerte
        /// </summary>
        /// <param name="tipoServizio"></param>
        /// <returns></returns>
        public async Task<ResponseTyped<IEnumerable<TipoOffertaView>>> GetTipoOfferta(string tipoServizio)
        {
            IEnumerable<TipoOffertaView> risList;

            if (tipoServizio.EqualsIgnoreCase("all"))
            {
                risList = await _db.TipoPrezzi.AsNoTracking()
                    .TagWith("MotoreListini.Repository.GetTipoOfferta")
                    .TagWith("estrae lista tipo prezzi disponibili per tipo servizio")
                    .TagWith(@"Parameters: {tipoServizio}")
                    .ProjectTo<TipoOffertaView>(_mapper.ConfigurationProvider)
                    .FromCacheAsync(CacheEntryOptions, nameof(TipoPrezzi));
            }
            else
            {
                risList = await _db.TipoPrezzi.AsNoTracking()
                    .Where(x => x.TsTipoServizioId == tipoServizio)
                    .TagWith("MotoreListini.Repository.GetTipoOfferta")
                    .TagWith("estrae lista tipo prezzi disponibili per tipo servizio")
                    .TagWith(@"Parameters: {tipoServizio}")
                    .ProjectTo<TipoOffertaView>(_mapper.ConfigurationProvider)
                    .FromCacheAsync(CacheEntryOptions, nameof(TipoPrezzi));
            }

            if (risList?.Any() == true)
                return new ResponseTyped<IEnumerable<TipoOffertaView>>
                {
                    Content = risList,
                    Errors = null,
                    ResultTyped = ResultType.Ok
                };

            return new ResponseTyped<IEnumerable<TipoOffertaView>>
            {
                Message = new StringBuilder("Nessuna Tipologia di offerta trovata").ToString(),
                ResultTyped = ResultType.NotFound,
                Errors = null
            };
        }

        public async Task<ResponseTyped<IEnumerable<ComponentiPrezziGenericiView>>> GetComponentiPrezziGenerici(
            string tipoServizio)
        {
            var risList = await _db.TipoCompRel.AsNoTracking()
                .Where(x => x.TipoPrezzi.TsTipoServizioId == tipoServizio)
                .ProjectTo<ComponentiPrezziGenericiView>(_mapper.ConfigurationProvider)
                .Distinct()
                .TagWith("MotoreListini.Repository.GetComponentiPrezziGenerici")
                .TagWith("estrae lista componenti prezzi per tipo servizio")
                .TagWith(@"Parameters: {tipoServizio}")
                .FromCacheAsync(CacheEntryOptions, nameof(TipoCompRel));

            if (risList?.Any() == true)
                return new ResponseTyped<IEnumerable<ComponentiPrezziGenericiView>>
                {
                    Content = risList,
                    Errors = null,
                    ResultTyped = ResultType.Ok
                };

            return new ResponseTyped<IEnumerable<ComponentiPrezziGenericiView>>
            {
                Message = new StringBuilder("Nessuna componente della parte prezzi trovata").ToString(),
                ResultTyped = ResultType.NotFound,
                Errors = null
            };
        }

        public async Task<ResponseTyped<IEnumerable<ComponentiPrezziView>>> GetComponentiPrezzi(int idTipoOfferta)
        {
            var risList = await _db.TipoCompRel.AsNoTracking()
                .Where(x => x.TipoPrezziId == idTipoOfferta)
                .OrderBy(x => x.TipoCompRelId)
                .ProjectTo<ComponentiPrezziView>(_mapper.ConfigurationProvider)
                .TagWith("MotoreListini.Repository.GetComponentiPrezziGenerici")
                .TagWith("estrae lista componenti prezzi per tipo servizio")
                .TagWith(@"Parameters: {tipoServizio}")
                .FromCacheAsync(CacheEntryOptions, nameof(TipoCompRel));

            if (risList?.Any() == true)
                return new ResponseTyped<IEnumerable<ComponentiPrezziView>>
                {
                    Content = risList,
                    Errors = null,
                    ResultTyped = ResultType.Ok
                };

            return new ResponseTyped<IEnumerable<ComponentiPrezziView>>
            {
                Message = new StringBuilder("Nessuna componente della parte prezzi trovata").ToString(),
                ResultTyped = ResultType.NotFound,
                Errors = null
            };
        }

        public async Task<ResponseTyped<IEnumerable<ComponentiFatturazioneView>>> GetComponentiFatturazione(
            string tipoServizio)
        {
            IEnumerable<ComponentiFatturazioneView> risList;

            if (tipoServizio.EqualsIgnoreCase("all"))
            {
                risList = await _db.ComponentiFatturazione.AsNoTracking()
                    .TagWith("MotoreListini.Repository.GetComponentiFatturazione")
                    .TagWith("estrae lista componenti di fatturazione per tipo servizio")
                    .TagWith(@"Parameters: {tipoServizio}")
                    .ProjectTo<ComponentiFatturazioneView>(_mapper.ConfigurationProvider)
                    .FromCacheAsync(CacheEntryOptions, nameof(ComponentiFatturazione));
            }
            else
            {
                risList = await _db.ComponentiFatturazione.AsNoTracking()
                    .Where(x => x.TsTipoServizioId == tipoServizio)
                    .TagWith("MotoreListini.Repository.GetComponentiFatturazione")
                    .TagWith("estrae lista componenti di fatturazione per tipo servizio")
                    .TagWith(@"Parameters: {tipoServizio}")
                    .ProjectTo<ComponentiFatturazioneView>(_mapper.ConfigurationProvider)
                    .FromCacheAsync(CacheEntryOptions, nameof(ComponentiFatturazione));
            }

            if (risList?.Any() == true)
                return new ResponseTyped<IEnumerable<ComponentiFatturazioneView>>
                {
                    Content = risList,
                    Errors = null,
                    ResultTyped = ResultType.Ok
                };

            return new ResponseTyped<IEnumerable<ComponentiFatturazioneView>>
            {
                Message = new StringBuilder("Nessuna componente della parte elementi di fatturazione trovata").ToString(),
                ResultTyped = ResultType.NotFound,
                Errors = null
            };
        }

        public async Task<ResponseTyped<IEnumerable<ElencoListiniFullView>>> GetElencoListiniPanel(
            DateTime dataValiditaDa, DateTime dataValidataA, string prodotto, string listino)
        {
            var prodOff = await _db.ProdOffRel.AsNoTracking()
                .Where(x => x.Tts >= dataValiditaDa)
                .Where(x => x.Tte <= dataValidataA)
                .Where(x => EF.Functions.Like(x.ProdProdotto.ProdDescrizione, $"{prodotto}%"))
                .Where(x => EF.Functions.Like(x.Offerta.OffertaCod, $"{listino}%"))
                .ProjectTo<ElencoListiniFullView>(_mapper.ConfigurationProvider)
                .TagWith("MotoreListini.Repository.GetElencoListiniPanel")
                .TagWith("estrae i listini per popolare la pagina elenco listini")
                .FromCacheAsync(CacheEntryOptions, nameof(ProdOffRel));

            if (prodOff?.Any() == true)
                return new ResponseTyped<IEnumerable<ElencoListiniFullView>>
                {
                    Content = prodOff,
                    Errors = null,
                    ResultTyped = ResultType.Ok
                };

            return new ResponseTyped<IEnumerable<ElencoListiniFullView>>
            {
                Message = new StringBuilder("Elenco listini vuoto").ToString(),
                ResultTyped = ResultType.NotFound,
                Errors = null
            };
        }

        public async Task<ResponseTyped<IEnumerable<OffertaPrezziView>>> GetPrezziByListino(int idListino)
        {
            var anagraficaPrezzi = await _db.OffertaPrezzi.AsNoTracking()
                .Where(x => x.OffertaId == idListino)
                .ProjectTo<OffertaPrezziView>(_mapper.ConfigurationProvider)
                .Distinct()
                .TagWith("MotoreListini.Repository.GetPrezziByListino")
                .TagWith("estrae i prezzi associati al listino passato")
                .TagWith(@"Parameters: {idListino}")
                .FromCacheAsync(
                    new MemoryCacheEntryOptions {AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(60)},
                    nameof(OffertaPrezzi));

            if (anagraficaPrezzi?.Any() == true)
            {
                foreach (var item in anagraficaPrezzi.NotNullItems())
                {
                    var prices = _db.OffertaPrezzi.AsNoTracking()
                        .Where(x => x.OffertaId == idListino)
                        .Where(x => x.ProdProdottoId == item.TipoPrezziId)
                        .Where(x => x.TipoPrezziId == item.IdTipoOfferta)
                        .ProjectTo<OffertaComponentePrezziView>(_mapper.ConfigurationProvider);

                    foreach (var price in prices)
                    {
                        item.OffComponentePrezzi.Add(price);
                    }
                }

                return new ResponseTyped<IEnumerable<OffertaPrezziView>>
                {
                    Content = anagraficaPrezzi,
                    Errors = null,
                    ResultTyped = ResultType.Ok
                };
            }

            return new ResponseTyped<IEnumerable<OffertaPrezziView>>
            {
                Message = new StringBuilder("Nessun prezzo associato per il listino passato").ToString(),
                ResultTyped = ResultType.NotFound,
                Errors = null
            };
        }

        public async Task<ResponseTyped<IEnumerable<OffertaComponentiFatturazioneView>>> GetComponentiFattByListino(
            int idListino)
        {
            var anagraficaCompFatt = await _db.OffertaComponentifatt.AsNoTracking()
                .Where(x => x.OffertaId == idListino)
                .ProjectTo<OffertaComponentiFatturazioneView>(_mapper.ConfigurationProvider)
                .TagWith("MotoreListini.Repository.GetComponentiFattByListino")
                .TagWith("estrae le componenti di fatturazione associate al listino passato")
                .TagWith(@"Parameters: {idListino}")
                .FromCacheAsync(
                    new MemoryCacheEntryOptions {AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(60)},
                    nameof(OffertaComponentifatt));

            return await Task.FromResult(new ResponseTyped<IEnumerable<OffertaComponentiFatturazioneView>>
            {
                Content = anagraficaCompFatt,
                Errors = null,
                ResultTyped = ResultType.Ok
            }).ConfigureAwait(false);
        }

        public async Task<ResponseTyped<DataTable>> GetElencoListiniExcel(List<int> idsListini)
        {
            const string parametro = "@idListino";
            var parameters = new SqlParameter[idsListini.Count];

            var names = string.Join(",",
                idsListini.Select((value, i) =>
                {
                    var paramName = $"{parametro}{i.ToString(CultureInfo.InvariantCulture)}";
                    parameters[i] = new SqlParameter(paramName, value);
                    return paramName;
                }));

            var sqlClient = SqlAsync.New;

            var dtElencoListini = await sqlClient.ExecuteDataTableSqlDaAsync(
                _config.GetConnectionString("BO"),
                CommandType.Text,
                SqlStatements.SelectElencoListini.Replace(parametro, names, StringComparison.OrdinalIgnoreCase),
                parameters).ConfigureAwait(false);

            return new ResponseTyped<DataTable>
            {
                Content = dtElencoListini,
                Errors = null,
                ResultTyped = ResultType.Ok
            };
        }


        public async Task<ResponseTyped<string>> PostSaveFullListinoEntity(SaveFullListinoEntity listino)
        {
            var esito = string.Empty;
            var msgErrore = string.Empty;
            var lsErrors = new List<string>();

            ProdottoOfferta newProdottoOfferta;
            await _db.ExecuteTransactionAsync(async () =>
                    {
                        //se idListino è maggiore di 0 si tratta di modifica listino
                        newProdottoOfferta = listino.IdListino > 0 ? await ModificaListino(listino) : await NewListino(listino);

                        if (listino.SavePrezziEntityList?.Any() == true)
                        {
                            AddComponentiPrezzi(listino, newProdottoOfferta);
                        }

                        if (listino.SavePrezziEntityList?.Any() != true)
                        {
                            AddComponentiFatturazione(listino, newProdottoOfferta);
                        }

                        var success = await _db.SaveChangesAsync().ConfigureAwait(false) > 0;
                        esito = success ? "OK" : "FAILED";
                    }
                );

            QueryCacheManager.ExpireTag(nameof(Prodotto), nameof(ProdottoOfferta), nameof(ProdOffRel));

            return new ResponseTyped<string>
            {
                Content = esito,
                Message = msgErrore,
                Errors = lsErrors,
                ResultTyped = esito == "FAILED" ? ResultType.Invalid : ResultType.Ok
            };
        }

        private void AddComponentiFatturazione(SaveFullListinoEntity listino, ProdottoOfferta newProdottoOfferta)
        {
            var lsComFatt = new List<OffertaComponentifatt>();

            foreach (SaveComponentiFatturazioneEntity item in listino.SaveComponentiFatturazioneEntityList)
            {
                var fatt = new OffertaComponentifatt
                {
                    Offerta = newProdottoOfferta,
                    ComponentiFatturazioneId = item.ComponentiFatturazioneId,
                    Value = item.Value,
                    RangeDa = item.MeseDa,
                    RangeA = item.MeseA,
                    Opzionale = item.IsOptional,
                    DataInsert = DateTime.Now,
                    Tts = DateTime.Now
                };

                lsComFatt.Add(fatt);
            }

            _db.OffertaComponentifatt.AddRange(lsComFatt);
        }

        private void AddComponentiPrezzi(SaveFullListinoEntity listino, ProdottoOfferta newProdottoOfferta)
        {
            var lsPrice = new List<OffertaPrezzi>();

            foreach (SavePrezziEntity item in listino.SavePrezziEntityList.NotNullItems())
            {
                foreach (SaveComponentiPrezziEntity subitem in item.SaveComponentiPrezziEntityList.NotNullItems())
                {
                    var price = new OffertaPrezzi
                    {
                        Offerta = newProdottoOfferta,
                        ProdProdottoId = item.TipoPrezziId,
                        TipoPrezziId = item.TipoOfferteId,
                        ComponentiPrezziId = subitem.ComponentiPrezziId,
                        Prezzo = subitem.Prezzo,
                        RangeDa = item.MeseDa,
                        RangeA = item.MeseA,
                        Percapplicata = item.Percentuale,
                        DataInsert = DateTime.Now,
                        Tts = DateTime.Now
                    };

                    lsPrice.Add(price);
                }
            }

            _db.OffertaPrezzi.AddRange(lsPrice);
        }

        private async Task<ProdottoOfferta> NewListino(SaveFullListinoEntity listino)
        {
            //se idProdotto è maggiore di 0 si tratta di duplica listino altrimenti di creazione listino da zero
            Prodotto newProdotto;
            if (listino.IdProdotto > 0)
            {
                newProdotto = await _db.Prodotto.AsNoTracking()
                                    .FirstAsync(x => x.ProdProdottoId == listino.IdProdotto)
                                    .ConfigureAwait(false);
            }
            else
            {
                newProdotto = new Prodotto
                {
                    ProdDescrizione = listino.NomeProdotto.TrimIfNotEmpty(),
                    TsTipoServizioId = listino.TipoServizio,
                    Tariffa = listino.TipoTariffa,
                    HaveIban = false,
                    HavePaperless = false,
                    ProdNote = string.Empty
                };

                _db.Prodotto.Add(newProdotto);
            }

            var newProdottoOfferta = new ProdottoOfferta
            {
                OffertaCod = listino.CodiceListino.TrimIfNotEmpty(),
                OffertaDescrizione = newProdotto.ProdDescrizione.TrimIfNotEmpty(),
                OffertaNote = listino.Descrizione.TrimIfNotEmpty(),
                SchedaProdotto = listino.SpPDF.TrimIfNotEmpty(),
                Mese = listino.Mese,
                Anno = listino.Anno,
                Rev = listino.Revisione,
                Trim = listino.Trimestre,
                ScopaDataatt = listino.ValiditaPrezzo,
                Username = string.Empty,
                TipoTerminiPagamentiId = listino.TerminiPagamento,
                Flagindtofix = listino.Fixed
            };

            var porel = new ProdOffRel
            {
                Offerta = newProdottoOfferta,
                ProdProdotto = newProdotto,
                Tts = listino.InizioValidita,
                Tte = listino.FineValidita,
                DataDaAtt = listino.InizioAttivazione,
                DataAAtt = listino.FineAttivazione
            };

            _db.ProdottoOfferta.Add(newProdottoOfferta);
            _db.ProdOffRel.Add(porel);

            await _db.SaveChangesAsync().ConfigureAwait(false);

            return newProdottoOfferta;
        }

        private async Task<ProdottoOfferta> ModificaListino(SaveFullListinoEntity listino)
        {
            var prodottoOffertaEdit = await _db.ProdottoOfferta.FindAsync(listino.IdListino).ConfigureAwait(false);

            prodottoOffertaEdit.OffertaCod = listino.CodiceListino;
            prodottoOffertaEdit.OffertaNote = listino.Descrizione;

            if (prodottoOffertaEdit.SchedaProdotto != listino.SpPDF
                && !string.IsNullOrEmpty(listino.SpPDF))
                prodottoOffertaEdit.SchedaProdotto = listino.SpPDF;

            prodottoOffertaEdit.Mese = listino.Mese;
            prodottoOffertaEdit.Anno = listino.Anno;
            prodottoOffertaEdit.Rev = listino.Revisione;
            prodottoOffertaEdit.Trim = listino.Trimestre;
            prodottoOffertaEdit.ScopaDataatt = listino.ValiditaPrezzo;
            prodottoOffertaEdit.TipoTerminiPagamentiId = listino.TerminiPagamento;
            prodottoOffertaEdit.Flagindtofix = listino.Fixed;

            var porel = await _db.ProdOffRel
                .FirstOrDefaultAsync(x => x.OffertaId == listino.IdListino)
                .ConfigureAwait(false);

            if (porel is null)
                throw new InvalidOperationException("problema a trovare collegamento tra prodotto e offerta");

            porel.Tts = listino.InizioValidita;
            porel.Tte = listino.FineValidita;
            porel.DataDaAtt = listino.InizioAttivazione;
            porel.DataAAtt = listino.FineAttivazione;

            var azzeraPrezziFuture = _db.OffertaPrezzi
                .Where(x => x.Tte == null && x.OffertaId == listino.IdListino)
                .Future();
            var azzeraCompFattFuture = _db.OffertaComponentifatt
                .Where(x => x.Tte == null && x.OffertaId == listino.IdListino)
                .Future();

            var azzeraPrezzi = await azzeraPrezziFuture.ToListAsync();
            var azzeraCompFatt = await azzeraCompFattFuture.ToListAsync();

            azzeraPrezzi.ForEach(item => { item.Tte = DateTime.Now; });
            azzeraCompFatt.ForEach(item => { item.Tte = DateTime.Now; });

            await _db.SaveChangesAsync().ConfigureAwait(false);

            return prodottoOffertaEdit;
        }

        /// <summary>
        /// Modifica data fine validità listino
        /// </summary>
        /// <param name="infoAnticipaData"></param>
        /// <returns></returns>
        public async Task<ResponseTyped<string>> PostAnticipaChiusuraListino(JObject infoAnticipaData)
        {
            var esito = string.Empty;
            var msgErrore = string.Empty;
            var lsErrors = new List<string>();
            ProdOffRel listino = null;

            dynamic infoAnticipa = infoAnticipaData;

            if (int.TryParse(infoAnticipa.idListino.ToString(), out int _))
            {
                int idOfferta = infoAnticipa.idListino ?? -1;
                listino = await _db.ProdOffRel
                    .Where(x => x.OffertaId == idOfferta)
                    .FirstOrDefaultAsync()
                    .ConfigureAwait(false);
            }

            if (listino == null) return null;

            if (DateTime.TryParse(infoAnticipa.validitaA.ToString(), out DateTime newDataAnt))
            {
                if (newDataAnt <= listino.Tts || newDataAnt < DateTime.Now.Date) //|| newDataAnt >= Listino.Tte
                    return new ResponseTyped<string>
                    {
                        Content = esito,
                        Message = new StringBuilder("Data non valida").ToString(),
                        Errors = lsErrors,
                        ResultTyped = ResultType.Invalid
                    };

                if (await _db.IsOkNewDateDuplicaAsync(listino.OffertaId ?? -1,
                    ControlliListino.Source.Listino,
                    listino.Tts,
                    newDataAnt).ConfigureAwait(false)
                )
                    return new ResponseTyped<string>
                    {
                        Content = esito,
                        Message = new StringBuilder("Nell'intervallo indicato di validità esiste già un altro listino")
                            .ToString(),
                        Errors = lsErrors, ResultTyped = ResultType.Invalid
                    };

                listino.Tte = infoAnticipa.validitaA;
                await _db.SaveChangesAsync().ConfigureAwait(false);

                QueryCacheManager.ExpireTag(nameof(Prodotto), nameof(ProdottoOfferta), nameof(ProdOffRel));

                return new ResponseTyped<string>
                {
                    Content = esito != "FAILED" ? "OK" : esito,
                    Message = msgErrore,
                    Errors = lsErrors,
                    ResultTyped = esito == "FAILED" ? ResultType.Invalid : ResultType.Ok
                };
            }

            return new ResponseTyped<string>
            {
                Content = esito,
                Message = new StringBuilder("Data non valida").ToString(),
                Errors = lsErrors,
                ResultTyped = ResultType.Invalid
            };

        }

        public async Task<ResponseTyped<string>> PostPdfUploader(IFormFile allegato)
        {
            if (allegato is null)
                return new ResponseTyped<string>
                {
                    Message = new StringBuilder("nessun file trovato").ToString(),
                    Errors = null,
                    ResultTyped = ResultType.NotFound
                };

            var filename =
                $"{DateTime.Now.ToString("yyyyMMddHHmmss", CultureInfo.InvariantCulture)}_{ContentDispositionHeaderValue.Parse(allegato.ContentDisposition).FileName?.Trim('"')}";

            if (filename.EndsWith(".pdf", StringComparison.OrdinalIgnoreCase) is false)
                return new ResponseTyped<string>
                {
                    Message = new StringBuilder("file solo in formato pdf").ToString(),
                    Errors = null,
                    ResultTyped = ResultType.Invalid
                };

            filename = IoHelper.EnsureCorrectFilename(filename);

            await using var output = File.Create(IoHelper.GetPathListini(_config, filename));
            await allegato.CopyToAsync(output).ConfigureAwait(false);
            await output.FlushAsync().ConfigureAwait(false);

            return new ResponseTyped<string> {Content = filename, Errors = null, ResultTyped = ResultType.Ok};
        }

        #region categorie

        public async Task<ResponseTyped<string>> PostSaveCategoriaEntity(SaveCategoriaEntity categoria)
        {
            var esito = string.Empty;
            var msgErrore = string.Empty;
            var lsErrors = new List<string>();

            Categoria newCategoria;
            await _db.ExecuteTransactionAsync(async () =>
            {
                //se idListino è maggiore di 0 si tratta di modifica listino
                newCategoria = categoria.IdCategoria > 0 ? await ModificaCategoria(categoria) : await NewCategoria(categoria);

                esito = newCategoria != null ? "OK" : "FAILED";
            });

            QueryCacheManager.ExpireTag(nameof(Prodotto), nameof(ProdottoOfferta), nameof(ProdOffRel));

            return new ResponseTyped<string>
            {
                Content = esito,
                Message = msgErrore,
                Errors = lsErrors,
                ResultTyped = esito == "FAILED" ? ResultType.Invalid : ResultType.Ok
            };
        }

        private async Task<Categoria> NewCategoria(SaveCategoriaEntity categoria)
        {
            //se idProdotto è maggiore di 0 si tratta di duplica listino altrimenti di creazione listino da zero
            Categoria newCategoria;
            if (categoria.IdCategoria > 0)
            {
                newCategoria = await _db.Categoria.AsNoTracking()
                                    .FirstAsync(x => x.CatId == categoria.IdCategoria)
                                    .ConfigureAwait(false);
            }
            else
            {
                newCategoria = new Categoria
                {
                    TsTipoServizioId = categoria.TipoServizio,
                    CatDescrizione = categoria.Descrizione.TrimIfNotEmpty(),
                    Attiva = false
                };

                _db.Categoria.Add(newCategoria);
            }

            await _db.SaveChangesAsync().ConfigureAwait(false);
            
            return newCategoria;
        }

        private async Task<Categoria> ModificaCategoria(SaveCategoriaEntity categoria)
        {
            var categoriaEdit = await _db.Categoria.FindAsync(categoria.IdCategoria).ConfigureAwait(false);

            categoriaEdit.TsTipoServizioId= categoria.TipoServizio;
            categoriaEdit.CatDescrizione = categoria.Descrizione;
            categoriaEdit.Attiva = categoria.Attiva;

            await _db.SaveChangesAsync().ConfigureAwait(false);
            
            return categoriaEdit;
        }

        public async Task<ResponseTyped<IEnumerable<ElencoCategorieFullView>>> GetElencoCategorie(string descrizione, string grado, string tipo_servizio)
        {
                Nullable<bool> statoCategoria = null;
                switch (grado)
                {
                    case "0":
                        statoCategoria = false;
                        break;
                    case "1":
                        statoCategoria = true;
                        break;
                }

                var catListQueryable = _db.Categoria.AsNoTracking()
                            .Include("Configurazioni")
                            .Where(x => x.TsTipoServizioId == tipo_servizio);

                catListQueryable = !string.IsNullOrEmpty(descrizione) ? catListQueryable.Where(x => EF.Functions.Like(x.CatDescrizione, $"{descrizione}%")) : catListQueryable;
                catListQueryable = statoCategoria != null ? catListQueryable.Where(x => x.Attiva == (bool)statoCategoria) : catListQueryable;

                var catList = await (catListQueryable
                    .TagWith("MotoreListini.Repository.GetElencoCategorie")
                    .TagWith("estrae le categorie per popolare la pagina luce o gas")
                    .ProjectTo<ElencoCategorieFullView>(_mapper.ConfigurationProvider).ToListAsync());

                if (catList.Any() == true)
                {
                    return new ResponseTyped<IEnumerable<ElencoCategorieFullView>>
                    {
                        Content = catList,
                        Errors = null,
                        ResultTyped = ResultType.Ok
                    };
                } 

                return new ResponseTyped<IEnumerable<ElencoCategorieFullView>>
                {
                    Message = new StringBuilder("Elenco categorie vuoto").ToString(),
                    ResultTyped = ResultType.NotFound,
                    Errors = null
                };
        }

        #endregion

        #region configurazioni

        public async Task<ResponseTyped<string>> PostSaveConfigurazioneEntity(SaveConfigurazioneEntity configurazione)
        {
            var esito = string.Empty;
            var msgErrore = string.Empty;
            var lsErrors = new List<string>();

            Configurazione newConfigurazione;
            await _db.ExecuteTransactionAsync(async () =>
            {
                //se idListino è maggiore di 0 si tratta di modifica listino
                newConfigurazione = configurazione.IdConfigurazione > 0 ? await ModificaConfigurazione(configurazione) : await NewConfigurazione(configurazione);

                esito = newConfigurazione != null ? "OK" : "FAILED";
                
            });

            QueryCacheManager.ExpireTag(nameof(Prodotto), nameof(ProdottoOfferta), nameof(ProdOffRel));

            return new ResponseTyped<string>
            {
                Content = esito,
                Message = msgErrore,
                Errors = lsErrors,
                ResultTyped = esito == "FAILED" ? ResultType.Invalid : ResultType.Ok
            };
        }

        private async Task<Configurazione> NewConfigurazione(SaveConfigurazioneEntity configurazione)
        {
            //se idProdotto è maggiore di 0 si tratta di duplica listino altrimenti di creazione listino da zero
            Configurazione newConfigurazione;
            if (configurazione.IdConfigurazione > 0)
            {
                newConfigurazione = await _db.Configurazione.AsNoTracking()
                                    .FirstAsync(x => x.CatId == configurazione.IdConfigurazione)
                                    .ConfigureAwait(false);
            }
            else
            {
                newConfigurazione = new Configurazione
                {
                    CatId = configurazione.IdCategoria,
                    ConfNome = configurazione.NomeConfigurazione,
                    ConfTesto = configurazione.TestoConfigurazione,
                    ConfTextMaxLength = configurazione.TextMaxConfigurazione
                };

                _db.Configurazione.Add(newConfigurazione);
            }

            await _db.SaveChangesAsync().ConfigureAwait(false);
        
            return newConfigurazione;
        }

        private async Task<Configurazione> ModificaConfigurazione(SaveConfigurazioneEntity configurazione)
        {
            var configurazioneEdit = await _db.Configurazione.FindAsync(configurazione.IdConfigurazione).ConfigureAwait(false);

            configurazioneEdit.ConfNome = configurazione.NomeConfigurazione;
            configurazioneEdit.ConfTesto = configurazione.TestoConfigurazione;
            configurazioneEdit.ConfTextMaxLength = configurazione.TextMaxConfigurazione;

            await _db.SaveChangesAsync().ConfigureAwait(false);
            
            return configurazioneEdit;
        }

        public async Task<ResponseTyped<string>> DeleteConfigurazioneEntity(int idConfigurazione)
        {
            var esito = string.Empty;
            var msgErrore = string.Empty;
            var lsErrors = new List<string>();

            var configurazione = await _db.Configurazione.FindAsync(idConfigurazione).ConfigureAwait(false);

            if (configurazione != null)
            {
                _db.Configurazione.Remove(configurazione);
                await _db.SaveChangesAsync().ConfigureAwait(false);
                esito = "OK";
            } else
            {
                esito = "FAILED";
                msgErrore = "Configurazione non trovata";
            }

            QueryCacheManager.ExpireTag(nameof(Configurazione));

            return new ResponseTyped<string>
            {
                Content = esito,
                Message = msgErrore,
                Errors = lsErrors,
                ResultTyped = esito == "FAILED" ? ResultType.Invalid : ResultType.Ok
            };
        }

        #endregion

        #region caratteristicheOfferta

        public async Task<ResponseTyped<IEnumerable<TipoTariffaView>>> GetTipiTariffa(int servizioId)
        {

            var tipiTariffa = await _db.TipoTariffa.AsNoTracking()
            .Where(x => x.TsTipoServizioId == servizioId.ToString())
            .ProjectTo<TipoTariffaView>(_mapper.ConfigurationProvider)
            .TagWith("MotoreListini.Repository.GetTipiTariffa")
            .TagWith("estrae tipi tariffa in base al parametro idServizio passato")
            .TagWith(@"Parameters: {idServizio}")
            .FromCacheAsync(CacheEntryOptions, nameof(TipoTariffa));

            if (tipiTariffa?.Any() == true)
                return new ResponseTyped<IEnumerable<TipoTariffaView>>
                {
                    Content = tipiTariffa,
                    Errors = null,
                    ResultTyped = ResultType.Ok
                };

            return new ResponseTyped<IEnumerable<TipoTariffaView>>
            {
                Message = new StringBuilder("Nessun Tipo tariffa trovato").ToString(),
                ResultTyped = ResultType.NotFound,
                Errors = null
            };
        }

        public async Task<ResponseTyped<IEnumerable<UsoView>>> GetUsi(int servizioId)
        {

            var usi = await _db.Uso.AsNoTracking()
            .Where(x => x.TsTipoServizioId == servizioId.ToString())
            .ProjectTo<UsoView>(_mapper.ConfigurationProvider)
            .TagWith("MotoreListini.Repository.Get")
            .TagWith("estrae gli usi prodotto in base al parametro idServizio")
            .TagWith(@"Parameters: {idServizio}")
            .FromCacheAsync(CacheEntryOptions, nameof(Uso));

            if (usi?.Any() == true)
                return new ResponseTyped<IEnumerable<UsoView>>
                {
                    Content = usi,
                    Errors = null,
                    ResultTyped = ResultType.Ok
                };

            return new ResponseTyped<IEnumerable<UsoView>>
            {
                Message = new StringBuilder("Nessun uso prodotto trovato").ToString(),
                ResultTyped = ResultType.NotFound,
                Errors = null
            };
        }

        public async Task<ResponseTyped<IEnumerable<MateriaPrimaView>>> GetMateriePrime(int servizioId)
        {

            var usi = await _db.MateriaPrima.AsNoTracking()
            .Where(x => x.TsTipoServizioId == servizioId.ToString())
            .ProjectTo<MateriaPrimaView>(_mapper.ConfigurationProvider)
            .TagWith("MotoreListini.Repository.Get")
            .TagWith("estrae le materie prime prodotto in base al parametro idServizio")
            .TagWith(@"Parameters: {idServizio}")
            .FromCacheAsync(CacheEntryOptions, nameof(MateriaPrima));

            if (usi?.Any() == true)
                return new ResponseTyped<IEnumerable<MateriaPrimaView>>
                {
                    Content = usi,
                    Errors = null,
                    ResultTyped = ResultType.Ok
                };

            return new ResponseTyped<IEnumerable<MateriaPrimaView>>
            {
                Message = new StringBuilder("Nessuna materia prima trovata").ToString(),
                ResultTyped = ResultType.NotFound,
                Errors = null
            };
        }

        public async Task<ResponseTyped<IEnumerable<ElencoCategorieOffertaFullView>>> GetElencoCategorieOfferta(int? idOfferta, string grado, string tipo_servizio)
        {
                var catList = new List<ElencoCategorieOffertaFullView>();

                if(idOfferta > 0)
                {
                    //L'offerta è già esistente, la stiamo modificando
                    var categorieOfferta = await _db.OffertaCaratteristiche
                         .Include("Categoria")
                         .Include("Categoria.Configurazioni")
                         .Include("Offerta")
                         //.Include("Offerta.ProdOffRel")
                         .Include("Offerta.ProdOffRel.ProdProdotto")
                        .Where(m => m.OffertaId == idOfferta)
                        .ProjectTo<ElencoCategorieOffertaFullView>(_mapper.ConfigurationProvider)
                        .ToListAsync();
                } else
                {
                    //L'offerta deve ancora essere creata: estraggo le categorie suggerite
                    catList = await (_db.Categoria.AsNoTracking()
                            .Include("Configurazioni")
                            .Where(x => x.TsTipoServizioId == tipo_servizio && x.Attiva == true)
                            .ProjectTo<ElencoCategorieOffertaFullView>(_mapper.ConfigurationProvider).ToListAsync());
                }

                if (catList.Any() == true)
                {
                    return new ResponseTyped<IEnumerable<ElencoCategorieOffertaFullView>>
                    {
                        Content = catList,
                        Errors = null,
                        ResultTyped = ResultType.Ok
                    };
                }

                return new ResponseTyped<IEnumerable<ElencoCategorieOffertaFullView>>
                {
                    Message = new StringBuilder("Elenco categorie vuoto").ToString(),
                    ResultTyped = ResultType.NotFound,
                    Errors = null
                };
        }

        public async Task<ResponseTyped<int>> PostSaveSetupOfferta(SaveSetupEntity setup)
        {
            int esito = 0;
            var msgErrore = string.Empty;
            var lsErrors = new List<string>();

           
            await _db.ExecuteTransactionAsync(async () =>
                {
                    //se idListino è maggiore di 0 si tratta di modifica listino
                    ProdottoOfferta offerta = setup.IdListino > 0 ? await ModificaSetup(setup) : await NewSetup(setup);

                    //var success = await _db.SaveChangesAsync().ConfigureAwait(false) > 0;
                    //esito = success ? "OK" : "FAILED";

                    esito = offerta != null ? offerta.OffertaId : 0;
                   
                }
            
            );

            QueryCacheManager.ExpireTag(nameof(Prodotto), nameof(ProdottoOfferta), nameof(ProdOffRel), nameof(OffertaCaratteristiche));

            return new ResponseTyped<int>
            {
                Content = esito,
                Message = msgErrore,
                Errors = lsErrors,
                ResultTyped = esito == 0 ? ResultType.Invalid : ResultType.Ok
            };
        }

        private async Task<ProdottoOfferta> NewSetup(SaveSetupEntity setup)
        {

            //se idProdotto è maggiore di 0 si tratta di duplica listino altrimenti di creazione listino da zero
            Prodotto newProdotto;
            var newProdottoOfferta = new ProdottoOfferta();

                if (setup.IdProdotto > 0)
                {
                    newProdotto = await _db.Prodotto.AsNoTracking()
                                        .FirstAsync(x => x.ProdProdottoId == setup.IdProdotto)
                                        .ConfigureAwait(false);
                }
                else
                {
                    newProdotto = new Prodotto
                    {
                        ProdDescrizione = setup.NomeProdotto.TrimIfNotEmpty(),
                        TsTipoServizioId = setup.TipoServizio,
                        Tariffa = setup.TipoTariffa,
                        HaveIban = false,
                        HavePaperless = false,
                        ProdNote = string.Empty
                    };

                    _db.Prodotto.Add(newProdotto);
                }

                newProdottoOfferta = new ProdottoOfferta
                {
                    OffertaCod = DateTime.Now.ToUniversalTime().ToString("O"), // listino.CodiceListino.TrimIfNotEmpty(),
                    OffertaDescrizione = newProdotto.ProdDescrizione.TrimIfNotEmpty(),
                    Username = string.Empty,
                };

                var porel = new ProdOffRel
                {
                    Offerta = newProdottoOfferta,
                    ProdProdotto = newProdotto,
                    Tts = DateTime.Now, 
                    Tte = DateTime.Now
                };

                _db.ProdottoOfferta.Add(newProdottoOfferta);
                _db.ProdOffRel.Add(porel);
                await _db.SaveChangesAsync().ConfigureAwait(false);


                //Salvo le configurazioni per questo prodotto
                if (setup.CaratteristicheList.Any())
                {
                    foreach (SaveCaratteristicheEntity caratteristica in setup.CaratteristicheList)
                    {
                        OffertaCaratteristiche newSetup = new OffertaCaratteristiche
                        {
                            OffertaId = newProdottoOfferta.OffertaId,
                            CategoriaId = caratteristica.CategoriaId,
                            ConfigurazioneId = caratteristica.ConfigurazioneId > 0 ? caratteristica.ConfigurazioneId : null,
                            FreeText = caratteristica.FreeText
                        };

                        _db.OffertaCaratteristiche.Add(newSetup);
                    }
                }

                await _db.SaveChangesAsync().ConfigureAwait(false);

            return newProdottoOfferta;
        }

        private async Task<ProdottoOfferta> ModificaSetup(SaveSetupEntity setup)
        {
            var prodottoOffertaEdit = await _db.ProdottoOfferta.FindAsync(setup.IdListino).ConfigureAwait(false);

            //var configurazioneEdit = await _db.OffertaCaratteristiche.FindAsync(setup.IdListino).ConfigureAwait(false);
            if(setup.CaratteristicheList.Any())
            {
                foreach(OffertaCaratteristiche caratteristica in prodottoOffertaEdit.OffertaCaratteristiche)
                {
                    //Voglio ancora che faccia parte delle caratteristiche offerta?
                    var toSaveEntity = setup.CaratteristicheList.Where(m => m.CategoriaId == caratteristica.CategoriaId).FirstOrDefault();
                    if(toSaveEntity != null)
                    {
                        caratteristica.ConfigurazioneId = toSaveEntity.ConfigurazioneId;
                        caratteristica.FreeText = toSaveEntity.FreeText;
                        _db.OffertaCaratteristiche.Update(caratteristica);
                    } else
                    {
                        //Elimino l'entità dal db, non esiste più
                        _db.OffertaCaratteristiche.Remove(caratteristica);
                    }
                }
            } else
            {
                //cambio configurazione
                if(prodottoOffertaEdit != null)
                {
                    foreach (OffertaCaratteristiche caratteristica in prodottoOffertaEdit.OffertaCaratteristiche)
                    {
                        _db.OffertaCaratteristiche.Remove(caratteristica);
                    }
                }
            }

            await _db.SaveChangesAsync().ConfigureAwait(false);

            return prodottoOffertaEdit;
        }

        public async Task<ResponseTyped<ElencoCaratteristicheFullView>> GetCaratteristicheOfferta(int idOfferta)
        {

                var prodottoOffertaEdit = await _db.ProdottoOfferta
                   .Where(x => x.OffertaId == idOfferta)
                   .Include("OffertaCaratteristiche")
                   .Include("ProdOffRel")
                   .Include("ProdOffRel.ProdProdotto")
                   //.Include("ProdOffRel.Configurazione")
                   .TagWith("MotoreListini.Repository.GetCaratteristicheOfferta")
                   .TagWith("estrae le caratteristiche offerta prodotto per composizione listino")
                   .ProjectTo<ElencoCaratteristicheFullView>(_mapper.ConfigurationProvider).FirstOrDefaultAsync();

                if (prodottoOffertaEdit != null)
                {
                    return new ResponseTyped<ElencoCaratteristicheFullView>
                    {
                        Content = prodottoOffertaEdit,
                        Errors = null,
                        ResultTyped = ResultType.Ok
                    };
                }

                return new ResponseTyped<ElencoCaratteristicheFullView>
                {
                    Message = new StringBuilder("Nessuna offerta trovata").ToString(),
                    ResultTyped = ResultType.NotFound,
                    Errors = null
                };
        }


        public async Task<ResponseTyped<int>> PostSaveListinoEntity(FinalizeListinoEntity listino)
        {
            int esito = 0;
            var msgErrore = string.Empty;
            var lsErrors = new List<string>();
           
            string templateName = "";
            string templateHtml = "";


            await _db.ExecuteTransactionAsync(async () =>
            {
                var offerta = await _db.ProdottoOfferta.Include("OffertaCaratteristiche").Include("OffertaCaratteristiche.Configurazione").Include("OffertaCaratteristiche.Categoria").Where(m => m.OffertaId == listino.IdOfferta).FirstOrDefaultAsync();

                //se il template non è stato selezionato, non procedo
                templateName = offerta.OffertaCaratteristiche.Where(m => m.Categoria.CatDescrizione == "Scelta template").Select(m => m.Configurazione.ConfTesto).SingleOrDefault();

                if(templateName != null)
                {
                    //se ho selezionato un template, procedo
                    //1 - a recuperare l'html del template
                    templateHtml = File.ReadAllText(IoHelper.GetPathTemplates(_config, templateName));

                    //2 - al salvataggio e contemporanea personalizzazione del template
                    //var configurazioneEdit = await _db.OffertaCaratteristiche.FindAsync(setup.IdListino).ConfigureAwait(false);
                    if (listino.CaratteristicheList.Any())
                    {
                        foreach (OffertaCaratteristiche caratteristica in offerta.OffertaCaratteristiche)
                        {
                            var toSaveEntity = listino.CaratteristicheList.Where(m => m.CategoriaId == caratteristica.CategoriaId).FirstOrDefault();
                            
                            //Salvo le modifiche alla caratteristica
                            if (toSaveEntity != null)
                            {
                                //in questo caso sto salvando solo i parametri testuali (il resto l'ho gestito nelle impostazioni Setup Offerta
                                if(caratteristica.Configurazione.ConfTextMaxLength < 0 || (caratteristica.Configurazione.ConfTextMaxLength > 0 && caratteristica.FreeText.Length <= caratteristica.Configurazione.ConfTextMaxLength))
                                {
                                    //Salvo il testo della configurazione
                                    caratteristica.ConfigurazioneId = toSaveEntity.ConfigurazioneId;
                                    caratteristica.FreeText = toSaveEntity.FreeText;
                                    _db.OffertaCaratteristiche.Update(caratteristica);
                                    templateHtml = templateHtml.Replace("[" + caratteristica.Categoria.CatDescrizione + "]", toSaveEntity.FreeText);
                                }
                            }

                            if(caratteristica.ConfigurazioneId != null)
                            {
                                //E' un parametro di configurazione, personalizzo la voce di template
                                templateHtml = templateHtml.Replace("[" + caratteristica.Categoria.CatDescrizione + "]", caratteristica.Configurazione.ConfTesto);
                            }
                        }
                    }

                    //Parametri fissi
                    offerta.OffertaCod = listino.CodiceListino.TrimIfNotEmpty();
                    templateHtml = templateHtml.Replace("[Codice prodotto]", offerta.OffertaCod);

                    offerta.OffertaNome = listino.NomeOfferta.TrimIfNotEmpty();
                    templateHtml = templateHtml.Replace("[Nome offerta]", offerta.OffertaNome);

                    offerta.OffertaNote = listino.Descrizione.TrimIfNotEmpty();
                    offerta.SchedaConfrontabilita = listino.ScPDF.TrimIfNotEmpty();
                    offerta.SchedaProdotto = listino.SpPDF.TrimIfNotEmpty();
                    offerta.Mese = listino.Mese;
                    offerta.Anno = listino.Anno;
                    offerta.Trim = listino.Trimestre;
                    offerta.Rev = listino.Rev;
                    offerta.Uso = listino.Uso;
                    offerta.MateriaPrima = listino.MateriaPrima;

                    offerta.PrezzoEnergia = listino.PrezzoEnergia;
                    templateHtml = templateHtml.Replace("[Prezzo energia]", offerta.PrezzoEnergia.ToString());
                    templateHtml = templateHtml.Replace("[Prezzo gas]", offerta.PrezzoEnergia.ToString());

                    offerta.PercMateriaEnergia = listino.PercMateriaEnergia;
                    templateHtml = templateHtml.Replace("[% materia energia]", offerta.PercMateriaEnergia.ToString());
                    
                    offerta.PercComponenteEnergia = listino.PercComponenteEnergia;
                    templateHtml = templateHtml.Replace("[% componente energia]", offerta.PercComponenteEnergia.ToString());

                    offerta.PercSpesaTrasportoGestione = listino.PercSpesaTrasportoGestione;
                    templateHtml = templateHtml.Replace("[% componente energia]", offerta.PercSpesaTrasportoGestione.ToString());

                    offerta.PercOneriSistema = listino.PercOneriSistema;
                    templateHtml = templateHtml.Replace("[% Oneri di sistema]", offerta.PercOneriSistema.ToString());

                    offerta.PercAsos = listino.PercAsos;
                    templateHtml = templateHtml.Replace("[% Asos]", offerta.PercAsos.ToString());

                    offerta.PercCompExtra1 = listino.PercCompExtra1;
                    templateHtml = templateHtml.Replace("[% componente extra 1]", offerta.PercCompExtra1.ToString());
                    offerta.PercCompExtra2 = listino.PercCompExtra2;
                    templateHtml = templateHtml.Replace("[% componente extra 2]", offerta.PercCompExtra2.ToString());
                    offerta.PercCompExtra3 = listino.PercCompExtra3;
                    templateHtml = templateHtml.Replace("[% componente extra 3]", offerta.PercCompExtra3.ToString());

                    _db.ProdottoOfferta.Update(offerta);

                    ProdOffRel prodOffRel = await _db.ProdOffRel.Where(m => m.OffertaId == listino.IdOfferta).SingleOrDefaultAsync(); //.ConfigureAwait(false);
                    if (prodOffRel != null)
                    {
                        prodOffRel.Tts = listino.InizioValidita;
                        prodOffRel.Tte = listino.FineValidita;
                        //prodOffRel.DataDaAtt = listino.InizioAttivazione,     --per ora non valorizzati
                        //prodOffRel.DataAAtt = listino.FineAttivazione         --per ora non valorizzati
                        _db.ProdOffRel.Update(prodOffRel);
                    }

                    await _db.SaveChangesAsync().ConfigureAwait(false);
                } else
                {
                    msgErrore = msgErrore == "" ? "Impossibile trovare il template selezionato." : msgErrore;
                    esito = 0;
                }

                //Procedo a generare il pdf del preventivo

                //regular expression per eliminare le eventuali parentesi rimaste
                templateHtml = Regex.Replace(templateHtml, @"\[[^]]*\]", "");

                HtmlToPdf converter = new HtmlToPdf();
                converter.Options.PdfPageSize = PdfPageSize.Custom;
                converter.Options.ViewerPreferences.FitWindow = true;
                converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;

                converter.Options.PdfPageSize = SelectPdf.PdfPageSize.Custom; // 12; // pageSize;
                converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait; // pdfOrientation;
                converter.Options.WebPageWidth = 900; // webPageWidth;
                converter.Options.WebPageHeight = 1080;

                // create a new pdf document converting an url
                PdfDocument doc = converter.ConvertHtmlString(templateHtml); //.ConvertUrl(html); //.ConvertUrl(url);
                string fileName = "Listino_" + offerta.OffertaId + "_" + offerta.OffertaCod + "_" + DateTime.Now.ToShortTimeString();
                doc.Save(IoHelper.GetPathSchedeGenerate(_config, fileName));
                esito = offerta != null ? offerta.OffertaId : 0;
            }
            );

            QueryCacheManager.ExpireTag(nameof(Prodotto), nameof(ProdottoOfferta), nameof(ProdOffRel), nameof(OffertaCaratteristiche));

            return new ResponseTyped<int>
            {
                Content = esito,
                Message = msgErrore,
                Errors = lsErrors,
                ResultTyped = esito == 0 ? ResultType.Invalid : ResultType.Ok
            };

        }

        public async Task<ResponseTyped<IEnumerable<ElencoListiniView>>> GetElencoListini(
            DateTime dataValiditaDa, DateTime dataValidataA, string prodotto, string listino)
        {
            var prodOff = await _db.ProdOffRel.AsNoTracking()
                .Where(x => x.Tts >= dataValiditaDa)
                .Where(x => x.Tte <= dataValidataA)
                .Where(x => EF.Functions.Like(x.ProdProdotto.ProdDescrizione, $"{prodotto}%"))
                .Where(x => EF.Functions.Like(x.Offerta.OffertaCod, $"{listino}%"))
                .ProjectTo<ElencoListiniView>(_mapper.ConfigurationProvider)
                .TagWith("MotoreListini.Repository.GetElencoListini")
                .TagWith("estrae i listini per popolare la pagina elenco listini - NUOVA VERSIONE")
                .FromCacheAsync(CacheEntryOptions, nameof(ProdOffRel));

            if (prodOff?.Any() == true)
                return new ResponseTyped<IEnumerable<ElencoListiniView>>
                {
                    Content = prodOff,
                    Errors = null,
                    ResultTyped = ResultType.Ok
                };

            return new ResponseTyped<IEnumerable<ElencoListiniView>>
            {
                Message = new StringBuilder("Elenco listini vuoto").ToString(),
                ResultTyped = ResultType.NotFound,
                Errors = null
            };
        }

        #endregion
    }
}