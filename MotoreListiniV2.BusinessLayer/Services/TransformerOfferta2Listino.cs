﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using MotoreListiniV2.BusinessLayer.Contracts;
using MotoreListiniV2.DataAccessLayer.Database;
using MotoreListiniV2.DataAccessLayer.Models.Offerta;
using MotoreListiniV2.Shared.Dto;
using MotoreListiniV2.Shared.ResponseBase;
using Z.EntityFramework.Plus;

namespace MotoreListiniV2.BusinessLayer.Services
{
    public class TransformerOfferta2Listino : ITransformerOfferta2Listino
    {
        private readonly OffertaContext _dboc;
        private readonly IMapper _mapper;
        private readonly IServiceProvider _serviceProvider;

        public TransformerOfferta2Listino(OffertaContext dboc, IMapper mapper, IServiceProvider serviceProvider)
        {
            _dboc = dboc ?? throw new ArgumentNullException(nameof(dboc));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
        }

        public async Task<ResponseTyped<string>> CreateListinoByOfferta(int idOfferta)
        {
            using var scope = _serviceProvider.CreateScope();
            var motoreListini =
                (IMotoreListiniOfferte) scope.ServiceProvider.GetRequiredService(typeof(IMotoreListiniOfferte));

            ProspectLam offLam = await _dboc.ProspectLam.AsNoTracking()
                .FirstOrDefaultAsync(x => x.ProspectLamId == idOfferta).ConfigureAwait(false);

            if (offLam == null)
                return await Task.FromResult(new ResponseTyped<string>
                {
                    Message = $"Nessuna offerta trovata con id {idOfferta.ToString()}",
                    ResultTyped = ResultType.NotFound,
                    Errors = null
                }).ConfigureAwait(false);


            SaveFullListinoEntity listino = _mapper.Map<SaveFullListinoEntity>(offLam);

            /*gestione prezzi*/
            SavePrezziEntity spe = _mapper.Map<SavePrezziEntity>(offLam);
            spe.TipoOfferteId =
                await GetIdTipoPrezzoListinoByOffLam(offLam.TsTipoServizioId, offLam.TipoPrezzoId ?? -1,
                    offLam.TipoFasceId ?? -1).ConfigureAwait(false);

            /*estraggo i valori dei prezzi*/
            var structTipoComponentePrezzo =
                await motoreListini.GetComponentiPrezzi(spe.TipoOfferteId).ConfigureAwait(false);

            if (structTipoComponentePrezzo.ResultTyped != ResultType.Ok)
                return await Task.FromResult(new ResponseTyped<string>
                {
                    Message = $"problemi nel creare il listino dall'offerta {idOfferta.ToString()}, {structTipoComponentePrezzo.Message}",
                    ResultTyped = ResultType.Unexpected
                }).ConfigureAwait(false);

            List<SaveComponentiPrezziEntity> listComponentiPrezziEntity = new List<SaveComponentiPrezziEntity>();

            if (string.Equals(listino.TipoServizio, "ee", StringComparison.OrdinalIgnoreCase))
            {
                int i = 0;
                string[] prezziFasce = offLam.PrezzoGas.TrimEnd(';').Split(';');
                foreach (ComponentiPrezziView cpw in structTipoComponentePrezzo.Content)
                {
                    SaveComponentiPrezziEntity priceUnit = new SaveComponentiPrezziEntity()
                    {
                        ComponentiPrezziId = cpw.ComponentiPrezziId,
                        Prezzo = Convert.ToDouble(prezziFasce[i], CultureInfo.CurrentCulture)
                    };

                    listComponentiPrezziEntity.Add(priceUnit);
                    i++;
                }
            }
            else if (string.Equals(listino.TipoServizio, "gas", StringComparison.OrdinalIgnoreCase))
            {
                IReadOnlyDictionary<string, string> prezziGas = new Dictionary<string, string>
                {
                    {"Prezzo del gas naturale", offLam.PrezzoGas},
                    {"Termini Fissi", offLam.TfGas},
                    {"Fee Fissa", "0"}
                };

                foreach (ComponentiPrezziView cpw in structTipoComponentePrezzo.Content)
                {
                    SaveComponentiPrezziEntity priceUnit = new SaveComponentiPrezziEntity()
                    {
                        ComponentiPrezziId = cpw.ComponentiPrezziId,
                        Prezzo = Convert.ToDouble(prezziGas[cpw.LabelGrafica], CultureInfo.CurrentCulture)
                    };

                    listComponentiPrezziEntity.Add(priceUnit);
                }
            }

            spe.SaveComponentiPrezziEntityList = listComponentiPrezziEntity;

            listino.SavePrezziEntityList = new List<SavePrezziEntity> {spe};


            var risEsito = await motoreListini.PostSaveFullListinoEntity(listino).ConfigureAwait(false);

            if (risEsito.ResultTyped == ResultType.Ok)
            {
                int idProdotto = await GetIdProdByDesc($"LISTINO_OFF_{idOfferta}").ConfigureAwait(false);

                ProdArea pa = new ProdArea
                {
                    ProdProdottoId = idProdotto,
                    ArAreaId = await GetIdArea("Lam").ConfigureAwait(false),
                    Tts = DateTime.Now,
                    Tte = null
                };

                await _dboc.SaveChangesAsync().ConfigureAwait(false);

                return await Task.FromResult(new ResponseTyped<string> {Content = listino.CodiceListino, ResultTyped = ResultType.Ok}).ConfigureAwait(false);
            }


            return await Task.FromResult(new ResponseTyped<string>
            {
                Message = $"problemi nel creare il listino dall'offerta {idOfferta}, {risEsito.Message}",
                ResultTyped = ResultType.Unexpected
            }).ConfigureAwait(false);
        }

        private async Task<int> GetIdArea(string areaDesc)
        {
            var lam = await _dboc.Area.AsNoTracking()
                .Where(x => x.Ar_Area_Cod == areaDesc && x.Tte == null)
                .DeferredFirstOrDefault()
                .FromCacheAsync(
                    new MemoryCacheEntryOptions {AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(1)},
                    nameof(Area))
                .ConfigureAwait(false);

            if (lam == null)
                return -1;

            return lam.Ar_Area_Id;
        }


        private async Task<int> GetIdTipoPrezzoListinoByOffLam(string ts, int tipoPrezzoidOffLam, int tipoFasceidOffLam)
        {
            var connection = _dboc.Database.GetDbConnection();
            if (connection.State == ConnectionState.Closed)
            {
                await connection.OpenAsync();
            }

            await using var command = connection.CreateCommand();
            command.CommandText = $"select [PROSPECT].[ufn_calcListinoTipoPrezziByOffLam] (@ts, @tipoPrezzoidOffLam, @tipoFasceidOffLam ) ";
            command.Parameters.Add(new SqlParameter("@ts", ts));
            command.Parameters.Add(new SqlParameter("@tipoPrezzoidOffLam", tipoPrezzoidOffLam));
            command.Parameters.Add(new SqlParameter("@tipoFasceidOffLam", tipoFasceidOffLam));

            var id = (int) await command.ExecuteScalarAsync().ConfigureAwait(false);


            return id;
        }

        private async Task<int> GetIdProdByDesc(string prodottoDescrizione)
        {
            var connection = _dboc.Database.GetDbConnection();
            if (connection.State == ConnectionState.Closed)
            {
                await connection.OpenAsync();
            }

            await using var command = connection.CreateCommand();
            command.CommandText = $"select top 1 prod_prodotto_id from prodotto where prod_descrizione = @prodDesc ";
            command.Parameters.Add(new SqlParameter("@prodDesc", prodottoDescrizione));


            var id = (int) await command.ExecuteScalarAsync().ConfigureAwait(false);

            return id;
        }
    }
}