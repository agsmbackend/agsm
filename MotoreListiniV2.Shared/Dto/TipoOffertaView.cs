﻿namespace MotoreListiniV2.Shared.Dto
{
    public record TipoOffertaView
    {
        public int TipoOffertaId { get; init; }
        public string TipoOffertaDesc { get; init; }
    }
}