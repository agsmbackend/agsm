﻿using System;
using System.Collections.Generic;

namespace MotoreListiniV2.Shared.Dto
{
    public record ElencoCategorieFullView
    {
        public ElencoCategorieFullView()
        {
            Configurazioni = new List<ConfigurazioneView>();
        }

        public int IdCategoria { get; init; }
        public string TsTipoServizioId { get; set; }
        public string CatDescrizione { get; set; }
        public ICollection<ConfigurazioneView> Configurazioni { get; init; }
    }

    public class ConfigurazioneView
    {
        public int ConfId { get; set; }
        public int CatId { get; set; }
        public string ConfNome { get; set; }
        public string ConfTesto { get; set; }
        public int ConfTextMaxLength { get; set; }
    }

    //public class OffertaPrezziView
    //{
    //    public OffertaPrezziView()
    //    {
    //        OffComponentePrezzi = new List<OffertaComponentePrezziView>();
    //    }

    //    public int TipoPrezziId { get; init; }
    //    public string TipoPrezzo { get; init; }
    //    public int IdTipoOfferta { get; init; }
    //    public int ValMeseDa { get; init; }
    //    public int ValMeseA { get; init; }
    //    public int Percentuale { get; init; }

    //    public ICollection<OffertaComponentePrezziView> OffComponentePrezzi { get; init; }
    //}

    //public class OffertaComponentePrezziView
    //{
    //    public int IdPrezzo { get; init; }
    //    public int ComponentiPrezziId { get; init; }
    //    public string LabelGrafica { get; init; }
    //    public double Prezzo { get; init; }
    //    public string UnitaMisuraPrezzo { get; init; }
    //}

    //public class OffertaComponentiFatturazioneView
    //{
    //    public int IdOffertaComponente { get; init; }
    //    public int IdComponente { get; init; }
    //    public string Componente { get; init; }
    //    public double PrezzoAltraComp { get; init; }
    //    public string UnitaMisuraAltraComp { get; init; }
    //    public int ValMeseDaAltraComp { get; init; }
    //    public int ValMeseAAltraComp { get; init; }
    //    public bool IsOpzionaleAltraComp { get; init; }
    //}
}