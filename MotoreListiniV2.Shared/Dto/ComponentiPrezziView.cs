﻿namespace MotoreListiniV2.Shared.Dto
{
    public record ComponentiPrezziView
    {
        public int ComponentiPrezziId { get; init; }
        public string Unitamisura { get; init; }
        public string LabelGrafica { get; init; }
    }

    public record ComponentiPrezziGenericiView
    {
        public int Id { get; init; }
        public string Unitamisura { get; init; }
        public string Nome { get; init; }
    }
}