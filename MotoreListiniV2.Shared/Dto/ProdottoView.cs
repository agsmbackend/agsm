﻿namespace MotoreListiniV2.Shared.Dto
{
    public record ProdottoView
    {
        public int ProdProdottoId { get; init; }
        public string ProdDescrizione { get; init; }
        public bool IsIndicizzato { get; init; }
    }
}