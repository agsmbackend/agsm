﻿using System;
using System.Collections.Generic;

namespace MotoreListiniV2.Shared.Dto
{
    public record ElencoCaratteristicheFullView
    {
        public ElencoCaratteristicheFullView()
        {
            Caratteristiche = new List<CaratteristicaView>();
        }

        public int IdProdotto { get; set; }

        public string NomeProdotto { get; set; }

        public string TipoServizio { get; set; }

        public string TipoTariffa { get; set; }

        public ICollection<CaratteristicaView> Caratteristiche { get; init; }
    }

    public class CaratteristicaView
    {
        public int OffCaratteristicheId { get; set; }
        public int? OffertaId { get; set; }
        public int? CategoriaId { get; set; }
        public string CategoriaDesc { get; set; }
        public int? ConfigurazioneId { get; set; }
        public string ConfigurazioneText { get; set; }
        public string FreeText { get; set; }

    }

}