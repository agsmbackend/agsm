﻿using System;
using System.Collections.Generic;

namespace MotoreListiniV2.Shared.Dto
{
    public record ElencoListiniFullView
    {
        public int IdProdotto { get; init; }
        public string Prodotto { get; init; }
        public string TipoServizio { get; init; }
        public string Tariffa { get; init; }
        public int IdListino { get; init; }
        public string CodOfferta { get; init; }
        public string Descrizione { get; init; }
        public string Scheda_prodotto { get; init; }
        public int Mese { get; init; }
        public int Anno { get; init; }
        public int Revisione { get; init; }
        public int Trimestre { get; init; }
        public string Username { get; init; }
        public int TerminiPagamento { get; init; }
        public bool ValiditaPrezzo { get; init; }
        public bool PassaFisso { get; init; }
        public DateTime InizioValidita { get; init; }
        public DateTime FineValidita { get; init; }
        public DateTime InizioAttivazione { get; init; }
        public DateTime FineAttivazione { get; init; }
        public bool IsEditable { get; init; }
    }

    public record ElencoListiniView
    {
        public int IdProdotto { get; init; }
        public string Prodotto { get; init; }
        public string TipoServizio { get; init; }
        public string Tariffa { get; init; }
        public int IdOfferta { get; init; }
        public string CodOfferta { get; init; }
        public string Descrizione { get; init; }
        public string Scheda_confrontabilita { get; init; }
        public string Scheda_prodotto { get; init; }
        public int? Mese { get; set; }
        public int? Anno { get; set; }
        public int? Trim { get; set; }
        public int Revisione { get; init; }
        public float PrezzoEnergia { get; init; }
        public DateTime InizioValidita { get; init; }
        public DateTime FineValidita { get; init; }
    }

    public class OffertaPrezziView
    {
        public OffertaPrezziView()
        {
            OffComponentePrezzi = new List<OffertaComponentePrezziView>();
        }

        public int TipoPrezziId { get; init; }
        public string TipoPrezzo { get; init; }
        public int IdTipoOfferta { get; init; }
        public int ValMeseDa { get; init; }
        public int ValMeseA { get; init; }
        public int Percentuale { get; init; }

        public ICollection<OffertaComponentePrezziView> OffComponentePrezzi { get; init; }
    }

    public class OffertaComponentePrezziView
    {
        public int IdPrezzo { get; init; }
        public int ComponentiPrezziId { get; init; }
        public string LabelGrafica { get; init; }
        public double Prezzo { get; init; }
        public string UnitaMisuraPrezzo { get; init; }
    }

    public class OffertaComponentiFatturazioneView
    {
        public int IdOffertaComponente { get; init; }
        public int IdComponente { get; init; }
        public string Componente { get; init; }
        public double PrezzoAltraComp { get; init; }
        public string UnitaMisuraAltraComp { get; init; }
        public int ValMeseDaAltraComp { get; init; }
        public int ValMeseAAltraComp { get; init; }
        public bool IsOpzionaleAltraComp { get; init; }
    }
}