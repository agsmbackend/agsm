﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using MotoreListiniV2.DataAccessLayer.Database;
using MotoreListiniV2.DataAccessLayer.Extensions;
using UtilitiesGSA.Extensions;

namespace MotoreListiniV2.Shared.Dto
{
    public class SaveCategoriaEntity : AbstractValidatableObject
    {
        //se valorizzato > 0 si tratta di duplica listino altrimenti di nuovo listino
        public int IdCategoria { get; set; }

        [Required] public string TipoServizio { get; set; }

        public string Descrizione { get; set; }
        public bool Attiva { get; set; }

        public override async Task<IEnumerable<ValidationResult>> ValidateAsync(ValidationContext validationContext,
            CancellationToken cancellation)
        {
            var errors = new List<ValidationResult>();

            var db = validationContext.GetService<ListinoContext>();

            var (esitoCheckResultDuplicatedListino, listValidationResultDuplicatedListino) = await ValidateDuplicatedCategoria(db, errors).ConfigureAwait(false);
            if (esitoCheckResultDuplicatedListino) return listValidationResultDuplicatedListino;

            //New Listino
            if (await db.ExistsProdottoAsync(Descrizione).ConfigureAwait(false))
            {
                errors.Add(new ValidationResult($"Categoria {Descrizione} già esistente"));
            }

            if (string.IsNullOrWhiteSpace(Descrizione))
                errors.Add(new ValidationResult("Descrizione categoria obbligatoria"));

            return errors;
        }

        private async Task<(bool check, IEnumerable<ValidationResult> listResult)> ValidateDuplicatedCategoria(ListinoContext db, List<ValidationResult> results)
        {
            var validateNotSuperated = false;

            if (IdCategoria > 0)
            {
                if (await db.Categoria.FindAsync(IdCategoria) is null)
                {
                    results.Add(new ValidationResult($"Non esiste nessuna categoria con chiave {IdCategoria.ToString(CultureInfo.InvariantCulture)}"));
                    validateNotSuperated = true;
                }

                if (await db.ExistsCodListinoAsync(Descrizione).ConfigureAwait(false))
                {
                    results.Add(new ValidationResult($"Descrizione {Descrizione} già esistente"));
                    validateNotSuperated = true;
                }

            }

            return validateNotSuperated ? (true, results) : (false, results);
        }

    }

}