﻿namespace MotoreListiniV2.Shared.Dto
{
    public record UsoView
    {
        public int UsoId { get; init; }
        public string UsoDesc { get; init; }
        public string TipoServizioId { get; init; }
    }
}