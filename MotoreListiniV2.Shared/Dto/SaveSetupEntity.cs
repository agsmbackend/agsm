﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using MotoreListiniV2.DataAccessLayer.Database;
using MotoreListiniV2.DataAccessLayer.Extensions;
using UtilitiesGSA.Extensions;

namespace MotoreListiniV2.Shared.Dto
{
    public class SaveSetupEntity : AbstractValidatableObject
    {
        //se valorizzato > 0 si tratta di edit listino (vince sui controlli di idprodotto)
        public int IdListino { get; set; }

        //se valorizzato > 0 si tratta di duplica listino altrimenti di nuovo listino
        public int IdProdotto { get; set; }

        public string NomeProdotto { get; set; }

        public string TipoServizio { get; set; }

        public string TipoTariffa { get; set; }



        //[Required] public string CodiceListino { get; set; }

        //public string Descrizione { get; set; } = string.Empty;

        //[Required]
        //[Range(1, 12, ErrorMessage = "Mese deve essere un valore compreso tra 1 e 12")]
        //public int Mese { get; set; }

        //[Required]
        //[RegularExpression(@"^(20)\d{2}$",
        //    ErrorMessage = "Anno deve essere in formato numerico yyyy con le prime tre cifre uguali a 20")]
        //public int Anno { get; set; }

        //public bool ValiditaPrezzo { get; set; }

        //[Range(1, 999, ErrorMessage = "Revisione deve essere un valore compreso tra 1 e 999")]
        //public int Revisione { get; set; } = 1;

        //[Required]
        //[Range(0, 4, ErrorMessage = "Trimestre deve essere un valore compreso tra 0 e 4")]
        //public int Trimestre { get; set; } = 1;

        //[Required] public DateTime InizioValidita { get; set; }

        //[Required] public DateTime FineValidita { get; set; }

        //[Required] public DateTime InizioAttivazione { get; set; }

        //[Required] public DateTime FineAttivazione { get; set; }

        //public string SpPDF { get; set; }

        //[Required] public int TerminiPagamento { get; set; }

        //public bool Fixed { get; set; }

        public IEnumerable<SaveCaratteristicheEntity> CaratteristicheList { get; set; } = new List<SaveCaratteristicheEntity>();

        //public IEnumerable<SaveComponentiFatturazioneEntity> SaveComponentiFatturazioneEntityList { get; set; } =
        //    new List<SaveComponentiFatturazioneEntity>();


        public override async Task<IEnumerable<ValidationResult>> ValidateAsync(ValidationContext validationContext,
            CancellationToken cancellation)
        {
            var errors = new List<ValidationResult>();

            return errors;
        }

        //private async Task<(bool check, IEnumerable<ValidationResult> listResult)> ValidateDuplicatedListino(ListinoContext db, List<ValidationResult> results)
        //{
        //    var validateNotSuperated = false;

        //    if (IdProdotto > 0)
        //    {
        //        if (await db.Prodotto.FindAsync(IdProdotto) is null)
        //        {
        //            results.Add(new ValidationResult($"Non esiste nessun prodotto con chiave {IdProdotto.ToString(CultureInfo.InvariantCulture)}"));
        //            validateNotSuperated = true;
        //        }

        //        if (await db.ExistsCodListinoAsync(CodiceListino).ConfigureAwait(false))
        //        {
        //            results.Add(new ValidationResult($"Codice Listino {CodiceListino} già esistente"));
        //            validateNotSuperated = true;
        //        }

        //        if (ValiditaPrezzo)
        //        {
        //            if (await db.IsOkNewDateDuplicaAsync(IdProdotto, ControlliListino.Source.AttivazioneProdotto, InizioAttivazione, FineAttivazione))
        //            {
        //                results.Add(new ValidationResult($"Nell'intervallo indicato di attivazione esiste già un altro listino per il prodotto {NomeProdotto}"));
        //                validateNotSuperated = true;
        //            }
        //        }
        //        else
        //        {
        //            if (await db.IsOkNewDateDuplicaAsync(IdProdotto, ControlliListino.Source.Prodotto, InizioValidita, FineValidita))
        //            {
        //                results.Add(new ValidationResult($"Nell'intervallo indicato di validità esiste già un altro listino per il prodotto {NomeProdotto}"));
        //                validateNotSuperated = true;
        //            }
        //        }
        //    }

        //    return validateNotSuperated ? (true, results) : (false, results);
        //}

        //private async Task<(bool check, IEnumerable<ValidationResult> listResult)> ValidateEditListino(ListinoContext db, ICollection<ValidationResult> results)
        //{
        //    if (IdListino > 0)
        //    {
        //        if (ValiditaPrezzo)
        //        {
        //            if (await db.IsOkNewDateDuplicaAsync(IdListino, ControlliListino.Source.AttivazioneListino, InizioAttivazione, FineAttivazione))
        //                results.Add(new ValidationResult(
        //                    $"Nell'intervallo indicato di attiviazione esiste già un altro listino per il prodotto {NomeProdotto}"));

        //            return (false, results);
        //        }

        //        if (await db.IsOkNewDateDuplicaAsync(IdListino, ControlliListino.Source.Listino, InizioValidita, FineValidita))
        //        {
        //            results.Add(new ValidationResult(
        //                $"Nell'intervallo indicato di validità esiste già un altro listino per il prodotto {NomeProdotto}"));

        //            return (false, results);
        //        }
        //    }

        //    return (false, results);
        //}
    }

    public class SaveCaratteristicheEntity
    {
        public int OffertaId { get; set; }

        public int CategoriaId { get; set; }

        public int ConfigurazioneId { get; set; }

        public string FreeText { get; set; }

    }
}