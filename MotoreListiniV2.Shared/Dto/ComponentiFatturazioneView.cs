﻿namespace MotoreListiniV2.Shared.Dto
{
    public record ComponentiFatturazioneView
    {
        public int Id { get; init; }
        public string Nome { get; init; }
        public string Unitamisura { get; init; }
    }
}