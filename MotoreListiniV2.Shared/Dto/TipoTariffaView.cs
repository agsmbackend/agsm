﻿namespace MotoreListiniV2.Shared.Dto
{
    public record TipoTariffaView
    {
        public int TipoTariffaId { get; init; }
        public string TipoTariffaDesc { get; init; }
        public string TipoServizioId { get; init; }
    }
}