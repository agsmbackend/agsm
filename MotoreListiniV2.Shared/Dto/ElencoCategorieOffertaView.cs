﻿using System;
using System.Collections.Generic;

namespace MotoreListiniV2.Shared.Dto
{
    public record ElencoCategorieOffertaFullView
    {
        public ElencoCategorieOffertaFullView()
        {
            Configurazioni = new List<ConfigurazioneOffertaView>();
        }

        public int? IdOfferta { get; init; }
        public int IdCategoria { get; init; }
        public string TsTipoServizioId { get; set; }
        public string Tariffa { get; set; }
        public string CatDescrizione { get; set; }
        public string Testo { get; set; }
        public ICollection<ConfigurazioneOffertaView> Configurazioni { get; init; }
    }

    public class ConfigurazioneOffertaView
    {
        public int ConfId { get; set; }
        public int CatId { get; set; }
        public string ConfNome { get; set; }
        public string ConfTesto { get; set; }
        public bool Selezionata { get; set; }
    }

}