﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using MotoreListiniV2.DataAccessLayer.Database;
using MotoreListiniV2.DataAccessLayer.Extensions;
using UtilitiesGSA.Extensions;

namespace MotoreListiniV2.Shared.Dto
{
    public class FinalizeListinoEntity : AbstractValidatableObject
    {
        //se valorizzato > 0 si tratta di duplica listino altrimenti di nuovo listino
        public int IdOfferta { get; set; }
        [Required] public string CodiceListino { get; set; }
        public string NomeOfferta { get; set; }
        public string Descrizione { get; set; } = string.Empty;

        [Required]
        [Range(1, 12, ErrorMessage = "Mese deve essere un valore compreso tra 1 e 12")]
        public int Mese { get; set; }

        [Required]
        [RegularExpression(@"^(20)\d{2}$",
            ErrorMessage = "Anno deve essere in formato numerico yyyy con le prime tre cifre uguali a 20")]
        public int Anno { get; set; }

        [Required]
        [Range(0, 4, ErrorMessage = "Trimestre deve essere un valore compreso tra 0 e 4")]
        public int Trimestre { get; set; } = 1;

        [Range(1, 999, ErrorMessage = "Revisione deve essere un valore compreso tra 1 e 999")]
        public int? Rev { get; set; }
        public string Uso { get; set; }
        public string MateriaPrima { get; set; }
        public string ScPDF { get; set; }
        public string SpPDF { get; set; }
        public float? PrezzoEnergia { get; set; }
        public int? PercMateriaEnergia { get; set; }
        public int? PercComponenteEnergia { get; set; }
        public int? PercSpesaTrasportoGestione { get; set; }
        public int? PercOneriSistema { get; set; }
        public int? PercAsos { get; set; }
        public int? PercCompExtra1 { get; set; }
        public int? PercCompExtra2 { get; set; }
        public int? PercCompExtra3 { get; set; }
        [Required] public DateTime InizioValidita { get; set; }
        [Required] public DateTime FineValidita { get; set; }

        public IEnumerable<SaveCaratteristicheEntity> CaratteristicheList { get; set; } = new List<SaveCaratteristicheEntity>();

        //public IEnumerable<SaveComponentiFatturazioneEntity> SaveComponentiFatturazioneEntityList { get; set; } =
        //    new List<SaveComponentiFatturazioneEntity>();


        public override async Task<IEnumerable<ValidationResult>> ValidateAsync(ValidationContext validationContext,
            CancellationToken cancellation)
        {
            var errors = new List<ValidationResult>();

            return errors;
        }
        
    }

}