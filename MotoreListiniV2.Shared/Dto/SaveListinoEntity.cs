﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using MotoreListiniV2.DataAccessLayer.Database;
using MotoreListiniV2.DataAccessLayer.Extensions;
using UtilitiesGSA.Extensions;

namespace MotoreListiniV2.Shared.Dto
{
    public class SaveFullListinoEntity : AbstractValidatableObject
    {
        //se valorizzato > 0 si tratta di duplica listino altrimenti di nuovo listino
        public int IdProdotto { get; set; }

        public string NomeProdotto { get; set; }

        public string TipoServizio { get; set; }

        public string TipoTariffa { get; set; }

        //se valorizzato > 0 si tratta di edit listino (vince sui controlli di idprodotto)
        public int IdListino { get; set; }

        [Required] public string CodiceListino { get; set; }

        public string Descrizione { get; set; } = string.Empty;

        [Required]
        [Range(1, 12, ErrorMessage = "Mese deve essere un valore compreso tra 1 e 12")]
        public int Mese { get; set; }

        [Required]
        [RegularExpression(@"^(20)\d{2}$",
            ErrorMessage = "Anno deve essere in formato numerico yyyy con le prime tre cifre uguali a 20")]
        public int Anno { get; set; }

        public bool ValiditaPrezzo { get; set; }

        [Range(1, 999, ErrorMessage = "Revisione deve essere un valore compreso tra 1 e 999")]
        public int Revisione { get; set; } = 1;

        [Required]
        [Range(0, 4, ErrorMessage = "Trimestre deve essere un valore compreso tra 0 e 4")]
        public int Trimestre { get; set; } = 1;

        [Required] public DateTime InizioValidita { get; set; }

        [Required] public DateTime FineValidita { get; set; }

        [Required] public DateTime InizioAttivazione { get; set; }

        [Required] public DateTime FineAttivazione { get; set; }

        public string SpPDF { get; set; }

        [Required] public int TerminiPagamento { get; set; }

        public bool Fixed { get; set; }

        public IEnumerable<SavePrezziEntity> SavePrezziEntityList { get; set; } = new List<SavePrezziEntity>();

        public IEnumerable<SaveComponentiFatturazioneEntity> SaveComponentiFatturazioneEntityList { get; set; } =
            new List<SaveComponentiFatturazioneEntity>();


        public override async Task<IEnumerable<ValidationResult>> ValidateAsync(ValidationContext validationContext,
            CancellationToken cancellation)
        {
            var errors = new List<ValidationResult>();

            if (!FineValidita.IsValidateDateRange(InizioValidita))
            {
                errors.Add(new ValidationResult("Fine validità deve essere maggiore della data di inizio validità"));
            }

            if (!FineAttivazione.IsValidateDateRange(InizioAttivazione))
            {
                errors.Add(new ValidationResult("Fine attivazione deve essere maggiore della data di inizio attivazione"));
            }

            var db = validationContext.GetService<ListinoContext>();

            var (esitoCheckResultEditListino, listValidationResultEditListino) = await ValidateEditListino(db, errors).ConfigureAwait(false);
            if (esitoCheckResultEditListino) return listValidationResultEditListino;

            var (esitoCheckResultDuplicatedListino, listValidationResultDuplicatedListino) = await ValidateDuplicatedListino(db, errors).ConfigureAwait(false);
            if (esitoCheckResultDuplicatedListino) return listValidationResultDuplicatedListino;

            //New Listino
            if (await db.ExistsProdottoAsync(NomeProdotto).ConfigureAwait(false))
            {
                errors.Add(new ValidationResult($"Prodotto {NomeProdotto} già esistente"));
            }

            if (await db.ExistsCodListinoAsync(CodiceListino).ConfigureAwait(false))
            {
                errors.Add(new ValidationResult($"Codice Listino {CodiceListino} già esistente"));
            }

            if (string.IsNullOrWhiteSpace(NomeProdotto))
                errors.Add(new ValidationResult("Nome Prodotto obbligatorio"));

            var tsPattern = new Regex("^(EE|GAS)$", RegexOptions.None);
            if (!tsPattern.IsMatch(TipoServizio))
                errors.Add(new ValidationResult("Per Tipo Servizio sono ammessi solamente i valori EE/GAS"));

            if (string.IsNullOrWhiteSpace(TipoTariffa))
                errors.Add(new ValidationResult("Tipo Tariffa obbligatoria"));

            return errors;
        }

        private async Task<(bool check, IEnumerable<ValidationResult> listResult)> ValidateDuplicatedListino(ListinoContext db, List<ValidationResult> results)
        {
            var validateNotSuperated = false;

            if (IdProdotto > 0)
            {
                if (await db.Prodotto.FindAsync(IdProdotto) is null)
                {
                    results.Add(new ValidationResult($"Non esiste nessun prodotto con chiave {IdProdotto.ToString(CultureInfo.InvariantCulture)}"));
                    validateNotSuperated = true;
                }

                if (await db.ExistsCodListinoAsync(CodiceListino).ConfigureAwait(false))
                {
                    results.Add(new ValidationResult($"Codice Listino {CodiceListino} già esistente"));
                    validateNotSuperated = true;
                }

                if (ValiditaPrezzo)
                {
                    if (await db.IsOkNewDateDuplicaAsync(IdProdotto, ControlliListino.Source.AttivazioneProdotto, InizioAttivazione, FineAttivazione))
                    {
                        results.Add(new ValidationResult($"Nell'intervallo indicato di attivazione esiste già un altro listino per il prodotto {NomeProdotto}"));
                        validateNotSuperated = true;
                    }
                }
                else
                {
                    if (await db.IsOkNewDateDuplicaAsync(IdProdotto, ControlliListino.Source.Prodotto, InizioValidita, FineValidita))
                    {
                        results.Add(new ValidationResult($"Nell'intervallo indicato di validità esiste già un altro listino per il prodotto {NomeProdotto}"));
                        validateNotSuperated = true;
                    }
                }
            }

            return validateNotSuperated ? (true, results) : (false, results);
        }

        private async Task<(bool check, IEnumerable<ValidationResult> listResult)> ValidateEditListino(ListinoContext db, ICollection<ValidationResult> results)
        {
            if (IdListino > 0)
            {
                if (ValiditaPrezzo)
                {
                    if (await db.IsOkNewDateDuplicaAsync(IdListino, ControlliListino.Source.AttivazioneListino, InizioAttivazione, FineAttivazione))
                        results.Add(new ValidationResult(
                            $"Nell'intervallo indicato di attiviazione esiste già un altro listino per il prodotto {NomeProdotto}"));

                    return (false, results);
                }

                if (await db.IsOkNewDateDuplicaAsync(IdListino, ControlliListino.Source.Listino, InizioValidita, FineValidita))
                {
                    results.Add(new ValidationResult(
                        $"Nell'intervallo indicato di validità esiste già un altro listino per il prodotto {NomeProdotto}"));

                    return (false, results);
                }
            }

            return (false, results);
        }
    }

    public class SavePrezziEntity
    {
        [Range(0, 999, ErrorMessage = "Mese da deve essere un valore compreso tra 0 e 999")]
        public int MeseDa { get; set; }

        [Range(1, 999, ErrorMessage = "Mese a deve essere un valore compreso tra 1 e 999")]
        public int MeseA { get; set; }

        public int TipoPrezziId { get; set; }

        public int TipoOfferteId { get; set; }

        [Range(1, 100, ErrorMessage = "Percentuale deve essere un valore compreso tra 1 e 100")]
        public int Percentuale { get; set; }

        public IEnumerable<SaveComponentiPrezziEntity> SaveComponentiPrezziEntityList { get; set; } =
            new List<SaveComponentiPrezziEntity>();
    }

    public class SaveComponentiPrezziEntity
    {
        public int ComponentiPrezziId { get; set; }

        public double Prezzo { get; set; }
    }

    public class SaveComponentiFatturazioneEntity
    {
        [Range(0, 999, ErrorMessage = "Mese da deve essere un valore compreso tra 0 e 999")]
        public int MeseDa { get; set; }

        [Range(1, 999, ErrorMessage = "Mese a deve essere un valore compreso tra 1 e 999")]
        public int MeseA { get; set; }

        public int ComponentiFatturazioneId { get; set; }

        public double Value { get; set; }

        public bool IsOptional { get; set; }
    }

    internal static class ValidationContextExtensions
    {
        public static T GetService<T>(this ValidationContext validationContext)
        {
            _ = validationContext ?? throw new ArgumentNullException(nameof(validationContext));

            return (T) validationContext.GetService(typeof(T));
        }
    }
}