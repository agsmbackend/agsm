﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using MotoreListiniV2.DataAccessLayer.Database;
using MotoreListiniV2.DataAccessLayer.Extensions;
using UtilitiesGSA.Extensions;

namespace MotoreListiniV2.Shared.Dto
{
    public class SaveConfigurazioneEntity : AbstractValidatableObject
    {
        //se valorizzato > 0 si tratta di duplica listino altrimenti di nuovo listino
        public int IdConfigurazione { get; set; }

        [Required] public int? IdCategoria { get; set; }

        public string NomeConfigurazione { get; set; }
        public string TestoConfigurazione { get; set; }
        public int TextMaxConfigurazione { get; set; }

        public override async Task<IEnumerable<ValidationResult>> ValidateAsync(ValidationContext validationContext,
            CancellationToken cancellation)
        {
            var errors = new List<ValidationResult>();

            var db = validationContext.GetService<ListinoContext>();

            var (esitoCheckResultDuplicatedConfigurazione, listValidationResultDuplicatedConfigurazione) = await ValidateDuplicatedConfigurazione(db, errors).ConfigureAwait(false);
            if (esitoCheckResultDuplicatedConfigurazione) return listValidationResultDuplicatedConfigurazione;

            //New Listino
            if (await db.ExistsProdottoAsync(NomeConfigurazione).ConfigureAwait(false))
            {
                errors.Add(new ValidationResult($"Categoria {NomeConfigurazione} già esistente"));
            }

            if (string.IsNullOrWhiteSpace(TestoConfigurazione))
                errors.Add(new ValidationResult("Testo categoria obbligatorio"));

            if (TextMaxConfigurazione <= 0)
                errors.Add(new ValidationResult("Numero massimo caratteri testo obbligatorio"));

            return errors;
        }

        private async Task<(bool check, IEnumerable<ValidationResult> listResult)> ValidateDuplicatedConfigurazione(ListinoContext db, List<ValidationResult> results)
        {
            var validateNotSuperated = false;

            if (IdConfigurazione > 0)
            {
                if (await db.Configurazione.FindAsync(IdConfigurazione) is null)
                {
                    results.Add(new ValidationResult($"Non esiste nessuna categoria con chiave {IdConfigurazione.ToString(CultureInfo.InvariantCulture)}"));
                    validateNotSuperated = true;
                }

                if (await db.ExistsCodListinoAsync(TestoConfigurazione).ConfigureAwait(false))
                {
                    results.Add(new ValidationResult($"Descrizione {TestoConfigurazione} già esistente"));
                    validateNotSuperated = true;
                }

            }

            return validateNotSuperated ? (true, results) : (false, results);
        }

    }

}