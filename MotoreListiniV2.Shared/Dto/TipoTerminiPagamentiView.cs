﻿namespace MotoreListiniV2.Shared.Dto
{
    public record TipoTerminiPagamentiView
    {
        public int TipoTerminiPagamentiId { get; init; }
        public string TipoTerminiPagamentiDescrizione { get; init; }
        public bool IsDefault { get; init; }
    }
}