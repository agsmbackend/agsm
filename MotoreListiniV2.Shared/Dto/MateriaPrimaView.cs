﻿namespace MotoreListiniV2.Shared.Dto
{
    public record MateriaPrimaView
    {
        public int MpId { get; init; }
        public string MpDesc { get; init; }
        public string TipoServizioId { get; init; }
    }
}