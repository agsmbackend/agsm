﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace MotoreListiniV2.Shared.ResponseBase
{
    public class ResponseTyped<T>
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public ResultType ResultTyped { get; init; }

        public T Content { get; init; }
        public string Message { get; init; }
        public IEnumerable<string> Errors { get; init; } = new List<string>();

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}