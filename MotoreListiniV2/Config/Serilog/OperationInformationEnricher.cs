﻿using System.Diagnostics;
using Serilog.Core;
using Serilog.Events;

namespace MotoreListiniV2.Config.Serilog
{
    public class OperationInformationEnricher: ILogEventEnricher
    {
        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            var activity = Activity.Current;
            if (activity == null) return;

            logEvent.AddPropertyIfAbsent(new LogEventProperty("CorrelationId", new ScalarValue(activity.RootId)));
            logEvent.AddPropertyIfAbsent(new LogEventProperty("OperationId", new ScalarValue(activity.Id)));
            logEvent.AddPropertyIfAbsent(new LogEventProperty("ParentId", new ScalarValue(activity.ParentId)));
            logEvent.AddPropertyIfAbsent(new LogEventProperty("Release", new ScalarValue(typeof(Program).Assembly.GetName().Version)));
        }
    }
}