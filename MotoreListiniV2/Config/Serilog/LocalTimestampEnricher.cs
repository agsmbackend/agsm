﻿using Serilog.Core;
using Serilog.Events;

namespace MotoreListiniV2.Config.Serilog
{
    public class LocalTimestampEnricher : ILogEventEnricher
    {
        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            logEvent?.AddPropertyIfAbsent(propertyFactory?.CreateProperty("LocalTimestamp", logEvent.Timestamp.ToLocalTime().ToString()));
        }
    }
}