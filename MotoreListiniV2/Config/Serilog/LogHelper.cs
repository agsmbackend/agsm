﻿using System;
using Microsoft.AspNetCore.Http;
using Serilog;
using Serilog.Events;

namespace MotoreListiniV2.Config.Serilog
{
    public static class LogHelper
    {
        public static void EnrichFromRequest(IDiagnosticContext diagnosticContext, HttpContext httpContext)
        {
            _ = diagnosticContext ?? throw new System.ArgumentNullException(nameof(diagnosticContext));
            _ = httpContext ?? throw new System.ArgumentNullException(nameof(httpContext));

            var request = httpContext.Request;

            // Set all the common properties available for every request
            diagnosticContext.Set("Host", request.Host.ToString());
            diagnosticContext.Set("Protocol", request.Protocol);
            diagnosticContext.Set("Scheme", request.Scheme);

            // Only set it if available. You're not sending sensitive data in a querystring right?!
            if (request.QueryString.HasValue)
            {
                diagnosticContext.Set("QueryString", request.QueryString.Value);
            }

            // Set the content-type of the ResponseApi at this point
            diagnosticContext.Set("ContentType", httpContext.Response.ContentType);

            // Retrieve the IEndpointFeature selected for the request
            var endpoint = httpContext.GetEndpoint();
            if (endpoint is { } resEndPoint)
            {
                diagnosticContext.Set("EndpointName", resEndPoint.DisplayName);
            }
        }

        public static LogEventLevel ExcludeHealthChecks(HttpContext ctx, double _, Exception ex) =>
            ex is not null
                ? LogEventLevel.Error
                : IsHealthCheckEndpoint(ctx)
                    ? LogEventLevel.Verbose // Was a health check, use Verbose
                    : GetLogLevelByStatusCode(ctx.Response.StatusCode);

        private static bool IsHealthCheckEndpoint(HttpContext httpContext)
        {
            var endpoint = httpContext.GetEndpoint();
            return endpoint is { }
                   && string.Equals(endpoint.DisplayName, "Health checks", StringComparison.Ordinal);
        }

        private static LogEventLevel GetLogLevelByStatusCode(int statusCode) =>
            statusCode switch
            {
                < 200 => LogEventLevel.Warning,
                > 299 and < 500 => LogEventLevel.Warning,
                >= 500 => LogEventLevel.Error,
                _ => LogEventLevel.Information
            };
    }
}