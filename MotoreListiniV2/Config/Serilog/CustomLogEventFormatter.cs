﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Serilog.Events;
using Serilog.Formatting;
using Serilog.Formatting.Json;

namespace MotoreListiniV2.Config.Serilog
{
    public class CustomLogEventFormatter : ITextFormatter
    {
        private static readonly JsonValueFormatter ValueFormatter = new(typeTagName: null);
        private const string COMMA_DELIMITER = ",";

        private const string TIMESTAMP_FIELD = "Timestamp";
        private const string LEVEL_FIELD = "Level";
        private const string MESSAGE_FIELD = "Message";
        private const string EXCEPTION_FIELD = "Exception";

        public static CustomLogEventFormatter Formatter { get; } = new();

        public void Format(LogEvent logEvent, TextWriter output)
        {
            _ = logEvent ?? throw new ArgumentNullException(nameof(logEvent));
            _ = output ?? throw new ArgumentNullException(nameof(output));

            output.Write("{");

            var precedingDelimiter = string.Empty;

            //Write(TIMESTAMP_FIELD, logEvent.Timestamp.DateTime.ToString("o"));                    // Local Time
            Write(TIMESTAMP_FIELD,
                logEvent.Timestamp.ToUniversalTime().DateTime
                    .ToString("o", CultureInfo.InvariantCulture)); // Convert to UTC

            Write(LEVEL_FIELD, logEvent.Level.ToString());
            Write(MESSAGE_FIELD, logEvent.RenderMessage());

            if (logEvent.Exception != null)
            {
                Write(EXCEPTION_FIELD, logEvent.Exception.ToString());
            }

            if (logEvent.Properties.Any())
            {
                output.Write(precedingDelimiter);
                WriteProperties(logEvent.Properties, output);
            }

            output.Write("}");

            void Write(string name, string value)
            {
                output.Write(precedingDelimiter);
                precedingDelimiter = COMMA_DELIMITER;
                JsonValueFormatter.WriteQuotedJsonString(name, output);
                output.Write(":");
                JsonValueFormatter.WriteQuotedJsonString(value, output);
            }
        }

        private static void WriteProperties(IReadOnlyDictionary<string, LogEventPropertyValue> properties,
            TextWriter output)
        {
            output.Write("\"Properties\":{");

            var precedingDelimiter = string.Empty;
            foreach (var (key, value) in properties)
            {
                output.Write(precedingDelimiter);
                precedingDelimiter = COMMA_DELIMITER;
                JsonValueFormatter.WriteQuotedJsonString(key, output);
                output.Write(':');
                ValueFormatter.Format(value, output);
            }

            output.Write('}');
        }
    }
}