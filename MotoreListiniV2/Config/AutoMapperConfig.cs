﻿using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using MotoreListiniV2.BusinessLayer;

namespace MotoreListiniV2.Config
{
    public static class AutoMapperConfig
    {
        public static void ConfigureAutoMapper(this IServiceCollection services)
        {
            //services.AddAutoMapper(typeof(MappingEntity).Assembly);
            services.AddSingleton(provider => new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingEntity(provider.GetService<IWebHostEnvironment>()));
            }).CreateMapper());
        }
    }
}