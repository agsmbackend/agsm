﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MotoreListiniV2.DataAccessLayer.Database;
using MotoreListiniV2.DataAccessLayer.Database.Interceptors;

namespace MotoreListiniV2.Config
{
    public static class DbContextConfig
    {
        public static void ConfigureDbContext(this IServiceCollection services, IConfiguration configuration, IWebHostEnvironment env)
        {
            services.AddDbContextPool<ListinoContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("BO"), sql =>
                {
                    sql.EnableRetryOnFailure(3, TimeSpan.FromMilliseconds(500), null)
                        .MaxBatchSize(3);
                });

                options.EnableDetailedErrors();
                options.AddInterceptors(new SqlDebugOutputInterceptor());

                if (env.IsDevelopment())
                {
                    options.EnableSensitiveDataLogging();
                    options.LogTo(Console.WriteLine, new[] {DbLoggerCategory.Database.Command.Name}, LogLevel.Information);
                    //options.UseLoggerFactory(GetLoggerFactory());
                }
            }, 256);


            services.AddDbContextPool<OffertaContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("BO"), sql =>
                {
                    sql.EnableRetryOnFailure(3, TimeSpan.FromMilliseconds(500), null)
                        .MaxBatchSize(3);
                });

                options.EnableDetailedErrors();
                options.AddInterceptors(new SqlDebugOutputInterceptor());

                if (env.IsDevelopment())
                {
                    options.EnableSensitiveDataLogging();
                    options.LogTo(Console.WriteLine, new[] {DbLoggerCategory.Database.Command.Name}, LogLevel.Information);
                    //options.UseLoggerFactory(GetLoggerFactory());
                }
            }, 256);
        }

        private static ILoggerFactory GetLoggerFactory()
        {
            IServiceCollection serviceCollection = new ServiceCollection();
            serviceCollection.AddLogging(builder =>
                                        builder.AddConsole()
                                        .AddFilter(DbLoggerCategory.Database.Command.Name, LogLevel.Information));

            return serviceCollection.BuildServiceProvider().GetService<ILoggerFactory>();
        }

    }
}