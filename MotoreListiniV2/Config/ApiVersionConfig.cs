﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using MotoreListiniV2.Documentation;

namespace MotoreListiniV2.Config
{
    public static class ApiVersionConfig
    {
        public static void ConfigureApiVersion(this IServiceCollection services)
        {
            services.AddApiVersioning(
                config =>
                {
                    config.ReportApiVersions = true;
                    config.AssumeDefaultVersionWhenUnspecified = true;
                    config.DefaultApiVersion = new ApiVersion(1, 0);
                    config.ErrorResponses = new ApiVersioningErrorProvider();
                    //config.ApiVersionReader = new HeaderApiVersionReader("api-version");
                });

            services.AddVersionedApiExplorer(
                options =>
                {
                    options.GroupNameFormat = "'v'VVV";

                    // note: this option is only necessary when versioning by url segment. the SubstitutionFormat
                    // can also be used to control the format of the API version in route templates
                    options.SubstituteApiVersionInUrl = true;
                });
        }
    }
}