﻿using Microsoft.Extensions.DependencyInjection;

namespace MotoreListiniV2.Config
{
    public static class CorsConfig
    {
        internal const string CorsPolicy = "CorsPolicy";

        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(CorsPolicy,
                   builder => builder.WithOrigins("https://servizionline.agsm.it", "https://localhost.it", "http://localhost.it")
                   .AllowAnyMethod()
                   .AllowAnyHeader()
                   .AllowCredentials());
            });
        }
    }
}