﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace MotoreListiniV2.Config
{
    public static class HealthChecksConfig
    {
        public static void ConfigureHealthChecks(this IServiceCollection services, IConfiguration configuration)
        {
            services  //.AddHealthChecksUI()
           .AddHealthChecks().AddCheck("self", () => HealthCheckResult.Healthy())
           .AddSqlServer(
               connectionString: configuration.GetConnectionString("BO"),
               healthQuery: "SELECT 1;",
               name: "BO",
               failureStatus: HealthStatus.Degraded,
               tags: new string[] { "db", "sql", "sqlserver" });
        }
    }
}