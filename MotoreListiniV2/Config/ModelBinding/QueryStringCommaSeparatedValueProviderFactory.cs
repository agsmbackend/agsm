﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace MotoreListiniV2.Config.ModelBinding
{
    public class QueryStringCommaSeparatedValueProviderFactory : IValueProviderFactory
    {
        public Task CreateValueProviderAsync(ValueProviderFactoryContext context)
        {
            _ = context ?? throw new ArgumentNullException(nameof(context));

            var query = context.ActionContext.HttpContext.Request.Query;
            if (query is {Count: > 0})
            {
                var valueProvider = new QueryStringCommaSeparatedValueProvider(
                  FromCommaSeparatedArrayAttribute.CommaSeparated,
                  query,
                  CultureInfo.InvariantCulture);

                context.ValueProviders.Add(valueProvider);
            }

            return Task.CompletedTask;
        }
    }
}