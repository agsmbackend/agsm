﻿using System;
using Microsoft.AspNetCore.Mvc.Filters;

namespace MotoreListiniV2.Config.ModelBinding
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Enum | AttributeTargets.Interface | AttributeTargets.Delegate)]
    public class CommaSeparatedAttribute : Attribute, IResourceFilter
    {
        public void OnResourceExecuted(ResourceExecutedContext context)
        { }

        public void OnResourceExecuting(ResourceExecutingContext context)
        {
            _ = context ?? throw new ArgumentNullException(nameof(context));

            context.ValueProviderFactories.Insert(0, new QueryStringCommaSeparatedValueProviderFactory());
        }
    }
}