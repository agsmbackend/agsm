﻿using System.Globalization;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Primitives;
using UtilitiesGSA.Shareds;

namespace MotoreListiniV2.Config.ModelBinding
{
    public class QueryStringCommaSeparatedValueProvider : QueryStringValueProvider
    {
        public QueryStringCommaSeparatedValueProvider(
          BindingSource bindingSource,
          IQueryCollection values,
          CultureInfo culture) : base(bindingSource, values, culture)
        { }

        public override ValueProviderResult GetValue(string key)
        {
            var result = base.GetValue(key);

            if (result == ValueProviderResult.None)
            {
                return result;
            }

            var values = result.SelectMany(r => r.Split(CommonChars.Comma));

            return new ValueProviderResult(new StringValues(values.ToArray()), Culture);
        }
    }
}