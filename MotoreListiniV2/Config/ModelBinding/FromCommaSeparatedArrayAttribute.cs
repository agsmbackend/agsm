﻿using System;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace MotoreListiniV2.Config.ModelBinding
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Enum | AttributeTargets.Interface | AttributeTargets.Parameter)]
    public class FromCommaSeparatedArrayAttribute : Attribute, IBindingSourceMetadata
    {
        public static BindingSource CommaSeparated => new(
            id: "commaSeparated",
            displayName: "Comma separated array",
            isGreedy: false,
            isFromRequest: true);

        public BindingSource BindingSource => CommaSeparated;
    }
}