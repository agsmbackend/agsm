﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using MotoreListiniV2.Shared.ResponseBase;


namespace MotoreListiniV2.Config
{
    public static class ApiBehaviorConfig
    {
        public static void ConfigureApiBehavior(this IServiceCollection services)
        {
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = context =>
                {
                    var errors = context.ModelState
                        .SelectMany(entry => entry.Value.Errors)
                        .Select(error => error.ErrorMessage);

                        return new UnprocessableEntityObjectResult(new ResponseTyped<string>
                        {
                            Content = string.Empty,
                            Message = "Modello non valido",
                            Errors = errors,
                            ResultTyped = ResultType.Invalid
                        });
                    };
            });
        }
    }
}