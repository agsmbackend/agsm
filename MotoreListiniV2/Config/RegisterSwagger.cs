﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using MotoreListiniV2.Documentation;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace MotoreListiniV2.Config
{
    public static class RegisterSwagger
    {
        public static void RegisterSwaggerServices(this IServiceCollection services)
        {
            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();
            services.AddSwaggerGen(options =>
            {
                options.EnableAnnotations();
                options.OperationFilter<SwaggerDefaultValues>();

                // Set the comments path for the Swagger JSON and UI.
                /*
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                options.IncludeXmlComments(xmlPath);*/

                options.CustomOperationIds(apiDesc => $"{apiDesc.ActionDescriptor.RouteValues["controller"]}_{apiDesc.ActionDescriptor.RouteValues["action"]}");
            });

            services.AddSwaggerGenNewtonsoftSupport();
        }
    }
}
