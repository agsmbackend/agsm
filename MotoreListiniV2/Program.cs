﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using MotoreListiniV2.Extensions;
using Serilog;

namespace MotoreListiniV2
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                Log.Information("Inizio del programma...");
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
                throw;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog((hostingContext, loggerConfiguration) =>
                {
                    loggerConfiguration.SerilogConfigurationBase(
                        hostingContext.Configuration,
                        typeof(Program).Assembly.GetName().Name,
                        hostingContext.HostingEnvironment.EnvironmentName);
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                        //.UseEnvironment(Environments.Development)
                        .UseSentry(SentryConfigurationExtensions.SentryConfigurationBase)
                        .UseStartup<Startup>()
                        .CaptureStartupErrors(true);
                });
    }
}