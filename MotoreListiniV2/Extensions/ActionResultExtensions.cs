﻿using System;
using Microsoft.AspNetCore.Mvc;
using MotoreListiniV2.Shared.ResponseBase;
using WrapperGSA.APIBase.Response;

namespace MotoreListiniV2.Extensions
{
    public static class ActionResultExtensions
    {
        public static ActionResult FromResult<T>(this ControllerBase controller, Shared.ResponseBase.ResponseTyped<T> result)
        {
            _ = controller ?? throw new ArgumentNullException(nameof(controller));

            if (result is null)
            {
                return controller.NotFound();
            }

            return result.ResultTyped switch
            {
                ResultType.Ok => controller.Ok(result),
                ResultType.NotFound => controller.NotFound(result),
                ResultType.Invalid => controller.BadRequest(result),
                ResultType.Unexpected => controller.BadRequest(result),
                ResultType.Unauthorized => controller.Unauthorized(result),
                ResultType.PartialOk => controller.BadRequest(result),
                ResultType.PermissionDenied => controller.Unauthorized(result),
                _ => new InternalServerErrorObjectResult("error application")
            };
        }
    }
}