﻿using Microsoft.Extensions.Configuration;
using MotoreListiniV2.Config.Serilog;
using Serilog;

namespace MotoreListiniV2.Extensions
{
    public static class SerilogConfigurationExtensions
    {
        public static void SerilogConfigurationBase(this LoggerConfiguration loggerConfiguration,
            IConfiguration configuration, string appName, string environment)
        {
            loggerConfiguration
                .ReadFrom.Configuration(configuration)
                .Enrich.FromLogContext()
                .Enrich.WithProperty("ApplicationName", appName)
                .Enrich.WithProperty("Environment", environment)
                .Enrich.With(new LocalTimestampEnricher())
                .Enrich.With(new OperationInformationEnricher());

#if DEBUG
            // Used to filter out potentially bad data due debugging.
            // Very useful when doing Seq dashboards and want to remove logs under debugging session.
            loggerConfiguration.Enrich.WithProperty("DebuggerAttached", System.Diagnostics.Debugger.IsAttached.ToString());

            Serilog.Debugging.SelfLog.Enable(msg => { System.Diagnostics.Debug.Print(msg); });
#endif
        }
    }
}