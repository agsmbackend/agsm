﻿using System;
using Sentry;
using Sentry.AspNetCore;

namespace MotoreListiniV2.Extensions
{
    public static class SentryConfigurationExtensions
    {
        public static void SentryConfigurationBase(SentryAspNetCoreOptions sentryAction)
        {
            sentryAction.MaxRequestBodySize = Sentry.Extensibility.RequestSize.Always;
            sentryAction.Release = typeof(Program).Assembly.GetName().Name + "@" +
                                   typeof(Program).Assembly.GetName().Version;
            //sentryAction.SampleRate = 0.25f;
            sentryAction.TracesSampler = SentryActionTracesSampler();
        }

        private static Func<TransactionSamplingContext, double?> SentryActionTracesSampler()
        {
            return context =>
            {
                var ctx = context.TransactionContext;

                return ctx.Name switch
                {
                    "GetElencoListiniPanel" => 0.5,
                    "GetElencoListiniExcel" => 0.5,
                    "GET /status" => 0,
                    _ => null
                };
            };
        }
    }
}