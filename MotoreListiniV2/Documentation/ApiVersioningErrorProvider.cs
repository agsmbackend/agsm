﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using MotoreListiniV2.Shared.ResponseBase;

namespace MotoreListiniV2.Documentation
{
    public class ApiVersioningErrorProvider : DefaultErrorResponseProvider
    {
        public override IActionResult CreateResponse(ErrorResponseContext context)
        {
            var errorResponse = new ResponseTyped<string>
            {
                ResultTyped = ResultType.Invalid,
                Message = "Something went wrong while selecting the api version"
            };

            var response = new BadRequestObjectResult(errorResponse);

            return response;
        }
    }
}
