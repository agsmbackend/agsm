﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using MotoreListiniV2.BusinessLayer.Contracts;
using MotoreListiniV2.BusinessLayer.Services;
using MotoreListiniV2.Config.ModelBinding;
using MotoreListiniV2.Extensions;
using MotoreListiniV2.Shared.Dto;
using MotoreListiniV2.Shared.ResponseBase;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using Sentry;
using Swashbuckle.AspNetCore.Annotations;
using WrapperGSA.APIBase.Param;

namespace MotoreListiniV2.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class MotoreListiniController : ControllerBase
    {
        private readonly IMotoreListiniOfferte _motoreListini;

        public MotoreListiniController(IMotoreListiniOfferte motoreListini)
        {
            _motoreListini = motoreListini ?? throw new ArgumentNullException(nameof(motoreListini));
        }

        [Produces(HttpContentMediaTypes.JSON)]
        [SwaggerResponse(StatusCodes.Status200OK, type: typeof(ResponseTyped<IEnumerable<TipoTerminiPagamentiView>>))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerOperation(Summary = "Elenco Termini di Pagamento", Description = "Elenco Termini di Pagamento")]
        [HttpGet]
        [Route("GetTerminiPagamento", Name = nameof(GetTerminiPagamento))]
        public async Task<IActionResult> GetTerminiPagamento()
        {
            var ris = await _motoreListini.GetListTerminiPagamento().ConfigureAwait(false);

            return this.FromResult(ris);
        }


        [Produces(HttpContentMediaTypes.JSON)]
        [SwaggerResponse(StatusCodes.Status200OK, type: typeof(ResponseTyped<IEnumerable<ProdottoView>>))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerOperation(Summary = "Elenco delle tipologie di prezzo disponibili per tipo servizio passato",
                            Description = "Elenco delle tipologie di prezzo disponibili per tipo servizio passato")]
        [HttpGet]
        [Route("GetTipoPrezzo/{ts}", Name = nameof(GetTipoPrezzo))]
        public async Task<IActionResult> GetTipoPrezzo([SwaggerParameter(Description = "tipo servizio, valori possibili EE o GAS")]string ts)
        {
            var ris = await _motoreListini.GetTipoPrezzo(ts).ConfigureAwait(false);

            return this.FromResult(ris);
        }


        [Produces(HttpContentMediaTypes.JSON)]
        [SwaggerResponse(StatusCodes.Status200OK, type: typeof(ResponseTyped<IEnumerable<TipoOffertaView>>))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerOperation(Summary = "Elenco possibili componenti prezzi disponibili per tipo servizio passato",
            Description = "Elenco possibili componenti prezzi disponibili per tipo servizio passato")]
        [HttpGet("GetTipoOfferta/{ts}", Name = nameof(GetTipoOfferta))]
        public async Task<IActionResult> GetTipoOfferta(string ts = "all")
        {
            var ris = await _motoreListini.GetTipoOfferta(ts).ConfigureAwait(false);

            return this.FromResult(ris);
        }


        [Produces(HttpContentMediaTypes.JSON)]
        [SwaggerResponse(StatusCodes.Status200OK, type: typeof(ResponseTyped<IEnumerable<ComponentiPrezziGenericiView>>))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerOperation(Summary = "Elenco componenti prezzi per tipo servizio passato",
            Description = "Elenco componenti prezzi per tipo servizio passato")]
        [HttpGet]
        [Route("GetComponentiPrezzi/{ts}", Name = nameof(ComponentiPrezziGenericiView))]
        public async Task<IActionResult> GetComponentiPrezziGenerici(string ts)
        {
            var ris = await _motoreListini.GetComponentiPrezziGenerici(ts).ConfigureAwait(false);

            return this.FromResult(ris);
        }


        [Produces(HttpContentMediaTypes.JSON)]
        [SwaggerResponse(StatusCodes.Status200OK, type: typeof(ResponseTyped<IEnumerable<ComponentiPrezziView>>))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerOperation(Summary = "Elenco componenti prezzi per il tipo offerta passato",
            Description = "Elenco componenti prezzi per il tipo offerta passato")]
        [HttpGet]
        [Route("GetComponentiPrezzi/{ts}/{idTipoOfferta:int}", Name = nameof(GetComponentiPrezzi))]
        public async Task<IActionResult> GetComponentiPrezzi(int idTipoOfferta)
        {
            var ris = await _motoreListini.GetComponentiPrezzi(idTipoOfferta).ConfigureAwait(false);

            return this.FromResult(ris);
        }


        [Produces(HttpContentMediaTypes.JSON)]
        [SwaggerResponse(StatusCodes.Status200OK, type: typeof(ResponseTyped<IEnumerable<ComponentiFatturazioneView>>))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerOperation(Summary = "Elenco componenti fatturazione per il tipo servizio passato",
            Description = "Elenco componenti fatturazione per il tipo servizio passato")]
        [HttpGet]
        [Route("GetComponentiFatturazione/{ts}", Name = nameof(GetComponentiFatturazione))]
        public async Task<IActionResult> GetComponentiFatturazione(string ts="all")
        {
            var ris = await _motoreListini.GetComponentiFatturazione(ts).ConfigureAwait(false);

            return this.FromResult(ris);
        }


        [Produces(HttpContentMediaTypes.JSON)]
        [SwaggerResponse(StatusCodes.Status200OK, type: typeof(ResponseTyped<IEnumerable<ElencoListiniFullView>>))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerOperation(Summary = "Chiamata per popolare pagina elenco listini",
            Description = "Chiamata per popolare pagina elenco listini")]
        [HttpGet]
        [Route("GetElencoListiniPanel", Name = nameof(GetElencoListiniPanel))]
        public async Task<IActionResult> GetElencoListiniPanel([FromQuery, BindRequired] DateTime dataValiditaDa,
                                                          [FromQuery, BindRequired] DateTime dataValidataA,
                                                          [FromQuery] string prodotto,
                                                          [FromQuery] string listino)
        {
            var transaction = SentrySdk.StartTransaction(
                nameof(GetElencoListiniPanel), // name
                "GetElencoListiniPanel-controller" // operation
            );

            var ris = await _motoreListini.GetElencoListiniPanel(dataValiditaDa, dataValidataA, prodotto ?? string.Empty, listino ?? string.Empty).ConfigureAwait(false);

            transaction.Finish();

            return this.FromResult(ris);
        }


        [Produces(HttpContentMediaTypes.JSON)]
        [SwaggerResponse(StatusCodes.Status200OK, type: typeof(ResponseTyped<IEnumerable<OffertaPrezziView>>))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerOperation(Summary = "Ritorna prezzi applicato per id listino",
            Description = "Ritorna prezzi applicato per id listino")]
        [HttpGet]
        [Route("GetPrezziByListino/{idListino:int}", Name = nameof(GetPrezziByListino))]
        public async Task<IActionResult> GetPrezziByListino(int idListino)
        {
            var ris = await _motoreListini.GetPrezziByListino(idListino).ConfigureAwait(false);

            return this.FromResult(ris);
        }


        [Produces(HttpContentMediaTypes.JSON)]
        [SwaggerResponse(StatusCodes.Status200OK, type: typeof(ResponseTyped<IEnumerable<OffertaComponentiFatturazioneView>>))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerOperation(Summary = "Ritorna componenti fatturazione per id listino",
            Description = "Ritorna componenti fatturazione per id listino")]
        [HttpGet]
        [Route("GetComponentiFattByListino/{idListino:int}", Name = nameof(GetComponentiFattByListino))]
        public async Task<IActionResult> GetComponentiFattByListino(int idListino)
        {
            var ris = await _motoreListini.GetComponentiFattByListino(idListino).ConfigureAwait(false);

            return ris == null
                ? this.FromResult(new ResponseTyped<string>
                {
                    Message = new StringBuilder("Nessuna componente di fatturazione associata per il listino passato").ToString(),
                    ResultTyped = ResultType.NotFound,
                    Errors = null
                })
                : this.FromResult(ris);
        }

        [Produces("application/octet-stream")]
        [HttpGet]
        [Route("GetElencoListiniExcel", Name = nameof(GetElencoListiniExcel))]
        public async Task<IActionResult> GetElencoListiniExcel([BindRequired, FromCommaSeparatedArray]List<int> idListini)
        {
            var transaction = SentrySdk.StartTransaction(
                nameof(GetElencoListiniExcel), // name
                "GetElencoListiniExcel-controller" // operation
            );

            var ris = await _motoreListini.GetElencoListiniExcel(idListini).ConfigureAwait(false);

            transaction.Finish();

            if (ris == null || ris.Content.Rows.Count == 0)
            {
                return NotFound(new ResponseTyped<string> { Message = new StringBuilder("Nessuna listino trovato").ToString() });
            }

            var stream = new MemoryStream();
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (var package = new ExcelPackage(stream))
            {
                var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                workSheet.Cells.LoadFromDataTable(ris.Content, true);
                await package.SaveAsync();
            }
            stream.Position = 0;
            var excelName = $"ElencoListini-{DateTime.Now.ToString("yyyyMMddHHmmssfff", CultureInfo.InvariantCulture)}.xlsx";

            return File(stream, "application/octet-stream", excelName);
        }


        /// <summary>
        /// Inserire listino completo da zero
        /// </summary>
        /// <param name="listino"></param>
        /// <returns></returns>
        [Produces(HttpContentMediaTypes.JSON)]
        [HttpPost]
        [Route("PostSaveFullListinoEntity", Name = nameof(PostSaveFullListinoEntity))]
        public async Task<IActionResult> PostSaveFullListinoEntity([FromBody]SaveFullListinoEntity listino)
        {
            var ris = await _motoreListini.PostSaveFullListinoEntity(listino).ConfigureAwait(false);

            if (ris == null)
            {
                var result = new ResponseTyped<string> { Message = new StringBuilder("Nessun listino salvato").ToString(), ResultTyped = ResultType.NotFound, Errors = null };
                return this.FromResult(result);
            }

            return this.FromResult(ris);
        }

        /// <summary>
        /// Chiusura anticipata listino
        /// </summary>
        /// <param name="dataAnticipo">Oggetto Dinamico, passare due campi: "idListino" (id offerta) e "validitaA" (data chiusura anticipata)</param>
        /// <returns></returns>
        [Produces(HttpContentMediaTypes.JSON)]
        [HttpPost]
        [Route("PostAnticipaChiusuraListino", Name = nameof(PostAnticipaChiusuraListino))]
        public async Task<IActionResult> PostAnticipaChiusuraListino([FromBody] JObject dataAnticipo)
        {
            if (dataAnticipo is null)
            {
                var result = new ResponseTyped<string> { Message = new StringBuilder("Nessuna dataanticipo salvato").ToString(), ResultTyped = ResultType.NotFound, Errors = null };
                return this.FromResult(result);
            }

            if (!dataAnticipo.HasValues)
                return this.FromResult(new ResponseTyped<string> { Message = new StringBuilder("Oggetto vuoto").ToString(), ResultTyped = ResultType.Invalid, Errors = null });

            var ris = await _motoreListini.PostAnticipaChiusuraListino(dataAnticipo).ConfigureAwait(false);

            return this.FromResult(ris ?? new ResponseTyped<string> { Message = new StringBuilder("Nessun Listino Trovato").ToString(), ResultTyped = ResultType.NotFound, Errors = null });
        }

        /// <summary>
        /// Salvataggio fisico sul server dell'allegato listino
        /// </summary>
        /// <returns></returns>
        [Produces(HttpContentMediaTypes.JSON)]
        [HttpPost]
        [Route("PDFUploader", Name = nameof(PostPdfUploader))]
        public async Task<IActionResult> PostPdfUploader(IFormFile allegato)
        {
            var ris = await _motoreListini.PostPdfUploader(allegato).ConfigureAwait(false);

            return this.FromResult(ris);
        }

        #region categorie

        /// <summary>
        /// Inserire nuova categoria
        /// </summary>
        /// <param name="categoria"></param>
        /// <returns></returns>
        [Produces(HttpContentMediaTypes.JSON)]
        [HttpPost]
        [Route("PostSaveCategoriaEntity", Name = nameof(PostSaveCategoriaEntity))]
        public async Task<IActionResult> PostSaveCategoriaEntity([FromBody] SaveCategoriaEntity categoria)
        {
            var ris = await _motoreListini.PostSaveCategoriaEntity(categoria).ConfigureAwait(false);

            if (ris == null)
            {
                var result = new ResponseTyped<string> { Message = new StringBuilder("Nessuna categoria salvata").ToString(), ResultTyped = ResultType.NotFound, Errors = null };
                return this.FromResult(result);
            }

            return this.FromResult(ris);
        }

        [Produces(HttpContentMediaTypes.JSON)]
        [SwaggerResponse(StatusCodes.Status200OK, type: typeof(ResponseTyped<IEnumerable<string>>))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerOperation(Summary = "Test",
            Description = "Test")]
        [HttpGet]
        [Route("GetMyTest/{idListino:int}", Name = nameof(GetMyTest))]
        public async Task<IActionResult> GetMyTest(int idListino)
        {
           
            return Ok("prova");
        }

        [Produces(HttpContentMediaTypes.JSON)]
        [SwaggerResponse(StatusCodes.Status200OK, type: typeof(ResponseTyped<IEnumerable<ElencoCategorieFullView>>))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerOperation(Summary = "Chiamata per popolare elenco categorie del pannello luce o gas",
        Description = "Chiamata per popolare elenco categorie del pannello luce o gas")]
        [HttpGet]
        [Route("GetElencoCategorie", Name = nameof(GetElencoCategorie))]
        public async Task<IActionResult> GetElencoCategorie([FromQuery] string descrizione,
                                                  [FromQuery] string grado,
                                                  [FromQuery] string tipo_servizio)
        {
            var transaction = SentrySdk.StartTransaction(
                nameof(GetElencoCategorie), // name
                "GetElencoCategorie-controller" // operation
            );

            var ris = await _motoreListini.GetElencoCategorie(descrizione ?? string.Empty, grado ?? string.Empty, tipo_servizio).ConfigureAwait(false);

            transaction.Finish();

            return this.FromResult(ris);
        }

        #endregion

        #region configurazioni

        /// <summary>
        /// Inserire nuova categoria
        /// </summary>
        /// <param name="configurazione"></param>
        /// <returns></returns>
        [Produces(HttpContentMediaTypes.JSON)]
        [HttpPost]
        [Route("PostSaveConfigurazioneEntity", Name = nameof(PostSaveConfigurazioneEntity))]
        public async Task<IActionResult> PostSaveConfigurazioneEntity([FromBody] SaveConfigurazioneEntity configurazione)
        {
            var ris = await _motoreListini.PostSaveConfigurazioneEntity(configurazione).ConfigureAwait(false);

            if (ris == null)
            {
                var result = new ResponseTyped<string> { Message = new StringBuilder("Nessuna configurazione salvata").ToString(), ResultTyped = ResultType.NotFound, Errors = null };
                return this.FromResult(result);
            }

            return this.FromResult(ris);
        }

        /// <summary>
        /// Inserire nuova categoria
        /// </summary>
        /// <param name="configurazione"></param>
        /// <returns></returns>
        [Produces(HttpContentMediaTypes.JSON)]
        [HttpPost]
        [Route("DeleteConfigurazioneEntity", Name = nameof(DeleteConfigurazioneEntity))]
        public async Task<IActionResult> DeleteConfigurazioneEntity([FromBody] int configurazioneId)
        {
            var ris = await _motoreListini.DeleteConfigurazioneEntity(configurazioneId).ConfigureAwait(false);

            if (ris == null)
            {
                var result = new ResponseTyped<string> { Message = new StringBuilder("Nessuna configurazione eliminata").ToString(), ResultTyped = ResultType.NotFound, Errors = null };
                return this.FromResult(result);
            }

            return this.FromResult(ris);
        }

        #endregion

        #region caratteristicheOfferta

        [Produces(HttpContentMediaTypes.JSON)]
        [SwaggerResponse(StatusCodes.Status200OK, type: typeof(ResponseTyped<IEnumerable<TipoTariffaView>>))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerOperation(Summary = "Elenco Termini Tariffa", Description = "Elenco Tipi Tariffa in base al servizio")]
        [HttpGet]
        [Route("GetTipiTariffa/{idServizio:int}", Name = nameof(GetTipiTariffa))]
        public async Task<IActionResult> GetTipiTariffa(int idServizio)
        {
            var ris = await _motoreListini.GetTipiTariffa(idServizio).ConfigureAwait(false);

            return this.FromResult(ris);
        }

        [Produces(HttpContentMediaTypes.JSON)]
        [SwaggerResponse(StatusCodes.Status200OK, type: typeof(ResponseTyped<IEnumerable<UsoView>>))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerOperation(Summary = "Elenco Usi Prodotto", Description = "Elenco Usi Prodotto in base al servizio")]
        [HttpGet]
        [Route("GetUsi/{idServizio:int}", Name = nameof(GetUsi))]
        public async Task<IActionResult> GetUsi(int idServizio)
        {
            var ris = await _motoreListini.GetUsi(idServizio).ConfigureAwait(false);

            return this.FromResult(ris);
        }

        [Produces(HttpContentMediaTypes.JSON)]
        [SwaggerResponse(StatusCodes.Status200OK, type: typeof(ResponseTyped<IEnumerable<MateriaPrimaView>>))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerOperation(Summary = "Elenco Materie Prime Prodotto", Description = "Elenco Materie Prime Prodotto in base al servizio")]
        [HttpGet]
        [Route("GetMateriePrime/{idServizio:int}", Name = nameof(GetMateriePrime))]
        public async Task<IActionResult> GetMateriePrime(int idServizio)
        {
            var ris = await _motoreListini.GetMateriePrime(idServizio).ConfigureAwait(false);

            return this.FromResult(ris);
        }

        [Produces(HttpContentMediaTypes.JSON)]
        [SwaggerResponse(StatusCodes.Status200OK, type: typeof(ResponseTyped<IEnumerable<ElencoCategorieFullView>>))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerOperation(Summary = "Chiamata per popolare elenco categorie dell'offerta",
        Description = "Chiamata per popolare elenco categorie dell'offerta")]
        [HttpGet]
        [Route("GetElencoCategorieOfferta", Name = nameof(GetElencoCategorieOfferta))]
        public async Task<IActionResult> GetElencoCategorieOfferta([FromQuery] int? idOfferta,
                                          [FromQuery] string grado,
                                          [FromQuery] string tipo_servizio)
        {
            var transaction = SentrySdk.StartTransaction(
                nameof(GetElencoCategorieOfferta), // name
                "GetElencoCategorieOfferta-controller" // operation
            );

            var ris = await _motoreListini.GetElencoCategorieOfferta(idOfferta, grado ?? string.Empty, tipo_servizio).ConfigureAwait(false);

            transaction.Finish();

            return this.FromResult(ris);
        }

        /// <summary>
        /// Inserire listino completo da zero
        /// </summary>
        /// <param name="listino"></param>
        /// <returns></returns>
        [Produces(HttpContentMediaTypes.JSON)]
        [HttpPost]
        [Route("PostSaveSetupOfferta", Name = nameof(PostSaveSetupOfferta))]
        public async Task<IActionResult> PostSaveSetupOfferta([FromBody] SaveSetupEntity setup)
        {
            var ris = await _motoreListini.PostSaveSetupOfferta(setup).ConfigureAwait(false);

            if (ris == null)
            {
                var result = new ResponseTyped<string> { Message = new StringBuilder("Nessuna configurazione listino salvata").ToString(), ResultTyped = ResultType.NotFound, Errors = null };
                return this.FromResult(result);
            }

            return this.FromResult(ris);
        }

        [Produces(HttpContentMediaTypes.JSON)]
        [SwaggerResponse(StatusCodes.Status200OK, type: typeof(ResponseTyped<ElencoCaratteristicheFullView>))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerOperation(Summary = "Elenco caratteristiche offerta", Description = "Elenco caratteristiche offerta")]
        [HttpGet]
        [Route("GetCaratteristicheOfferta/{idOfferta:int}", Name = nameof(GetCaratteristicheOfferta))]
        public async Task<IActionResult> GetCaratteristicheOfferta(int idOfferta)
        {
            var ris = await _motoreListini.GetCaratteristicheOfferta(idOfferta).ConfigureAwait(false);

            return this.FromResult(ris);
        }

        [Produces(HttpContentMediaTypes.JSON)]
        [SwaggerResponse(StatusCodes.Status200OK, type: typeof(ResponseTyped<IEnumerable<ElencoListiniView>>))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        [SwaggerOperation(Summary = "Chiamata per popolare elenco listini",
            Description = "Chiamata per popolare pagina elenco listini - NUOVA VERSIONE")]
        [HttpGet]
        [Route("GetElencoListini", Name = nameof(GetElencoListini))]
        public async Task<IActionResult> GetElencoListini([FromQuery, BindRequired] DateTime dataValiditaDa,
                                                  [FromQuery, BindRequired] DateTime dataValidataA,
                                                  [FromQuery] string prodotto,
                                                  [FromQuery] string listino)
        {
            var transaction = SentrySdk.StartTransaction(
                nameof(GetElencoListini), // name
                "GetElencoListini-controller" // operation
            );

            var ris = await _motoreListini.GetElencoListini(dataValiditaDa, dataValidataA, prodotto ?? string.Empty, listino ?? string.Empty).ConfigureAwait(false);

            transaction.Finish();

            return this.FromResult(ris);
        }

        /// <summary>
        /// Inserire listino completo da zero
        /// </summary>
        /// <param name="listino"></param>
        /// <returns></returns>
        [Produces(HttpContentMediaTypes.JSON)]
        [HttpPost]
        [Route("PostSaveListinoEntity", Name = nameof(PostSaveListinoEntity))]
        public async Task<IActionResult> PostSaveListinoEntity([FromBody] FinalizeListinoEntity listino)
        {
            var ris = await _motoreListini.PostSaveListinoEntity(listino).ConfigureAwait(false);

            if (ris == null)
            {
                var result = new ResponseTyped<string> { Message = new StringBuilder("Nessun listino salvato").ToString(), ResultTyped = ResultType.NotFound, Errors = null };
                return this.FromResult(result);
            }

            return this.FromResult(ris);
        }

        /// <summary>
        /// Inserire listino completo da zero
        /// </summary>
        /// <param name="listino"></param>
        /// <returns></returns>
        //[Produces(HttpContentMediaTypes.JSON)]
        //[HttpPost]
        //[Route("PostSaveListinoEntityTest", Name = nameof(PostSaveListinoEntityTest))]
        //public async Task<IActionResult> PostSaveListinoEntityTest()
        //{
        //    var ris = await _motoreListini.PostSaveListinoEntityTest().ConfigureAwait(false);

        //    if (ris == null)
        //    {
        //        var result = new ResponseTyped<string> { Message = new StringBuilder("Nessun listino salvato").ToString(), ResultTyped = ResultType.NotFound, Errors = null };
        //        return this.FromResult(result);
        //    }

        //    return this.FromResult(ris);
        //}

        #endregion

    }

}