﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using MotoreListiniV2.DataAccessLayer.Models.FakeData;
using MotoreListiniV2.Shared.Dto;
using WrapperGSA.APIBase.Param;

namespace MotoreListiniV2.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class FakeDataController : ControllerBase
    {
        public FakeDataController()
        {
        }

        /*
        [HttpGet("ListSaveFullListinoEntity")]
        public IEnumerable<SaveFullListinoEntity> ListSaveFullListinoEntity()
        {
            var data = _contactsGeneratorService.Collection(5);
            return data;
        }
        */

        /// <summary>
        /// Genera Singolo Oggetto per testare POST CREAZIONE LISTINO
        /// </summary>
        /// <returns></returns>
        [Produces(HttpContentMediaTypes.JSON)]
        [HttpGet("SingleSaveFullListinoEntity", Name = nameof(SingleSaveFullListinoEntity))]
        public SaveFullListinoEntity SingleSaveFullListinoEntity([BindRequired, FromQuery]bool isDuplica)
        {
            GenFu.GenFu.Default().FillerManager.RegisterFiller(new ProductFiller());
            GenFu.GenFu.Default().FillerManager.RegisterFiller(new ListinoFiller());

            GenFu.GenFu.Configure<SaveComponentiPrezziEntity>()
                .Fill(b => b.Prezzo).WithRandom(new double[] { 4.026, 5.589, 6.012, 7.236 })
                .Fill(b => b.ComponentiPrezziId).WithinRange(1, 7);

            var prices = GenFu.GenFu.ListOf<SaveComponentiPrezziEntity>(5);

            GenFu.GenFu.Configure<SavePrezziEntity>()
              .Fill(b => b.MeseDa).WithinRange(0, 0)
              .Fill(b => b.MeseA).WithinRange(12, 18)
              .Fill(b => b.Percentuale).WithinRange(1, 100)
              .Fill(b => b.TipoPrezziId).WithRandom(new[] { 33, 34, 35, 37, 38, 41, 93 })
              .Fill(b => b.TipoOfferteId).WithinRange(1, 5)
              .Fill(b => b.SaveComponentiPrezziEntityList).AsRandom(prices, 1, 2);

            var savePrezziEntityLists = GenFu.GenFu.ListOf<SavePrezziEntity>(1);

            GenFu.GenFu.Configure<SaveComponentiFatturazioneEntity>()
                 .Fill(b => b.ComponentiFatturazioneId).WithinRange(1, 5)
                 .Fill(b => b.MeseDa).WithinRange(0, 0)
                 .Fill(b => b.MeseA).WithinRange(12, 18)
                 .Fill(b => b.Value).WithRandom(new[] { 5.5 })
                 .Fill(b => b.IsOptional).WithRandom(new[] { true, true, false });

            var saveComponentiFatturazioneEntityLists = GenFu.GenFu.ListOf<SaveComponentiFatturazioneEntity>(2);

            if (isDuplica)
            {
                var i = 1;

                GenFu.GenFu.Configure<SaveFullListinoEntity>()
                    .Fill(b => b.IdProdotto, () => i++)
                    .Fill(b => b.TipoServizio).WithRandom(new[] { "EE", "GAS" })
                    .Fill(b => b.TipoTariffa).WithRandom(new[] { "DOM", "AU" })
                    .Fill(b => b.Mese).WithinRange(1, 12)
                    .Fill(b => b.Anno).WithinRange(2019, 2022)
                    .Fill(b => b.Revisione).WithinRange(1, 999)
                    .Fill(b => b.Trimestre).WithinRange(1, 5)
                    .Fill(b => b.InizioValidita, () => DateTime.Now.Date)
                    .Fill(b => b.FineValidita, () => DateTime.Now.Date.AddDays(30).AddMilliseconds(-3))
                    .Fill(b => b.InizioAttivazione, () => DateTime.Now.Date)
                    .Fill(b => b.FineAttivazione, () => DateTime.Now.Date.AddDays(30).AddMilliseconds(-3))
                    .Fill(b => b.SpPDF).WithRandom(new[] { "test.pdf", "test1.pdf", "test2.pdf" })
                    .Fill(b => b.TerminiPagamento).WithinRange(2, 5)
                    .Fill(b => b.SavePrezziEntityList).AsRandom(savePrezziEntityLists, 1, 1)
                    .Fill(b => b.SaveComponentiFatturazioneEntityList).AsRandom(saveComponentiFatturazioneEntityLists, 1, 1);
            }
            else
            {
                GenFu.GenFu.Configure<SaveFullListinoEntity>()
                  .Fill(b => b.IdProdotto, () => -1)
                  .Fill(b => b.TipoServizio).WithRandom(new string[] { "EE", "GAS" })
                  .Fill(b => b.TipoTariffa).WithRandom(new string[] { "DOM", "AU" })
                  .Fill(b => b.Mese).WithinRange(1, 12)
                  .Fill(b => b.Anno).WithinRange(2019, 2030)
                  .Fill(b => b.Revisione).WithinRange(1, 999)
                  .Fill(b => b.Trimestre).WithinRange(1, 5)
                  .Fill(b => b.InizioValidita, () => DateTime.Now.Date)
                  .Fill(b => b.FineValidita, () => DateTime.Now.Date.AddDays(30).AddMilliseconds(-3))
                  .Fill(b => b.InizioAttivazione, () => DateTime.Now.Date)
                  .Fill(b => b.FineAttivazione, () => DateTime.Now.Date.AddDays(30).AddMilliseconds(-3))
                  .Fill(b => b.SpPDF).WithRandom(new[] { "test.pdf", "test1.pdf", "test2.pdf" })
                  .Fill(b => b.TerminiPagamento).WithinRange(2, 5)
                  .Fill(b => b.SavePrezziEntityList).AsRandom(savePrezziEntityLists, 1, 1)
                  .Fill(b => b.SaveComponentiFatturazioneEntityList).AsRandom(saveComponentiFatturazioneEntityLists, 1, 1);
            }

            return GenFu.GenFu.New<SaveFullListinoEntity>();
        }
    }
}