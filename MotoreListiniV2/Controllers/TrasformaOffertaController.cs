﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MotoreListiniV2.BusinessLayer.Contracts;
using MotoreListiniV2.BusinessLayer.Services;
using MotoreListiniV2.Extensions;
using WrapperGSA.APIBase.Param;

namespace MotoreListiniV2.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class TrasformaOffertaController : ControllerBase
    {
        private readonly ITransformerOfferta2Listino _off2List;

        public TrasformaOffertaController(ITransformerOfferta2Listino off2List)
        {
            _off2List = off2List ?? throw new ArgumentNullException(nameof(off2List));
        }

        [Produces(HttpContentMediaTypes.JSON)]
        [HttpPost]
        [Route("CreateListinoByOfferta/{idOfferta:int}", Name = nameof(CreateListinoByOfferta))]
        public async Task<IActionResult> CreateListinoByOfferta(int idOfferta)
        {
            var ris = await _off2List.CreateListinoByOfferta(idOfferta).ConfigureAwait(false);

            return this.FromResult(ris);
        }
    }
}