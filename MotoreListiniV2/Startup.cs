﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MotoreListiniV2.BusinessLayer.Contracts;
using MotoreListiniV2.BusinessLayer.Services;
using MotoreListiniV2.Config;
using MotoreListiniV2.Config.ModelBinding;
using MotoreListiniV2.Config.Serilog;
using MotoreListiniV2.DataAccessLayer.Models.FakeData;
using MotoreListiniV2.Extensions;
using MotoreListiniV2.Filters;
using Newtonsoft.Json;
using Sentry.AspNetCore;
using Serilog;
using WrapperGSA.APIBase.Extensions;
using WrapperGSA.APIBase.Response;
using WrapperGSA.APIBase.Utils.HealtChecksUtils;

namespace MotoreListiniV2
{
    public class Startup
    {
        private readonly IWebHostEnvironment _env;

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            _env = env;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.ConfigureAutoMapper();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.ConfigureHealthChecks(Configuration);

            services.ConfigureCors();

            services.AddControllers(options =>
                {
                    options.RespectBrowserAcceptHeader = true;
                    options.OutputFormatters.RemoveType<HttpNoContentOutputFormatter>();
                    options.OutputFormatters.Insert(0, new HttpNoContentOutputFormatter
                    {
                        TreatNullValueAsNoContent = false
                    });
                    options.Filters.Add<SerilogLoggingActionFilter>();

                    options.ValueProviderFactories.Add(new QueryStringCommaSeparatedValueProviderFactory());
                })
                .ConfigureApiBehaviorOptions(options =>
                {
                    options.SuppressConsumesConstraintForFormFileParameters = true;
                    options.SuppressInferBindingSourcesForParameters = true;
                    //options.SuppressModelStateInvalidFilter = true;
                    options.SuppressMapClientErrors = true;
                    options.ClientErrorMapping[404].Link = "https://httpstatuses.com/404";
                })
                .AddControllersAsServices()
                .AddNewtonsoftJson(options =>
                    {
                        options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                        options.SerializerSettings.Formatting = _env.IsDevelopment() ? Formatting.Indented : Formatting.None;
                    }
                );

            services.ConfigureApiVersion();

            services.RegisterSwaggerServices();

            services.ConfigureDbContext(Configuration, _env);

            services.AddSingleton<IResponseFactory, ResponseFactory>();
            services.AddScoped<IMotoreListiniOfferte, MotoreListiniOfferte>();
            services.AddScoped<ITransformerOfferta2Listino, TransformerOfferta2Listino>();
            services.AddSingleton(typeof(IDataGenerator<>), typeof(DataGeneratorService<>));

            services.ConfigureApiBehavior();

            services.AddCustomExceptionDetails(options =>
            {
                options.IncludeExceptionDetails = _ => _env.IsDevelopment() || _env.IsStaging();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment enviroment)
        {
            if (enviroment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.ConfigureCustomExceptionMiddleware();

            app.UseHttpsRedirection();

            //This will make the HTTP requests log as rich logs instead of plain text.
            app.UseSerilogRequestLogging(opts =>
            {
                opts.EnrichDiagnosticContext = LogHelper.EnrichFromRequest;
                opts.GetLevel = LogHelper.ExcludeHealthChecks;
            });

            var provider = app.ApplicationServices.GetService<IApiVersionDescriptionProvider>();
            app.UseCustomSwagger(provider);

            app.UseRouting();

            app.UseSentryTracing();

            app.UseCors(CorsConfig.CorsPolicy);

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/status", new HealthCheckOptions
                {
                    ResponseWriter = HealtchCheckCustomResponse.CustomResponseWriterAsync
                });

                endpoints.MapControllers();
            });
        }

    }
}